package com.matrice.general;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MatriceTest {

	protected WebDriver driver;
	protected String baseUrl;
	protected StringBuffer verificationErrors = new StringBuffer();
	protected WebDriverWait wait;
	protected Calendar calendario = Calendar.getInstance();

	@Before
	public void setUp() throws Exception {
		driver = BuildDriver.getInstance();
		baseUrl = Constantes.BASE_URL;
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 200);
	}
	
}
