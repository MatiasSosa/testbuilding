package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Triple;

public class ReadExcelTriple {
	
	/**
	 * @param archivoDestino
	 * @param nombreHoja
	 * @return
	 */
	public static List<Triple> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Triple> triples = new ArrayList<Triple>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Triple triple;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						triple = new Triple(hoja.getCell(0, fila).getContents().toString(), 
  								 hoja.getCell(1, fila).getContents().toString(),hoja.getCell(2, fila).getContents().toString());
								
						triples.add(triple);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("ReadExcelTriple, cargados: "+triples.size());
		return triples;
	}


}
