package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Espacio;

public class ReadExcelEspacios {
	
	public static List<Espacio> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Espacio> espacios = new ArrayList<Espacio>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Espacio espacio;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						espacio = new Espacio(
								hoja.getCell(0, fila).getContents().toString(), 
  								hoja.getCell(1, fila).getContents().toString(),
  								hoja.getCell(2, fila).getContents().toString(),
  								hoja.getCell(3, fila).getContents().toString(),
  								hoja.getCell(4, fila).getContents().toString(),
  								hoja.getCell(5, fila).getContents().toString(),
  								hoja.getCell(6, fila).getContents().toString(),
  								hoja.getCell(7, fila).getContents().toString(),
  								hoja.getCell(8, fila).getContents().toString()
  								);
								
						espacios.add(espacio);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Espacios cargados "+espacios.size());
		return espacios;
	}


}
