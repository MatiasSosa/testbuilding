package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Usuarios;

public class ReadExcelUsuarios {
	
	public static List<Usuarios> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Usuarios> usuarios = new ArrayList<Usuarios>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Usuarios usuario;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						usuario = new Usuarios 
								(hoja.getCell(0, fila).getContents(), 
  								 hoja.getCell(1, fila).getContents(), 
								 hoja.getCell(2, fila).getContents(), 
								 hoja.getCell(3, fila).getContents(), 
  								 hoja.getCell(4, fila).getContents(), 
								 hoja.getCell(5, fila).getContents());
						usuarios.add(usuario);
						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Usuarios: "+usuarios.size());
		return usuarios;
	}
}