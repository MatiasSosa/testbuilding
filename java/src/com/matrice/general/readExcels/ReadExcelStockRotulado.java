package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.ItemRotulado;

public class ReadExcelStockRotulado {
	
	public static List<ItemRotulado> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<ItemRotulado> itemsRotulados = new ArrayList<ItemRotulado>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					ItemRotulado rotulado;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						rotulado = new ItemRotulado 
								(hoja.getCell(0, fila).getContents(), 
  								 hoja.getCell(1, fila).getContents(), 
								 hoja.getCell(2, fila).getContents(), 
								 hoja.getCell(3, fila).getContents(), 
								 hoja.getCell(4, fila).getContents(), 
								 hoja.getCell(5, fila).getContents(), 
								 hoja.getCell(6, fila).getContents(), 
								 hoja.getCell(7, fila).getContents(), 
								 hoja.getCell(8, fila).getContents()
								 );
								
						itemsRotulados.add(rotulado);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println(">>> Stock, Items Rotulados: "+itemsRotulados.size());
		return itemsRotulados;
	}


}