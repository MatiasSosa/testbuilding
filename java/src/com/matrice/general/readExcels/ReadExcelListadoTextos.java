package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import jxl.Sheet;
import jxl.Workbook;

public class ReadExcelListadoTextos {
	
	public static Collection<String> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		Collection elementos = new ArrayList<String>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja
			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					String elemento;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						elemento = hoja.getCell(0, fila).getContents();
						elementos.add(elemento);

						}
						System.out.println("\n");
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Elementos: "+ elementos.size());
		return elementos;
	}


}