package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Piso;

public class ReadExcelPisos {
	
	public static List<Piso> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Piso> pisos = new ArrayList<Piso>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Piso inmueble;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						inmueble = new Piso(
								hoja.getCell(0, fila).getContents().toString(), 
  								hoja.getCell(1, fila).getContents().toString(),
  								hoja.getCell(2, fila).getContents().toString(),
  								hoja.getCell(3, fila).getContents().toString());
								
						pisos.add(inmueble);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Pisos cargados "+pisos.size());
		return pisos;
	}


}
