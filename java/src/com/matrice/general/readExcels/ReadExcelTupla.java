package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Tupla;

public class ReadExcelTupla {
	
	/**
	 * @param archivoDestino
	 * @param nombreHoja
	 * @return
	 */
	public static List<Tupla> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Tupla> tuplas = new ArrayList<Tupla>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Tupla tupla;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						tupla = new Tupla(hoja.getCell(0, fila).getContents().toString(), 
  								 hoja.getCell(1, fila).getContents().toString());
								
						tuplas.add(tupla);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("ReadExcelTupla, cargadas: "+tuplas.size());
		return tuplas;
	}


}
