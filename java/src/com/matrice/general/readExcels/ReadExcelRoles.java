package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Roles;

public class ReadExcelRoles {

	public static List<Roles> leerArchivoExcel(String archivoDestino) {
		List<Roles> roles = new ArrayList<Roles>();
		try {
			Workbook archivo = Workbook.getWorkbook(new File(archivoDestino));

			Sheet hoja1 = archivo.getSheet(0);
			int numFilas1 = hoja1.getRows();
			Roles rol = new Roles();
			List<String> permisos = new ArrayList<String>();
			String nombreRolCargando = "";
			if (hoja1.getName().equals("roles")) {

				for (int fila = 1; fila < numFilas1; fila++) {
					// Recorre cada fila de la hoja
					String nombreRol = hoja1.getCell(0, fila).getContents()
							.toString();
					if (!nombreRol.equals(nombreRolCargando)) { // nuevo r
						if (permisos.size() != 0) {
							rol.setPermisos(permisos);
							roles.add(rol);
						}
						rol = new Roles(nombreRol);
						permisos = new ArrayList<String>();
						nombreRolCargando=nombreRol;
					}
					// Agregar permiso
					permisos.add(hoja1.getCell(1, fila).getContents()
							.toString());
				}
				// ultimo rol
				if (permisos.size() != 0) {
					rol.setPermisos(permisos);
					roles.add(rol);
				}
				
			}
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Roles "+roles.size());
		return roles;
	}
}