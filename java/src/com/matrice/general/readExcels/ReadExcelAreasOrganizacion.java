package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.AreasOrganizacion;

public class ReadExcelAreasOrganizacion {
	
	public static List<AreasOrganizacion> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<AreasOrganizacion> areas = new ArrayList<AreasOrganizacion>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					AreasOrganizacion area;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						area = new AreasOrganizacion 
								(hoja.getCell(0, fila).getContents(), 
  								 hoja.getCell(1, fila).getContents(), 
								 hoja.getCell(2, fila).getContents());
								
						areas.add(area);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Areas cargadas "+areas.size());
		return areas;
	}


}