package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.TipoBienConsumo;

public class ReadExcelTipoBienConsumo {
	
	public static List<TipoBienConsumo> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<TipoBienConsumo> tipos = new ArrayList<TipoBienConsumo>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					TipoBienConsumo tipo;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						tipo = new TipoBienConsumo 
								(hoja.getCell(0, fila).getContents(), 
  								 hoja.getCell(1, fila).getContents(), 
								 hoja.getCell(2, fila).getContents(), 
								 hoja.getCell(3, fila).getContents(), 
								 hoja.getCell(4, fila).getContents(), 
								 hoja.getCell(5, fila).getContents(), 
								 hoja.getCell(6, fila).getContents()
								 );
								
						tipos.add(tipo);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println(">>>  Tipos de bien de consumo cargados: "+tipos.size());
		return tipos;
	}


}