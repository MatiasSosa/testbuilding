package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.BienDeUso;

public class ReadExcelBienesDeUso {
	
	public static List<BienDeUso> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<BienDeUso> bienes = new ArrayList<BienDeUso>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				System.out.println("Nombre de la Hoja\t"
						+ archivoExcel.getSheet(sheetNo).getName());
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					ok = true;
					BienDeUso bien;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						bien = new BienDeUso 
								(hoja.getCell(0, fila).getContents(), 
  								 hoja.getCell(1, fila).getContents(), 
								 hoja.getCell(2, fila).getContents(),
								 hoja.getCell(3, fila).getContents(), 
  								 hoja.getCell(4, fila).getContents(), 
								 hoja.getCell(5, fila).getContents(),
								 hoja.getCell(6, fila).getContents(),
								 hoja.getCell(7, fila).getContents(),
								 hoja.getCell(8, fila).getContents(),
								 hoja.getCell(9, fila).getContents(),
								 hoja.getCell(10, fila).getContents()
								 );
								
						bienes.add(bien);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Bienes "+bienes.size());
		return bienes;
	}


}