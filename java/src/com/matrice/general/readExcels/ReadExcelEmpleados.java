package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Empleados;

public class ReadExcelEmpleados {
	
	public static List<Empleados> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Empleados> empleados = new ArrayList<Empleados>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Empleados empleado;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						empleado = new Empleados 
								(hoja.getCell(0, fila).getContents(), 
  								 hoja.getCell(1, fila).getContents(), 
								 hoja.getCell(2, fila).getContents(), 
								 hoja.getCell(3, fila).getContents(), 
  								 hoja.getCell(4, fila).getContents(), 
								 hoja.getCell(5, fila).getContents(),
								 hoja.getCell(6, fila).getContents(), 
  								 hoja.getCell(7, fila).getContents(), 
								 hoja.getCell(8, fila).getContents(),
								 hoja.getCell(9, fila).getContents(), 
  								 hoja.getCell(10, fila).getContents(), 
								 hoja.getCell(11, fila).getContents(),
								 hoja.getCell(12, fila).getContents(), 
  								 hoja.getCell(13, fila).getContents(), 
								 hoja.getCell(14, fila).getContents() );
								
						empleados.add(empleado);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Empleados: "+empleados.size());
		return empleados;
	}


}