package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Ciudad;

public class ReadExcelCiudades {
	
	public static List<Ciudad> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Ciudad> provincias = new ArrayList<Ciudad>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Ciudad provincia;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						provincia = new Ciudad(
								hoja.getCell(0, fila).getContents().toString(), 
  								hoja.getCell(1, fila).getContents().toString(),
  								hoja.getCell(2, fila).getContents().toString());
								
						provincias.add(provincia);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Ciudades cargadas "+provincias.size());
		return provincias;
	}


}
