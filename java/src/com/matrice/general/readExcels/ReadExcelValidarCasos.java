package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Casos;
import dto.DatoRevisarCompletar;
import dto.OpcionMenuValidar;

public class ReadExcelValidarCasos {
	
	public static List<OpcionMenuValidar> leerArchivoExcel(String archivoDestino) {
		List<OpcionMenuValidar> opcionesMenu = new ArrayList<OpcionMenuValidar>();
		try {
			Workbook archivoOpciones = Workbook.getWorkbook(new File(archivoDestino));

				Sheet hoja1 = archivoOpciones.getSheet(0);
				int numFilas1 = hoja1.getRows();
				Sheet hoja2 = archivoOpciones.getSheet(1);
				int numFilas2 = hoja2.getRows();
				int caso_num, caso_num_ant = 0;
				String tipoDato = "";
				
				if (hoja1.getName().equals("opcionMenu"))
				{
					OpcionMenuValidar opcionMenu;
					for (int fila = 1; fila < numFilas1; fila++) { 
					// Recorre cada fila de la hoja
						String opcion = hoja1.getCell(0, fila).getContents().toString();
						opcionMenu = new OpcionMenuValidar 
								(opcion,hoja1.getCell(1, fila).getContents());
						
						if (hoja2.getName().equals("casos")){
							
							DatoRevisarCompletar dat; // = new DatoRevisarCompletar();
							Casos caso = new Casos();
							
							for (int fila2 = 1; fila2 < numFilas2; fila2++) { 
								// Recorre cada fila de la hoja
								String opc = hoja2.getCell(0, fila2).getContents().toString();
								if(opcion.equals(opc)){
									caso_num = Integer.parseInt(hoja2.getCell(1, fila2).getContents());
									if(caso_num != caso_num_ant){ // nuevo caso
										if (caso.getListadoCompletar().size()!= 0 || 
												caso.getListadoRevisar().size() != 0){
											opcionMenu.setListado(caso);
										}
										caso = new Casos(caso_num);
									}

									dat = new DatoRevisarCompletar(
											hoja2.getCell(3, fila2).getContents().toString(),
											hoja2.getCell(4, fila2).getContents().toString(),
											hoja2.getCell(5, fila2).getContents().toString(),
											hoja2.getCell(6, fila2).getContents().toString(),
											hoja2.getCell(7, fila2).getContents().toString());
									tipoDato = hoja2.getCell(2, fila2).getContents();		
									if(tipoDato.equals("COMPLETAR")){
										caso.setListadoCompletar(dat);
									}else{
										caso.setListadoRevisar(dat);
									}
									caso_num_ant =caso_num; 
								}
								
							}//fin del for de casos
							
							// para el ultimo caso cargado
							if (caso.getListadoCompletar().size()!= 0 || 
									caso.getListadoRevisar().size()!= 0){
								opcionMenu.setListado(caso);
							}
						}
						opcionesMenu.add(opcionMenu);
					}
			}
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Opciones del menu "+opcionesMenu.size());
		return opcionesMenu;
	}


}