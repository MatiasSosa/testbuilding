package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Gasto;

public class ReadExcelGastos {
	
	public static List<Gasto> leerArchivoExcelGastosPeriodicos(String archivoDestino, String nombreHoja) {
		List<Gasto> gastos = new ArrayList<Gasto>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Gasto gasto;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						gasto = new Gasto(
								hoja.getCell(0, fila).getContents().toString(), 
  								hoja.getCell(1, fila).getContents().toString(),
  								hoja.getCell(2, fila).getContents().toString(),
  								hoja.getCell(3, fila).getContents().toString(),
  								hoja.getCell(4, fila).getContents().toString(),
  								hoja.getCell(5, fila).getContents().toString(),
  								hoja.getCell(6, fila).getContents().toString(),
  								hoja.getCell(7, fila).getContents().toString(),
  								hoja.getCell(8, fila).getContents().toString(),
  								hoja.getCell(9, fila).getContents().toString(),
  								hoja.getCell(10, fila).getContents().toString(),
  								hoja.getCell(11, fila).getContents().toString(),
  								hoja.getCell(12, fila).getContents().toString(),
  								hoja.getCell(13, fila).getContents().toString(),
  								hoja.getCell(14, fila).getContents().toString()
  								);
								
						gastos.add(gasto);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Gastos cargados "+gastos.size());
		return gastos;
	}

	public static List<Gasto> leerArchivoExcelGastosUnicos(String archivoDestino, String nombreHoja) {
		List<Gasto> gastos = new ArrayList<Gasto>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Gasto gasto;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						gasto = new Gasto(
								hoja.getCell(0, fila).getContents().toString(), 
  								hoja.getCell(1, fila).getContents().toString(),
  								hoja.getCell(2, fila).getContents().toString(),
  								hoja.getCell(3, fila).getContents().toString(),
  								hoja.getCell(4, fila).getContents().toString(),
  								hoja.getCell(5, fila).getContents().toString(),
  								hoja.getCell(6, fila).getContents().toString(),
  								hoja.getCell(7, fila).getContents().toString(),
  								hoja.getCell(8, fila).getContents().toString(),
  								hoja.getCell(9, fila).getContents().toString(),
  								hoja.getCell(10, fila).getContents().toString(),
  								hoja.getCell(11, fila).getContents().toString(),
  								hoja.getCell(12, fila).getContents().toString(),
  								hoja.getCell(13, fila).getContents().toString(),
  								hoja.getCell(14, fila).getContents().toString(),
  								hoja.getCell(15, fila).getContents().toString(),
  								hoja.getCell(16, fila).getContents().toString(),
  								hoja.getCell(17, fila).getContents().toString(),
  								hoja.getCell(18, fila).getContents().toString()
  								);
								
						gastos.add(gasto);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Gastos unicos cargados "+gastos.size());
		return gastos;
	}

}
