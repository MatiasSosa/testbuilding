package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Zona;

public class ReadExcelZonas {
	
	public static List<Zona> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Zona> zonas = new ArrayList<Zona>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Zona zona;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						zona = new Zona(
								hoja.getCell(0, fila).getContents().toString(), 
  								hoja.getCell(1, fila).getContents().toString(),
  								hoja.getCell(2, fila).getContents().toString(),
  								hoja.getCell(3, fila).getContents().toString());
								
						zonas.add(zona);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Zonas cargadas "+zonas.size());
		return zonas;
	}


}
