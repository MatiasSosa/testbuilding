package com.matrice.general.readExcels;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.TipoGastos;

public class ReadExcelTipoGastos {
	
	public static List<TipoGastos> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<TipoGastos> tipos = new ArrayList<TipoGastos>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();
				System.out.println("Nombre de la Hoja\t"
						+ archivoExcel.getSheet(sheetNo).getName());
				
				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					ok = true;
					TipoGastos tipo;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						tipo = new TipoGastos 
								(hoja.getCell(0, fila).getContents(), 
  								 hoja.getCell(1, fila).getContents(), 
								 hoja.getCell(2, fila).getContents(), 
								 hoja.getCell(3, fila).getContents());
								
						tipos.add(tipo);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Tipos cargados "+tipos.size());
		return tipos;
	}


}