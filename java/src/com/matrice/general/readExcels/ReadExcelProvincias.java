package com.matrice.general.readExcels;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import dto.Provincia;

public class ReadExcelProvincias {
	
	public static List<Provincia> leerArchivoExcel(String archivoDestino, String nombreHoja) {
		List<Provincia> provincias = new ArrayList<Provincia>();
		try {
			Workbook archivoExcel = Workbook.getWorkbook(new File(
					archivoDestino));
			boolean ok = false;
			
			for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets() && !ok; sheetNo++) 
			// Recorre cada hoja

			{
				Sheet hoja = archivoExcel.getSheet(sheetNo);
				int numFilas = hoja.getRows();

				if (archivoExcel.getSheet(sheetNo).getName().equals(nombreHoja)){
					System.out.println("Nombre de la Hoja\t"
							+ archivoExcel.getSheet(sheetNo).getName());
					ok = true;
					Provincia provincia;
					for (int fila = 1; fila < numFilas; fila++) { 
					// Recorre cada fila de la hoja
						provincia = new Provincia(
								hoja.getCell(0, fila).getContents().toString(), 
  								 hoja.getCell(1, fila).getContents().toString());
								
						provincias.add(provincia);

						}
						
				}
			}
			
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		System.out.println("Provincias cargadas "+provincias.size());
		return provincias;
	}


}
