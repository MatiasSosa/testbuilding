package com.matrice.general;

import org.junit.Test;
import org.openqa.selenium.By;

public class LoginAdmin extends MatriceTest {
	
	@Test
	  public void testLoginAdmin() throws Exception {

	    driver.get(baseUrl + "/public/admin/user/login");
	    driver.findElement(By.id("email")).click();
	    driver.findElement(By.id("email")).clear();
	    driver.findElement(By.id("email")).sendKeys("admin");
	    driver.findElement(By.id("password")).click();
	    driver.findElement(By.id("password")).clear();
	    driver.findElement(By.id("password")).sendKeys("admin");
	    driver.findElement(By.id("btn_login")).click();
    
	  }
}
