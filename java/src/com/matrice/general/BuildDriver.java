package com.matrice.general;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BuildDriver {
	
	private static WebDriver DRIVER;
//	  private boolean acceptNextAlert = true;
//	  private StringBuffer verificationErrors = new StringBuffer();
//	  private static ChromeDriverService service;

	  
	  public static WebDriver getInstance() throws IOException{
		  if (DRIVER == null) {
			  //Firefox
			  DRIVER = new FirefoxDriver();	
			  
			  //Remote Firefox prender webdriver y asegurarse de saltear proxy en IE y Firefox
			  //Selenium selenium = new DefaultSelenium("localhost", 4444, "*firefox", "http://www.google.com");
			  //DesiredCapabilities capability = DesiredCapabilities.firefox();
			  //capability.setVersion("23.0.1");
			  //DRIVER = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
			  
			  //Chrome 
			  //DRIVER = new RemoteWebDriver(new URL("http://localhost:9515"), DesiredCapabilities.chrome());

			  //IE
			  //DRIVER = new RemoteWebDriver(new URL("http://localhost:5555"),DesiredCapabilities.internetExplorer());
			  //capability.setVersion("8");
			  DRIVER.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			  }
		  return DRIVER;
	  }
		  
		  public static void close(){
			  DRIVER.quit();
			  DRIVER = null;
		  } 
}
