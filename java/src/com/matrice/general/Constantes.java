package com.matrice.general;

public class Constantes {
	
	public static final String BASE_URL = "http://building.matriceconsulting.com.ar/building";
	public static final String PATH_SRC = "C:/Dropbox/Building/Pruebas/testbuilding/java/src/";
	
	public static final String PATH_IMAGES = "C:\\Dropbox\\Building\\Pruebas\\testbuilding\\java\\src\\imagenes\\";
	//public static final String PATH_IMAGES = "C:\\Dropbox\\Building\\info-psa\\relevamiento\\imagenes\\";
			
	public static final String PATH_DATA = "C:\\Dropbox\\Building\\Pruebas\\testbuilding\\java\\src\\data\\";
	//public static final String PATH_DATA = "C:\\Dropbox\\Building\\info-psa\\relevamiento\\planillas\\";
	
	public static final String NAME_ARCH_MOD_CONFIGURACION = "configuracion.xls";
	//public static final String NAME_ARCH_MOD_CONFIGURACION = "plantilla_modulo_configuracion.xls";

	public static final String NAME_ARCH_MOD_GESTION = "gestion.xls";
	//public static final String NAME_ARCH_MOD_GESTION = "plantilla_modulo_gestion.xls";
	
	public static final String NAME_ARCH_MOD_ADMINISTRACION = "administracion.xls";
	public static final String NAME_ARCH_INMUEBLES = "inmuebles.xls";
	public static final String NAME_ARCH_PEDIDOS = "Pedidos.xls";
	public static final String NAME_ARCH_ALTA_DATOS = "alta-datos.xls";
	public static final String NAME_ARCH_GASTOS = "gastos.xls";

	
}
