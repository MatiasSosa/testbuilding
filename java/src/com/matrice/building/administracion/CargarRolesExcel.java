package com.matrice.building.administracion;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelRoles;

import dto.Roles;

import com.matrice.general.Constantes;
public class CargarRolesExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testCargarRolesExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA+Constantes.NAME_ARCH_MOD_ADMINISTRACION;
   List<Roles> roles = ReadExcelRoles.leerArchivoExcel(archivoDestino);

   if(roles.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Roles rol: roles){
	   			driver.get(baseUrl + "/public/admin/roles");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(rol.getRol());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("roles_processing")))));
	   		 
	   			noExiste = driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Crear Rol")));
		   		    driver.findElement(By.linkText("Crear Rol")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(rol.getRol());
		   		    driver.findElement(By.cssSelector("input.btn.btn-primary")).click();
		   		    
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    System.out.println("Se agrego Rol: "+rol.getRol());
		   		    if(rol.getPermisos().size()!=0){
			   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
			   		    driver.findElement(By.id("table_filter")).sendKeys(rol.getRol());
			   		    
			   		    wait.until(ExpectedConditions.not(ExpectedConditions
			   		    	.visibilityOf(driver.findElement(By.id("roles_processing")))));
		   		    	
			   		    driver.findElement(By.id("btn_edit")).click();
			   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
			   		    driver.findElement(By.linkText("Permisos")).click();
			   		    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@id='permissions[1]'])[2]")));

			   		    for (String permiso: rol.getPermisos()){
				   		   driver.findElement(By.xpath("(//input[@id='"+permiso+"'])[2]")).click();
			   		    }
			   		    
			   		    driver.findElement(By.cssSelector("input.btn.btn-primary")).click();
			   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
			   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Rol actualizado con exito[\\s\\S]*$"));
		   		    }
	   			}else{
	   				System.out.println("El rol "+rol.getRol()+" ya existe");
	   			}
	   		}
   }   		
  }
}
