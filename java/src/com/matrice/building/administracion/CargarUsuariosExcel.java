package com.matrice.building.administracion;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelUsuarios;

import dto.Usuarios;

public class CargarUsuariosExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testCargarUsuariosExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA+Constantes.NAME_ARCH_MOD_ADMINISTRACION;
   List<Usuarios> usuarios = ReadExcelUsuarios.leerArchivoExcel(archivoDestino,"usuarios");

   if(usuarios.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Usuarios usuario: usuarios){
	   			driver.get(baseUrl + "/public/admin/users");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(usuario.getUsuario());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("users_processing")))));
	   		 
	   			noExiste = driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Crear Usuario")));
		   		    driver.findElement(By.linkText("Crear Usuario")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("username")));
		   		    
		   		    driver.findElement(By.id("username")).clear();
		   		    driver.findElement(By.id("username")).sendKeys(usuario.getUsuario());
		   		    driver.findElement(By.id("email")).clear();
		   		    driver.findElement(By.id("email")).sendKeys(usuario.getEmail());
		   		    driver.findElement(By.id("password")).clear();
		   		    driver.findElement(By.id("password")).sendKeys(usuario.getContrasenia());
		   		    driver.findElement(By.id("password_confirmation")).clear();
		   		    driver.findElement(By.id("password_confirmation")).sendKeys(usuario.getConfirmarContrasenia());
		   		    new Select(driver.findElement(By.id("confirm"))).selectByVisibleText(usuario.getActivado());
		   		    new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div[1]/div/div[6]/div/select"))).selectByVisibleText(usuario.getRol());
		   		    driver.findElement(By.cssSelector("input.btn.btn-primary")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    System.out.println("Se agrego usuario: "+usuario.getUsuario());
	   			}else{
	   				System.out.println("El usuario "+usuario.getUsuario()+" ya existe");
	   			}
	   		}
   }   		
  }
}
