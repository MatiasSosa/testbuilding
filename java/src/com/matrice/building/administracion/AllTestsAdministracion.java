package com.matrice.building.administracion;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	CargarRolesExcel.class,
	CargarUsuariosExcel.class
})
public class AllTestsAdministracion {

}
