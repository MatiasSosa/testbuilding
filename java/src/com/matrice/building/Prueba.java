package com.matrice.building;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.building.gastos.periodicos.CargarGastosUnicosExcel;
import com.matrice.general.LoginAdmin;
import com.matrice.general.TestCerrar;


@RunWith(Suite.class)
@SuiteClasses({
	// Test Iniciar sesion
	LoginAdmin.class, 
	// Tests de funcionalidad
	CargarGastosUnicosExcel.class,

	// Cerrar
	TestCerrar.class })


public class Prueba {
}
