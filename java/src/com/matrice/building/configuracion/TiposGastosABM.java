package com.matrice.building.configuracion;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class TiposGastosABM extends MatriceTest {

  private Long num = calendario.getTimeInMillis();
  private String tipoGastoNombre = "tipoGastoGenerico_"+num;
  private String tipoGastoNombreEditado = "tipoGastoEditado_"+num;
  @Test
  public void testTiposGastosABM() throws Exception {
   
    driver.get(baseUrl + "/public/param/expenseTypes/index");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(tipoGastoNombre);
    
    new Select(driver.findElement(By.id("expenseTypeClassification"))).selectByVisibleText("Servicio");
    new Select(driver.findElement(By.id("goodType"))).selectByValue("2");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Tipo de Gasto creado con exito[\\s\\S]*$"));

	//busca y edita
    driver.get(baseUrl + "/public/param/expenseTypes");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(tipoGastoNombre);
        
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("expenseTypes_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));
    Thread.sleep(2000);    
    driver.findElement(By.id("btn_edit")).click();
    
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(tipoGastoNombreEditado);
    new Select(driver.findElement(By.id("expenseTypeClassification"))).selectByVisibleText("Otro");
    new Select(driver.findElement(By.id("goodType"))).selectByValue("4");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*con exito[\\s\\S]*$"));
    
	//valido que el nombre modificado no aparece mas
	driver.findElement(By.id("table_filter")).clear();
	driver.findElement(By.id("table_filter")).sendKeys(tipoGastoNombre);
	wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("expenseTypes_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$"));   

	//busco y borro el editado
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(tipoGastoNombreEditado);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("expenseTypes_processing")))));
    Thread.sleep(2000);
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));   
    Thread.sleep(2000);

    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));        
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*eliminado con exito[\\s\\S]*$"));

    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(tipoGastoNombreEditado);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("expenseTypes_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$"));
    }
}
