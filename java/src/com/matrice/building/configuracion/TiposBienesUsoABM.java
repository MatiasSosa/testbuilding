package com.matrice.building.configuracion;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class TiposBienesUsoABM extends MatriceTest {
  private Long num = calendario.getTimeInMillis();
  private String tipoBienesNombre = "tipoBienesGenerico_"+num;
  private String tipoBienesNombreEditado= "tipoBienesGenerico_editado_"+num;
  
  @Test
  public void testTiposBienesUsoABM() throws Exception {

	driver.get(baseUrl + "/public/param/assetTypes/index");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    
    // validar mensajes de datos requeridos
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Nombre es requerido[\\s\\S]*$"));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Rubro es requerido[\\s\\S]*$"));

	//Cargar datos
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(tipoBienesNombre);
    new Select(driver.findElement(By.id("sector"))).selectByVisibleText("LibreriaTest");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*creado con exito[\\s\\S]*$"));

    // busco registro creado
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(tipoBienesNombre);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("assetTypes_processing")))));
    Thread.sleep(2000);
    
    //actualizo sus datos
    driver.findElement(By.id("btn_edit")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
	driver.findElement(By.id("name")).clear();
	driver.findElement(By.id("name")).sendKeys(tipoBienesNombreEditado);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*actualizado con exito[\\s\\S]*$"));

    // valido que no pueda cargar otro elemento con el mismo nombre
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(tipoBienesNombreEditado);
    new Select(driver.findElement(By.id("sector"))).selectByVisibleText("LibreriaTest");
    driver.findElement(By.id("btn_accept")).click();
    
    // validar mensajes de nombre ocupado
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Nombre se encuentra ocupado[\\s\\S]*$"));

	driver.findElement(By.id("link_back")).click();

    // busco registro creado
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(tipoBienesNombreEditado);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("assetTypes_processing")))));
    Thread.sleep(2000);
    //elimino el elemento
    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*eliminado con exito[\\s\\S]*$"));
    
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(tipoBienesNombreEditado);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("assetTypes_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$"));
  }
}
