package com.matrice.building.configuracion;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class ResultadosMantenimientoABM extends MatriceTest {

  private Long num = calendario.getTimeInMillis();
  private String nombreResultado = "ResultadoMantenimientoGenerico_"+num;
  private String nombreResultadoEditado = "ResultadoMantenimientoEditado_"+num;
  @Test
  public void testResultadosMantenimientoABM() throws Exception {
   
	driver.get(baseUrl + "/public/param/maintenanceResults");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    // validar mensajes de datos requeridos
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Nombre es requerido[\\s\\S]*$"));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Color es requerido[\\s\\S]*$"));

	// cargar datos
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreResultado);
    ((JavascriptExecutor) driver).executeScript ("document.getElementById ('color').removeAttribute('readonly',0);");
    driver.findElement(By.id("color")).sendKeys("#18c71b");
    driver.findElement(By.id("name")).click();
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Resultado de mantenimiento creado con exito.[\\s\\S]*$"));    

    //validar que el nombre este ocupado
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreResultado);
    ((JavascriptExecutor) driver).executeScript ("document.getElementById ('color').removeAttribute('readonly',0);");
    driver.findElement(By.id("color")).sendKeys("#65b3e0");
    driver.findElement(By.id("name")).click();
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Nombre se encuentra ocupado[\\s\\S]*$"));

	//busca y edita
	driver.get(baseUrl + "/public/param/maintenanceResults");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(nombreResultado);
        
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("maintenanceResults_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));
    Thread.sleep(2000);    
    driver.findElement(By.id("btn_edit")).click();
    
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreResultadoEditado);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*con exito[\\s\\S]*$"));
    
	//valido que el nombre modificado no aparece mas
	driver.findElement(By.id("table_filter")).clear();
	driver.findElement(By.id("table_filter")).sendKeys(nombreResultado);
	wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("maintenanceResults_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$"));   

	//busco y borro el editado
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(nombreResultadoEditado);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("maintenanceResults_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));   
    Thread.sleep(2000);

    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));        
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*con exito[\\s\\S]*$"));

    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(nombreResultadoEditado);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("maintenanceResults_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$"));
    }
}
