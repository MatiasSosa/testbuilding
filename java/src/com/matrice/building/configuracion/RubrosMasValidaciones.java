package com.matrice.building.configuracion;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class RubrosMasValidaciones extends MatriceTest {

  private Long num = calendario.getTimeInMillis();
  private String rubro1 = "rubroGenerico1_"+num;
  private String rubro2 = "rubroGenerico2_"+num;

  @Test
  public void testRubrosMasValidaciones() throws Exception {
   
	driver.get(baseUrl + "/public/param/sectors");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(rubro1);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*creado con exito[\\s\\S]*$"));

    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(rubro2);
    new Select(driver.findElement(By.id("sectorFather"))).selectByVisibleText(rubro1);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*creado con exito[\\s\\S]*$"));

	//rubro1 no se debe poder borrar
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(rubro1);
        
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("sectors_processing")))));

    Thread.sleep(2000);    
    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));        
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("session_error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El rubro tiene rubros asociados[\\s\\S]*$"));

	
	
    }
}
