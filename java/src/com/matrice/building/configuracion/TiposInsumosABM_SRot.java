package com.matrice.building.configuracion;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
/*
 * Pruebas de Tipos de Bien de Consumo sin rotulacion  
 */
public class TiposInsumosABM_SRot extends MatriceTest {
	
  private Long num = calendario.getTimeInMillis();
  private String nombreTipoInsumo = "insumo_"+num;
  private String nombreTipoInsumoEditado = "insumo_editado_"+num;
  
  @Test
  public void testTiposInsumos() throws Exception {
    driver.get(baseUrl + "/public/param/commodityTypes");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    
    // validar mensajes de datos requeridos
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Nombre es requerido[\\s\\S]*$"));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Rubro es requerido[\\s\\S]*$"));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Descripcion es requerido[\\s\\S]*$"));    
    
	// cargar datos
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreTipoInsumo);
    new Select(driver.findElement(By.id("sector"))).selectByVisibleText("Escritura");
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Descripción del insumo "+num);
    driver.findElement(By.id("picture")).sendKeys(Constantes.PATH_IMAGES+"bic negra.jpg");

    driver.findElement(By.id("initialStock__int")).clear();
    driver.findElement(By.id("initialStock__int")).sendKeys("100");
    driver.findElement(By.id("minimumStock__int")).clear();
    driver.findElement(By.id("minimumStock__int")).sendKeys("20");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Tipo de Bien de Consumo creado con exito[\\s\\S]*$"));
    
    
    // busco registro creado
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(nombreTipoInsumo);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("commodityTypes_processing")))));
    Thread.sleep(2000);
    
    //actualizo sus datos
    driver.findElement(By.id("btn_edit")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
	driver.findElement(By.id("name")).clear();
	driver.findElement(By.id("name")).sendKeys(nombreTipoInsumoEditado);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Tipo de Bien de Consumo actualizado con exito[\\s\\S]*$"));

    // valido que no pueda cargar otro elemento con el mismo nombre
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreTipoInsumoEditado);
    new Select(driver.findElement(By.id("sector"))).selectByVisibleText("Escritura");
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Descripción del insumo "+num);
    driver.findElement(By.id("btn_accept")).click();
    
    // validar mensajes de nombre ocupado
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Nombre se encuentra ocupado[\\s\\S]*$"));

	driver.findElement(By.id("link_back")).click();

    // busco registro editado
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(nombreTipoInsumoEditado);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("commodityTypes_processing")))));
    Thread.sleep(2000);
    
    //elimino el elemento
    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El tipo de bien de consumo fue eliminado con exito[\\s\\S]*$"));
  }
}

