package com.matrice.building.configuracion;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelValidarCasos;

import dto.Casos;
import dto.DatoRevisarCompletar;
import dto.OpcionMenuValidar;

public class ValidarAltasExcel extends MatriceTest {
	
	private Long num = calendario.getTimeInMillis();
	
	@Test
	public void testValidarAltasExcel() throws Exception {

		String archivoDestino = Constantes.PATH_DATA+Constantes.NAME_ARCH_ALTA_DATOS;
		List<OpcionMenuValidar> opcionesMenu = ReadExcelValidarCasos.leerArchivoExcel(archivoDestino);

		if (opcionesMenu.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (OpcionMenuValidar opcionMenu : opcionesMenu) {
				System.out.println("OPCION " + opcionMenu.getOpcionMenu());
				if(opcionMenu.getListado().size()!=0){
					for (Casos caso : opcionMenu.getListado()) {
						driver.get(baseUrl + opcionMenu.getUrl());
						System.out.println("Nro caso: " + caso.getCaso_num());
						wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
						driver.findElement(By.id("link_create")).click();
						
						if(caso.getListadoCompletar().size()!=0){
							System.out.println("For caso.getListadoCompletar(): "+caso.getListadoCompletar().size());
							for (DatoRevisarCompletar datoCompletar : caso.getListadoCompletar()) {
								System.out.println("Completar:  "+datoCompletar.getCampo()
										+"   Valor: "+datoCompletar.getValor());
							      switch ( datoCompletar.getTipo() ) {
							      case "INPUT":
							    	   driver.findElement(By.id(datoCompletar.getCampo())).clear();
							    	   driver.findElement(By.id(datoCompletar.getCampo()))
							    	   			.sendKeys(datoCompletar.getValor()+"_"+num);
							           break;
							      case "COMBO":
							    	   new Select(driver.findElement(By.id(datoCompletar.getCampo())))
							    	   			.selectByVisibleText(datoCompletar.getValor());
							           break;
							      case "FECHA":
							   			((JavascriptExecutor) driver).executeScript 
							   				("document.getElementById ('"+datoCompletar.getCampo()+"').removeAttribute('readonly',0);");
							   		    driver.findElement(By.id(datoCompletar.getCampo()))
							   		    		.sendKeys(datoCompletar.getValor()); 					           
							   		    break;
							      case "CHECK":
							    	    driver.findElement(By.id(datoCompletar.getCampo())).click();
							   		    break;
							      default:
							           System.out.println("No se encontro tipo" );
							           break;
							      }
							}
							
						}
						
					    driver.findElement(By.id("btn_accept")).click();
					    if(caso.getListadoRevisar().size()!=0){
							System.out.println("For caso.getListadoRevisar(): "+caso.getListadoRevisar().size());
							wait.until(ExpectedConditions.elementToBeClickable
									(By.id(caso.getListadoRevisar().get(0).getContenedorMensaje())));
							for (DatoRevisarCompletar mensajeRevisar : caso.getListadoRevisar()) {
							    System.out.println("Mensaje:  "+mensajeRevisar.getMensaje());
								assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
										.matches("^[\\s\\S]*"+mensajeRevisar.getMensaje()+"[\\s\\S]*$"));
							}
					    	
					    }
					}
				}

			}
		}

	}
}
