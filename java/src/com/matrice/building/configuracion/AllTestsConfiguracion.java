package com.matrice.building.configuracion;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	TiposInsumosABM_SRot.class,
	TiposInsumosABM_CRot.class,
	TiposBienesUsoABM.class, 
	TiposGastosABM.class, 
	RubrosABM.class,
	RubrosMasValidaciones.class,
	TiposEspacioABM.class,
	ProveedoresABM.class,
	ResultadosMantenimientoABM.class,
	GravedadABM.class, 
	EstadosPedidosABM.class, 
	TiposMantenimientoABM.class,
	Cargos.class,
	ValidarAltasExcel.class
  })
public class AllTestsConfiguracion {

}
