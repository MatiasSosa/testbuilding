package com.matrice.building.gestion.areas;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	ValidarAgregarAreasExcel.class,
	ValidarAltaEdicionGenerica.class,
	ValidarAltaEliminacionGenerica.class
  })

public class AllAreas {
	
}
