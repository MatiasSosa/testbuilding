package com.matrice.building.gestion.areas;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class ValidarAltaEdicionGenerica extends MatriceTest {
  
  private String nombreArea = "AREA GENERICA TESTCASE " + new Date();
  
  @Test
  public void testValidarAltaEdicionGenerica() throws Exception {
   
	driver.get(baseUrl + "/public/employee/areas");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("btn_accept")).click();
    // validar mensajes de error
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre es requerido[\\s\\S]*$"));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreArea);
    driver.findElement(By.id("btn_accept")).click();
    // validar guardado con exito
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
    
    //actualizar registro
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(nombreArea);
    
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("areas_processing")))));
    
	// los 2 seg extras despues del wait son para evitar error de JSON al
	// parsear la data de la grilla
    Thread.sleep(2000);  
    driver.findElement(By.id("btn_edit")).click();

    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    nombreArea = "AREA EDITADA TESTCASE " + new Date();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreArea);
    driver.findElement(By.id("btn_accept")).click();
    // validar guardado con exito
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
  
    
   }   		

}
