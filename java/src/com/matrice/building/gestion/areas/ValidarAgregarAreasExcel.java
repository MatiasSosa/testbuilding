package com.matrice.building.gestion.areas;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelAreasOrganizacion;

import dto.AreasOrganizacion;

public class ValidarAgregarAreasExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarAreasExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;

   List<AreasOrganizacion> areas = ReadExcelAreasOrganizacion.leerArchivoExcel(archivoDestino,"areas");

   if(areas.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(AreasOrganizacion area: areas){
	   			driver.get(baseUrl + "/public/employee/areas");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(area.getNombre());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("areas_processing")))));
	   		    Thread.sleep(2000);
	   			noExiste = driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(area.getNombre());
		   		    if(!area.getPadre().equals("-1")){
		   		    	new Select(driver.findElement(By.id("areaFather")))
		   		    		.selectByVisibleText(area.getPadre());
		   		    }
		   		    if(!area.getResponsable().equals("-1")){
		   		    	new Select(driver.findElement(By.id("employee")))
			   		   		.selectByVisibleText(area.getResponsable());
			   		}
		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego area "+area.getNombre());
		   		    
	   			}else{
	   				System.out.println("El area "+area.getNombre()+" ya existe");
	   			}
	   		}
   }   		
  }
}
