package com.matrice.building.gestion.inmuebles;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
	CrearInmuebleEdificioTest.class,
	BuscarEdificioTest.class,
	CrearPisoValidaciones.class,
	BuscarEdificioTest.class,
	CrearEspacio.class,
	BorrarEspacio.class,
	BorrarPiso1.class,
	EliminarEdificioTest.class
	})

public class SuiteGestionarEspacios {
	  
	}



