package com.matrice.building.gestion.inmuebles;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.building.gestion.empleados.BorrarEmpleadoEdificioTest;
import com.matrice.building.gestion.empleados.CrearEmpleadoEdificioTest;
import com.matrice.building.gestion.empleados.ValidarNoEliminarInmuebleEmpleados;


@RunWith(Suite.class)
@SuiteClasses({
	CrearInmuebleEdificioTest.class,
	CrearEmpleadoEdificioTest.class,
	BuscarEdificioTest.class,
	ValidarNoEliminarInmuebleEmpleados.class,
	BorrarEmpleadoEdificioTest.class,
	EliminarEdificioTest.class
	})

public class SuiteValidarNoEliminarInmuebleConEmpleados {

	
	}



