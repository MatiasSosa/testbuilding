package com.matrice.building.gestion.inmuebles;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.building.gestion.mantenimientos.BorrarMantenimientoEdificioTest;
import com.matrice.building.gestion.mantenimientos.CrearMantenimientoEdificioTest;
import com.matrice.building.gestion.mantenimientos.ValidarNoEliminarInmuebleConMantenimiento;


@RunWith(Suite.class)
@SuiteClasses({
	CrearInmuebleEdificioTest.class,
	CrearMantenimientoEdificioTest.class,
	BuscarEdificioTest.class,
	ValidarNoEliminarInmuebleConMantenimiento.class,
	BorrarMantenimientoEdificioTest.class,
	EliminarEdificioTest.class
	})

public class SuiteValidarNoEliminarInmuebleConMantenimientos {

	
	}



