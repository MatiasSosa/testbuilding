package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class EditarInmuebles extends MatriceTest {
  
  @Test
  public void testEditarInmuebles() throws Exception {
    driver.get(baseUrl + "/publics/index");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    driver.findElement(By.id("btn_accept")).click();
    // Warning: waitForTextPresent may require manual changes
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Verifique los siguientes errores:[\\s\\S]*$")) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre es requerido\\.[\\s\\S]*$"));
    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Zona es requerido\\.[\\s\\S]*$"));
    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Direccion es requerido\\.[\\s\\S]*$"));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Edificio1Test");
    new Select(driver.findElement(By.id("country"))).selectByVisibleText("China");
    Thread.sleep(6000);
    new Select(driver.findElement(By.id("state"))).selectByVisibleText("Beijing");
    Thread.sleep(6000);
    new Select(driver.findElement(By.id("city"))).selectByVisibleText("Fangshan");
    Thread.sleep(6000);
    new Select(driver.findElement(By.id("zone"))).selectByVisibleText("CentroTest");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Calle 4");
    driver.findElement(By.id("btn_accept")).click();
    driver.findElement(By.id("link_create")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Edificio2Test");
    new Select(driver.findElement(By.id("country"))).selectByVisibleText("China");
    Thread.sleep(6000);
    new Select(driver.findElement(By.id("state"))).selectByVisibleText("Beijing");
    Thread.sleep(6000);
    new Select(driver.findElement(By.id("city"))).selectByVisibleText("Fangshan");
    Thread.sleep(6000);
    new Select(driver.findElement(By.id("zone"))).selectByVisibleText("CentroTest");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Calle 5");
    driver.findElement(By.id("measurement__int")).clear();
    driver.findElement(By.id("measurement__int")).sendKeys("200");
    driver.findElement(By.id("isOwner")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("(//input[@id='isRented'])[2]")).click();
    driver.findElement(By.id("btn_accept")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Edificio1Test");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_edit")).click();
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("link_create_floor")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Planta BajaTest");
    driver.findElement(By.id("mapPicture")).sendKeys("C:\\Dropbox\\Building\\Pruebas\\Imagenes\\plano.jpg");
    driver.findElement(By.id("btn_accept")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("link_create_floor")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Piso 1Test");
    driver.findElement(By.id("btn_accept")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("(//a[contains(text(),'Editar')])[2]")).click();
    driver.findElement(By.id("link_back")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    driver.findElement(By.id("link_create_space")).click();
    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("Planta BajaTest");
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Espacio ComunTest");
    new Select(driver.findElement(By.id("spaceType"))).selectByVisibleText("Espacio ComunTest");
    new Select(driver.findElement(By.id("areas"))).selectByVisibleText("MarketingTest");
    driver.findElement(By.id("measurement__int")).clear();
    driver.findElement(By.id("measurement__int")).sendKeys("20");
    driver.findElement(By.id("btn_accept")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("link_create_space")).click();
    driver.findElement(By.id("btn_accept")).click();
    // Warning: waitForTextPresent may require manual changes
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Verifique los siguientes errores:[\\s\\S]*$")) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre es requerido\\.[\\s\\S]*$"));
    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Piso es requerido\\.[\\s\\S]*$"));
    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Area es requerido\\.[\\s\\S]*$"));
    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("Piso 1Test");
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Oficina GerenciaTest");
    new Select(driver.findElement(By.id("spaceType"))).selectByVisibleText("OficinaTest");
    new Select(driver.findElement(By.id("areas"))).selectByVisibleText("Contabilidad y FinanzasTest");
    driver.findElement(By.id("measurement__int")).clear();
    driver.findElement(By.id("measurement__int")).sendKeys("22");
    driver.findElement(By.id("btn_accept")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("(//a[contains(text(),'Editar')])[2]")).click();
    driver.findElement(By.id("link_back")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("(//a[contains(text(),'Editar')])[4]")).click();
    driver.findElement(By.id("link_back")).click();
    driver.get(baseUrl + "/publics");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Edificio2Test");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_edit")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("EdificioBorrarTest");
    driver.findElement(By.id("btn_accept")).click();
    // Warning: waitForTextPresent may require manual changes
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*EdificioBorrarTest[\\s\\S]*$")) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    // Warning: assertTextNotPresent may require manual changes
    assertFalse(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Edificio2Test[\\s\\S]*$"));
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Edificio1Test");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    // Warning: waitForTextPresent may require manual changes
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Verifique los siguientes errores[\\s\\S]*$")) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El Inmueble tiene Pisos asociados[\\s\\S]*$"));
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("EdificioBorrarTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    // Warning: assertTextNotPresent may require manual changes
    assertFalse(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*EdificioBorrarTest[\\s\\S]*$"));
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Edificio1Test");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_edit")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    // Warning: waitForTextPresent may require manual changes
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Verifique los siguientes errores[\\s\\S]*$")) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El Piso tiene Espacios asociados[\\s\\S]*$"));
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("(//a[contains(text(),'Editar')])[4]")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Oficina ContableTest");
    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("Planta BajaTest");
    driver.findElement(By.id("btn_accept")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    // Warning: waitForTextPresent may require manual changes
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Oficina ContableTest[\\s\\S]*$")) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    // Warning: assertTextNotPresent may require manual changes
    assertFalse(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Oficina GerenciaTest[\\s\\S]*$"));
    driver.findElement(By.xpath("(//a[contains(text(),'Eliminar')])[3]")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.linkText("Gestionar espacios")).click();
    Thread.sleep(6000);
    // Warning: assertTextNotPresent may require manual changes
    assertFalse(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Piso 1Test[\\s\\S]*$"));
  }
}
