package com.matrice.building.gestion.inmuebles;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	CrearInmuebleEdificioExcel.class,
	CrearPisoExcel.class,
	CrearEspacioExcel.class
	})
public class AllTestsInmueblesExcel {

}
