package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class CrearEspacio extends MatriceTest {

  @Test
  public void testCrearEspacio() throws Exception {
	System.out.println(">> testCrearEspacio");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_edit")));
	driver.findElement(By.id("btn_edit")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Gestionar espacios")));
	driver.findElement(By.linkText("Gestionar espacios")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_space")));
    driver.findElement(By.id("link_create_space")).click();
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));    
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre es requerido[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Piso es requerido[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Area es requerido[\\s\\S]*$"));
    
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Espacio A");
    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("piso 1 testcase");
    new Select(driver.findElement(By.id("spaceType"))).selectByVisibleText("Area Comun");
    new Select(driver.findElement(By.id("areas"))).selectByVisibleText("Directorio");
    new Select(driver.findElement(By.id("areas"))).selectByVisibleText("Gerente General");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Espacio creado con exito[\\s\\S]*$"));
    
    driver.findElement(By.linkText("Gestionar espacios")).click();
    driver.findElement(By.id("link_create_space")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Espacio A");
    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("piso 1 testcase");
    new Select(driver.findElement(By.id("areas"))).selectByVisibleText("Directorio");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre se encuentra ocupado[\\s\\S]*$"));
    driver.findElement(By.id("link_back")).click();
    
  }

}
