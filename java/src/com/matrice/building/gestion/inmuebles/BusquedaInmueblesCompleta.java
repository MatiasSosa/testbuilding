package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertFalse;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelInmuebles;

import dto.Inmueble;

public class BusquedaInmueblesCompleta extends MatriceTest {


  @Test
  public void testBusquedaInmueblesCompleta() throws Exception {
		System.out.println(">> testBusquedaInmueblesCompleta");
	    String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_INMUEBLES;


		Collection<Inmueble> inmuebles = ReadExcelInmuebles
				.leerArchivoExcel(archivoDestino, "Inmueble");

		if (inmuebles.size() != 0) {
			// agrego los tipos definidos en el archivo
			int cantidadCol = 11;
			for (Inmueble inmueble : inmuebles) {
				System.out.println("inmuebles.getNombre(): "+inmueble.getNombre());
				int j;
				for (j=1;j!=cantidadCol;j++){
				System.out.println("Criterios aplicados:");
				driver.get(baseUrl + "/public/building/buildings");
				wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Busqueda Avanzada")));
				driver.findElement(By.linkText("Busqueda Avanzada")).click();

				System.out.println("cantidadCol-j= "+(cantidadCol-j));
				
				if ((cantidadCol-j) > 0) {
					System.out.println("1) Nombre: "+inmueble.getNombre());
					driver.findElement(By.id("nameBuildingCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("nameBuilding"))));
					new Select(driver.findElement(By
						.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[1]/div[3]/select")))
							.selectByVisibleText("CONTIENE");
					driver.findElement(By.id("nameBuilding")).clear();
					driver.findElement(By.id("nameBuilding")).sendKeys(inmueble.getNombre());
				}
				
				if ((cantidadCol-j) > 1) {	
					System.out.println("2) Responsable: "+inmueble.getResponsable());
					if(!inmueble.getResponsable().equals("")){
						driver.findElement(By.id("employeeCheck")).click();
						wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("employee"))));
						new Select(driver.findElement(By
								.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[2]/div[3]/select")))
									.selectByVisibleText("IGUAL");
						new Select(driver.findElement(By.id("employee")))
							.selectByVisibleText(inmueble.getResponsable());
						
					}
				}
				if ((cantidadCol-j) > 2) {	
					System.out.println("3) Pais: "+inmueble.getPais());
					driver.findElement(By.id("countryCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("country"))));
					new Select(driver.findElement(By
							.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[3]/div[3]/select")))
								.selectByVisibleText("IGUAL");
					new Select(driver.findElement(By.id("country"))).selectByVisibleText(inmueble.getPais());
				}
				
				if ((cantidadCol-j) > 3) {
					System.out.println("4) Provincia: "+inmueble.getProvincia());
					driver.findElement(By.id("stateCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("state"))));
					new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[4]/div[3]/select")))
								.selectByVisibleText("IGUAL");
					new Select(driver.findElement(By.id("state"))).selectByVisibleText(inmueble.getProvincia());
				}
				if ((cantidadCol-j) > 4) {
					System.out.println("5) Ciudad: "+inmueble.getCiudad());
					driver.findElement(By.id("cityCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("city"))));
					new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[5]/div[3]/select")))
					.selectByVisibleText("IGUAL");
					new Select(driver.findElement(By.id("city"))).selectByVisibleText(inmueble.getCiudad());
				}
				if ((cantidadCol-j) > 5) {
					System.out.println("6) Zona: "+inmueble.getZona());
					driver.findElement(By.id("zoneCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("zone"))));
					new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[6]/div[3]/select")))
					.selectByVisibleText("IGUAL");
					new Select(driver.findElement(By.id("zone"))).selectByVisibleText(inmueble.getZona());
				}
				if ((cantidadCol-j) > 6) {
					System.out.println("7) Direccion: "+inmueble.getDireccion());
					driver.findElement(By.id("addressCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("address"))));
					new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[7]/div[3]/select")))
					.selectByVisibleText("IGUAL");
					driver.findElement(By.id("address")).clear();
					driver.findElement(By.id("address")).sendKeys(inmueble.getDireccion());
				}
				if ((cantidadCol-j) > 7) {
					System.out.println("8) Dimension: "+inmueble.getDimension());
					if(!inmueble.getDimension().equals("")){
						driver.findElement(By.id("measurementCheck")).click();
						wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("measurement"))));
						new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[8]/div[3]/select")))
							.selectByVisibleText("IGUAL");;
						driver.findElement(By.id("measurement")).clear();
						driver.findElement(By.id("measurement")).sendKeys(inmueble.getDimension());
					}
				}
				if ((cantidadCol-j) > 8) {
					System.out.println("9) Propietaro: "+inmueble.getDimension());
					if(!inmueble.getPropietario().equals("")){
						driver.findElement(By.id("ownerCheck")).click();	
						if(inmueble.getPropietario().equals("Si")){
							driver.findElement(By.id("ownerY")).click();		
						}else{
							driver.findElement(By.id("ownerN")).click();							
						}
					}
				}
				if ((cantidadCol-j) > 9) {
					System.out.println("10) Empleado: "+inmueble.getEmpleados());
					if(!inmueble.getEmpleados().equals("")){
						driver.findElement(By.id("employeeBuildingCheck")).click();
						wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("employeeBuilding"))));
						new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[10]/div[3]/select")))
							.selectByVisibleText("IGUAL");
						new Select(driver.findElement(By.id("employeeBuilding")))
							.selectByVisibleText(inmueble.getEmpleados());
					}
				}
				
				if ((cantidadCol-j) > 10) {
					System.out.println("11) Piso: "+inmueble.getPiso());
					if(!inmueble.getPiso().equals("")){
						driver.findElement(By.id("buildingFloorCheck")).click();
						wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("buildingFloor"))));
						new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[11]/div[3]/select")))
							.selectByVisibleText("IGUAL");
						new Select(driver.findElement(By.id("buildingFloor")))
						.selectByVisibleText(inmueble.getPiso());
					}

				}				
				if ((cantidadCol-j) > 11) {
					System.out.println("12) Espacio: "+inmueble.getEspacio());
					if(!inmueble.getEspacio().equals("")){
						driver.findElement(By.id("floorSpaceCheck")).click();
						wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("floorSpace"))));
						new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[12]/div[3]/select")))
							.selectByVisibleText("IGUAL");
						new Select(driver.findElement(By.id("floorSpace")))
						.selectByVisibleText(inmueble.getEspacio());
					}
				}				
				if ((cantidadCol-j) > 12) {
					System.out.println("13) Area: "+inmueble.getArea());
					if(!inmueble.getArea().equals("")){
						driver.findElement(By.id("areaCheck")).click();
						wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("area"))));
						new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[13]/div[3]/select")))
							.selectByVisibleText("IGUAL");
						new Select(driver.findElement(By.id("area")))
						.selectByVisibleText(inmueble.getArea());
					}
							
				}				
				
				Thread.sleep(3000);
				driver.findElement(By.id("btn_accept")).click();

				wait.until(ExpectedConditions.not(ExpectedConditions
						.visibilityOf(driver.findElement(By.id("buildings_processing")))));
				
				Thread.sleep(1000);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("buildings_info")));
				System.out.println("buildings_info:  "+driver.findElement(By.id("buildings_info")).getText()+"\n");
				Thread.sleep(1000);
				assertFalse(driver.findElement(By.id("buildings_info")).getText().matches("^*Mostrando registros del 0 al 0[\\s\\S]*$"));
				}
			}
		}
  }

  }
