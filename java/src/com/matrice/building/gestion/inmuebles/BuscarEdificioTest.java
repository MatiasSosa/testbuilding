package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class BuscarEdificioTest extends MatriceTest {
	String num_marca = System.getProperty("num_marca").toString();
	private String edificioNombre = "Edificio_"+ num_marca;
  @Test
  public void testBuscarEdificio() throws Exception {
	System.out.println(">> testBuscarEdificio");
	System.out.println("Recuperar nombre del inmueble: "+edificioNombre);  
    driver.get(baseUrl + "/public/building/buildings");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	driver.findElement(By.id("table_filter")).sendKeys(edificioNombre);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("buildings_processing")))));
    Thread.sleep(2000);
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));
    Thread.sleep(2000);
  }

}
