package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertTrue;

import java.rmi.server.UID;
import java.util.Calendar;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;

public class Inmuebles extends MatriceTest {
  Calendar calendario = Calendar.getInstance();
  private Long num = calendario.getTimeInMillis();
  String id_user = "("+new UID().toString()+")";

	  
  @Test
  public void testInmuebles() throws Exception {
    driver.get(baseUrl + "/public/building/buildings");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre es requerido\\.[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Zona es requerido\\.[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Direccion es requerido\\.[\\s\\S]*$"));
    
    driver.findElement(By.id("name")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Edificio TestCase_"+num);
    new Select(driver.findElement(By.id("country"))).selectByVisibleText("Argentina");
    new Select(driver.findElement(By.id("state"))).selectByVisibleText("Buenos Aires");
    new Select(driver.findElement(By.id("city"))).selectByVisibleText("La Plata");
    new Select(driver.findElement(By.id("zone"))).selectByVisibleText("Centro");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Calle 4");
    driver.findElement(By.id("measurement__int")).clear();
    driver.findElement(By.id("measurement__int")).sendKeys("200");
    driver.findElement(By.id("isOwner")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@id='isRented'])[2]")));
    driver.findElement(By.xpath("(//input[@id='isRented'])[2]")).click();
    
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));

    //Actualizar registro
	driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Edificio TestCase_"+num);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("buildings_processing")))));
    Thread.sleep(2000);
    driver.findElement(By.id("btn_edit")).click();
    
    wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Gestionar espacios")));
    driver.findElement(By.linkText("Gestionar espacios")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_floor")));
    driver.findElement(By.id("link_create_floor")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Piso1 TestCase_"+num);
	driver.findElement(By.id("mapPicture")).sendKeys(Constantes.PATH_IMAGES+"plano.jpg");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));

    driver.findElement(By.linkText("Gestionar espacios")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_floor")));
    driver.findElement(By.id("link_create_floor")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Piso2 TestCase_"+num);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
    
    driver.findElement(By.linkText("Gestionar espacios")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[contains(text(),'Editar')])[2]")));
    Thread.sleep(2000);
    driver.findElement(By.xpath("(//a[contains(text(),'Editar')])[2]")).click();
    driver.findElement(By.id("link_back")).click();
    
    driver.findElement(By.linkText("Gestionar espacios")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_space")));
    driver.findElement(By.id("link_create_space")).click();
    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("Piso1 TestCase_"+num);
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Espacio1_"+num);
    new Select(driver.findElement(By.id("spaceType"))).selectByVisibleText("Area Comun");
    new Select(driver.findElement(By.id("areas"))).selectByVisibleText("Directorio");
    driver.findElement(By.id("measurement__int")).clear();
    driver.findElement(By.id("measurement__int")).sendKeys("20");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
        
    driver.findElement(By.linkText("Gestionar espacios")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_space")));
    driver.findElement(By.id("link_create_space")).click();
    driver.findElement(By.id("btn_accept")).click();
    
    // validar mensajes de datos requeridos
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre es requerido\\.[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Piso es requerido\\.[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Area es requerido\\.[\\s\\S]*$"));

    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("Piso1 TestCase_"+num);
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Espacio2_"+num);
    new Select(driver.findElement(By.id("spaceType"))).selectByVisibleText("Area Comun");
    new Select(driver.findElement(By.id("areas"))).selectByVisibleText("Directorio");
    driver.findElement(By.id("measurement__int")).clear();
    driver.findElement(By.id("measurement__int")).sendKeys("22");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
    
    driver.get(baseUrl + "/public/building/buildings");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys("Edificio TestCase_"+num);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("buildings_processing")))));
    Thread.sleep(2000);
    driver.findElement(By.id("btn_edit")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Edificio EDITADO TestCase_"+num);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));

  }
}
