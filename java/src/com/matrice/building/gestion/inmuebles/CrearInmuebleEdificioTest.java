package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class CrearInmuebleEdificioTest extends MatriceTest {
  private Long num_marca = this.calendario.getTimeInMillis();
  private String edificioNombre = "Edificio_"+ num_marca;
  @Test
  public void testCrearInmuebleEdificioTest() throws Exception {

		System.out.println(">> testCrearInmuebleEdificioTest");
	    driver.get(baseUrl + "/public/building/buildings");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		driver.findElement(By.id("link_create")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
	    driver.findElement(By.id("name")).clear();
	    driver.findElement(By.id("name")).sendKeys(edificioNombre);
	    new Select(driver.findElement(By.id("country"))).selectByVisibleText("Argentina");
	    Thread.sleep(6000);
	    new Select(driver.findElement(By.id("state"))).selectByVisibleText("Buenos Aires");
	    Thread.sleep(6000);
	    new Select(driver.findElement(By.id("city"))).selectByVisibleText("La Plata");
	    new Select(driver.findElement(By.id("zone"))).selectByVisibleText("Centro");
	    driver.findElement(By.id("address")).clear();
	    driver.findElement(By.id("address")).sendKeys("Av 7 n� 345");
	    driver.findElement(By.id("btn_accept")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
	    
	    System.out.println("Guardar nombre del inmueble: "+edificioNombre);
		System.setProperty("num_marca", num_marca.toString());

  }

}
