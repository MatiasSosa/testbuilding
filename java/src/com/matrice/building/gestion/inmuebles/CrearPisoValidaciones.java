package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class CrearPisoValidaciones extends MatriceTest {

  @Test
  public void testCrearPisoValidaciones() throws Exception {
    System.out.println(">> testCrearPisoValidaciones");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_edit")));
	driver.findElement(By.id("btn_edit")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Gestionar espacios")));
    driver.findElement(By.linkText("Gestionar espacios")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_floor")));
    driver.findElement(By.id("link_create_floor")).click();
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre es requerido[\\s\\S]*$"));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("piso 1 testcase");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Piso actualizado con exito[\\s\\S]*$"));
    wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Gestionar espacios")));
    driver.findElement(By.linkText("Gestionar espacios")).click();
    driver.findElement(By.id("link_create_floor")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("piso 1 testcase");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Nombre se encuentra ocupado[\\s\\S]*$"));
    driver.findElement(By.id("link_back")).click();
  }

}
