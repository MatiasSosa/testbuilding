package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class BorrarEspacio extends MatriceTest {

  @Test
  public void testBorrarEspacio() throws Exception {
	
		driver.findElement(By.linkText("Gestionar espacios")).click();
		
		wait.until(ExpectedConditions.elementToBeClickable
				(By.xpath("(//a[contains(text(),'Eliminar')])[2]")));
		
		driver.findElement(By.xpath("(//a[contains(text(),'Eliminar')])[2]")).click();
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		wait.until(ExpectedConditions.elementToBeClickable
				(By.id("succes_notify_div")));
		
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*El espacio fue eliminado con exito[\\s\\S]*$"));
  }

}
