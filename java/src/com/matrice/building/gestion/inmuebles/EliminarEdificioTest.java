package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class EliminarEdificioTest extends MatriceTest {
  String num_marca = System.getProperty("num_marca").toString();
  private String edificioNombre = "Edificio_"+ num_marca;
  
  @Test
  public void testEliminarEdificioA() throws Exception {
	driver.get(baseUrl + "/public/building/buildings");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(edificioNombre);
    Thread.sleep(1000);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("buildings_processing")))));
    Thread.sleep(2000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(1000);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*La propiedad fue eliminada con exito[\\s\\S]*$"));
    
	System.out.println(">> EliminarEdificioTest: "+edificioNombre);

  }

}
