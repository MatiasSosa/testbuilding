package com.matrice.building.gestion.inmuebles;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
	CrearInmuebleEdificioTest.class,
	CrearPiso1.class,
	BuscarEdificioTest.class,
	ValidarNoEliminarInmuebleConPisos.class,
	BorrarPiso1.class,
	EliminarEdificioTest.class
	})

public class SuiteValidarNoEliminarInmuebleConPisos {

	
	}

