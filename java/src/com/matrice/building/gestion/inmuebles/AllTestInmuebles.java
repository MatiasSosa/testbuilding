package com.matrice.building.gestion.inmuebles;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
 	Inmuebles.class,
	SuiteGestionarEspacios.class,
	SuiteValidarNoEliminarInmuebleConEmpleados.class,
	SuiteValidarNoEliminarInmuebleConPisos.class,
	SuiteValidarNoEliminarInmuebleConMantenimientos.class,
	//Busqueda
	BusquedaInmueblesCompleta.class
	})

public class AllTestInmuebles {

	
	}



