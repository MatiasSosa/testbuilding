package com.matrice.building.gestion.inmuebles;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class CrearPiso1 extends MatriceTest {
	String num_marca = System.getProperty("num_marca").toString();
	private String edificioNombre = "Edificio_"+ num_marca;
  @Test
  public void testCrearPiso1() throws Exception {
	System.out.println(">> testCrearPiso1");
	  
	System.out.println("Recuperar nombre del inmueble: "+edificioNombre);  
  	driver.get(baseUrl + "/public/building/buildings");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	driver.findElement(By.id("table_filter")).sendKeys(edificioNombre);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("buildings_processing")))));
    Thread.sleep(2000);
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));
    Thread.sleep(2000);
    wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_edit")));
	driver.findElement(By.id("btn_edit")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Gestionar espacios")));
    driver.findElement(By.linkText("Gestionar espacios")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_floor")));
    driver.findElement(By.id("link_create_floor")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("piso_"+num_marca);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Piso actualizado con exito[\\s\\S]*$"));
    
  }

}
