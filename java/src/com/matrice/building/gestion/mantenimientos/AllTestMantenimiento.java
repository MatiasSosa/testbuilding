package com.matrice.building.gestion.mantenimientos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ 
	ValidarEliminarMantenimientoConRevisiones.class,
	AMManteniento.class })

public class AllTestMantenimiento {

}
