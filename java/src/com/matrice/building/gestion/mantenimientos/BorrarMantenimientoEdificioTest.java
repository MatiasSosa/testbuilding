package com.matrice.building.gestion.mantenimientos;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.matrice.general.MatriceTest;

public class BorrarMantenimientoEdificioTest extends MatriceTest {
	String num_marca = System.getProperty("num_marca").toString();

  @Test
  public void testBorrarMantenimientoEdificioA() throws Exception {
	WebDriverWait wait = new WebDriverWait(driver, 60);
	driver.get(baseUrl + "/public/maintenance/maintenances");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys("Mantenimiento_"+num_marca);
    Thread.sleep(2000);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("maintenances_processing")))));
    Thread.sleep(2000);
    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*El mantenimiento se ha eliminado con exito[\\s\\S]*$"));
    							
  }

}
