package com.matrice.building.gestion.mantenimientos;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class CrearMantenimientoEdificioTest extends MatriceTest {
  String num_marca = System.getProperty("num_marca").toString();
  private String edificioNombre = "Edificio_"+ num_marca;

  @Test
  public void testCrearMantenimientoEdificioA() throws Exception {
    driver.get(baseUrl + "/public/maintenance/maintenances");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Mantenimiento_"+num_marca);
    new Select(driver.findElement(By.id("asset_type_id"))).selectByVisibleText("Inmuebles");
    Thread.sleep(6000);
    new Select(driver.findElement(By.id("assetBuilding"))).selectByVisibleText(edificioNombre);
    new Select(driver.findElement(By.id("maintenance_type_id"))).selectByVisibleText("Revision");
    new Select(driver.findElement(By.id("employee_id"))).selectByVisibleText("Caniglia, Javier");
    driver.findElement(By.name("periodicityTimes")).clear();
    driver.findElement(By.name("periodicityTimes")).sendKeys("1");
    new Select(driver.findElement(By.id("periodicityType"))).selectByVisibleText("Meses");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Mantenimiento creado con exito[\\s\\S]*$"));

  }

}
