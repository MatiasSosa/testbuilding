package com.matrice.building.gestion.mantenimientos;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class AMManteniento extends MatriceTest {
	  Long num_marca = calendario.getTimeInMillis();
	  private String mantenimientoNombre = "Mantenimiento_"+ num_marca;
	  private String mantenimientoNombreEditado = "Mantenimiento_Editado_"+ num_marca;
  @Test
  public void testAltaModificacionManteniento() throws Exception {
    driver.get(baseUrl + "/public/maintenance/maintenances");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create"))); 
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(mantenimientoNombre);
    new Select(driver.findElement(By.id("asset_type_id"))).selectByVisibleText("Inmuebles");
    new Select(driver.findElement(By.id("assetBuilding"))).selectByVisibleText("Central");
    new Select(driver.findElement(By.id("maintenance_type_id"))).selectByVisibleText("Revision");
    new Select(driver.findElement(By.id("employee_id"))).selectByVisibleText("Caniglia, Javier");
    driver.findElement(By.id("periodicityTimes")).clear();
    driver.findElement(By.id("periodicityTimes")).sendKeys("2");
    new Select(driver.findElement(By.id("periodicityType"))).selectByVisibleText("Meses");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Mantenimiento creado con exito[\\s\\S]*$"));
    
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(mantenimientoNombre);
    Thread.sleep(1000);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("maintenances_processing")))));
    Thread.sleep(1000);
    driver.findElement(By.id("btn_edit")).click();

    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(mantenimientoNombreEditado);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*con exito[\\s\\S]*$"));

  }


}
