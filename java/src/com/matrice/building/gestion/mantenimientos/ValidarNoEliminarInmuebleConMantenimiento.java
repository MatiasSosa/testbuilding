package com.matrice.building.gestion.mantenimientos;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class ValidarNoEliminarInmuebleConMantenimiento extends MatriceTest {

  @Test
  public void testValidarNoEliminarInmuebleMantenimiento() throws Exception {
	wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_delete")));
    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("session_error_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*El Inmueble tiene Mantenimientos asociados, eliminelos primero[\\s\\S]*$"));
  }

}
