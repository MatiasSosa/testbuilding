package com.matrice.building.gestion.bienes;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class EditarBienesUsoComputadoraTest extends MatriceTest {
	
  @Test
  public void testEditarBienesUsoComputadora() throws Exception {
    boolean ninguno = false;
    driver.get(baseUrl + "/public/management/assets");
    Thread.sleep(5000);
    driver.findElement(By.id("table_filter")).sendKeys("TESTCASE");
    Thread.sleep(5000);
    
    ninguno = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
	
	if (!ninguno) {
		// hay datos cargados, modifico el primero  
			driver.findElement(By.id("btn_edit")).click();
		    driver.findElement(By.id("model")).clear();
		    driver.findElement(By.id("model")).sendKeys("Inspiron Editado");
		    driver.findElement(By.id("btn_accept")).click();
		    Thread.sleep(5000);
		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Bien de Uso actualizado con exito\\.[\\s\\S]*$"));		
	} else {
		// no hay datos cargados
		System.out.println("No hay datos");
	}

  }

}
