package com.matrice.building.gestion.bienes;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class CrearBienesUsoComputadoraTest extends MatriceTest {

  @Test
  public void testCrearBienesUsoComputadora() throws Exception {
    driver.get(baseUrl + "/public/management/assets");
    Thread.sleep(5000);
    String datoEtiqueta = proximoBienDeUso();
    driver.findElement(By.id("link_create")).click();
    driver.findElement(By.id("btn_accept")).click();
    Thread.sleep(5000);
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Etiqueta es requerido[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Marca es requerido[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Estado es requerido[\\s\\S]*$"));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El campo Tipo es requerido\\.[\\s\\S]*$"));
    new Select(driver.findElement(By.id("assetType"))).selectByVisibleText("ComputadorasTest");
    driver.findElement(By.id("tag")).clear();
    driver.findElement(By.id("tag")).sendKeys(datoEtiqueta);
    driver.findElement(By.id("band")).clear();
    driver.findElement(By.id("band")).sendKeys("DELL");
    driver.findElement(By.id("model")).clear();
    driver.findElement(By.id("model")).sendKeys("Inspiron");
    driver.findElement(By.id("serialNumber")).clear();
    driver.findElement(By.id("serialNumber")).sendKeys(datoEtiqueta);
    driver.findElement(By.id("status")).clear();
    driver.findElement(By.id("status")).sendKeys("NUEVA");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Bien de Uso creado con exito[\\s\\S]*$"));
    driver.findElement(By.id("link_create")).click();
    Thread.sleep(2000);
    new Select(driver.findElement(By.id("assetType"))).selectByVisibleText("ComputadorasTest");
    driver.findElement(By.id("tag")).clear();
    driver.findElement(By.id("tag")).sendKeys(datoEtiqueta);
    driver.findElement(By.id("band")).clear();
    driver.findElement(By.id("band")).sendKeys("DELL");
    driver.findElement(By.id("model")).clear();
    driver.findElement(By.id("model")).sendKeys("Inspiron");
    driver.findElement(By.id("serialNumber")).clear();
    driver.findElement(By.id("serialNumber")).sendKeys(datoEtiqueta);
    driver.findElement(By.id("status")).clear();
    driver.findElement(By.id("status")).sendKeys("NUEVA");
    driver.findElement(By.id("btn_accept")).click();
    Thread.sleep(5000);
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Ya existe[\\s\\S]*$"));
    Thread.sleep(1000);
    
    
    
  }
  
  public String proximoBienDeUso() throws Exception {
	    
	  	String etiqueta = "";
	    String posEtiqueta = "";
	    boolean noExiste = false;
	    int numero = 1;
	    
	    
	    while (etiqueta.equals("")) {
	    	
	    	posEtiqueta = numero + "TESTCASE";
	    	driver.findElement(By.id("table_filter")).clear();
	    	driver.findElement(By.id("table_filter")).sendKeys(posEtiqueta);
	    	System.out.println("Probar etiqueta: "+posEtiqueta);
	    	Thread.sleep(5000);
	    	
	    	noExiste = driver.findElement(By.cssSelector("BODY")).getText()
					.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
	    	
			if (noExiste) {
				etiqueta=posEtiqueta;
			} else {
				numero++;
			}

		}
	    System.out.println("Etiqueta a utilizar:  "+etiqueta);
	    return etiqueta;
	    
	  }
}
