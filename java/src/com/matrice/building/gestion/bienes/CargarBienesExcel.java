package com.matrice.building.gestion.bienes;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelBienesDeUso;

import dto.BienDeUso;

public class CargarBienesExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testCargarBienesExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;

   List<BienDeUso> bienes = ReadExcelBienesDeUso.leerArchivoExcel(archivoDestino,"bienes");

   if(bienes.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(BienDeUso bien: bienes){
	   		    driver.get(baseUrl + "/public/management/assets");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(bien.getEtiqueta());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("assets_processing")))));
	   		 
	   			noExiste = driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("tag")));
		   		    //datos requeridos
		   		    new Select(driver.findElement(By.id("assetType"))).selectByVisibleText(bien.getTipo());
		   		    driver.findElement(By.id("tag")).clear();
		   		    driver.findElement(By.id("tag")).sendKeys(bien.getEtiqueta());
		   		    driver.findElement(By.id("band")).clear();
		   		    driver.findElement(By.id("band")).sendKeys(bien.getMarca());
		   		    driver.findElement(By.id("status")).clear();
		   		    driver.findElement(By.id("status")).sendKeys(bien.getEstado());
		   		    //Datos no obligatorios
		   		    if(!bien.getModelo().equals("-1")){
			   		    driver.findElement(By.id("model")).clear();
			   		    driver.findElement(By.id("model")).sendKeys(bien.getModelo());
		   		    }
		   		    if(!bien.getNumSerie().equals("-1")){
			   		    driver.findElement(By.id("serialNumber")).clear();
			   		    driver.findElement(By.id("serialNumber")).sendKeys(bien.getNumSerie());
		   		    }
		   		    if(!bien.getDescripcion().equals("-1")){
			   		    driver.findElement(By.id("tecDescription")).clear();
			   		    driver.findElement(By.id("tecDescription")).sendKeys(bien.getDescripcion());
		   		    }
		   		    if(!bien.getFechaFabricacion().equals("-1")){
			   			((JavascriptExecutor) driver).executeScript ("document.getElementById ('buildDate').removeAttribute('readonly',0);");
			   		    driver.findElement(By.id("buildDate")).sendKeys(bien.getFechaFabricacion()); 
		   		    }
		   		    if(!bien.getFechaCompra().equals("-1")){
			   			((JavascriptExecutor) driver).executeScript ("document.getElementById ('boughtDate').removeAttribute('readonly',0);");
			   		    driver.findElement(By.id("boughtDate")).sendKeys(bien.getFechaCompra()); //2014-05-01
		   		    }
		   		    if(!bien.getVidaUtil().equals("-1")){
			   		    driver.findElement(By.id("lifespan")).clear();
			   		    driver.findElement(By.id("lifespan")).sendKeys(bien.getVidaUtil());
		   		    }
		   		    if(!bien.getFoto().equals("-1")){
			   			driver.findElement(By.id("picture"))
			   			.sendKeys(Constantes.PATH_IMAGES+bien.getFoto());
		   		    }

		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
		   		    		.matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego Bien: "+bien.getTipo()+" con etiqueta "+bien.getEtiqueta());
		   		    
	   			}else{
	   				System.out.println("El bien "+bien.getTipo()+" con etiqueta "+bien.getEtiqueta()+" ya existe");
	   			}
	   		}
   }   		
  }
}
