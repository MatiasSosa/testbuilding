package com.matrice.building.gestion;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.building.gestion.areas.AllAreas;
import com.matrice.building.gestion.bienes.AllTestBienes;
import com.matrice.building.gestion.empleados.AllEmpleados;
import com.matrice.building.gestion.inmuebles.AllTestInmuebles;
import com.matrice.building.gestion.inmuebles.AllTestsInmueblesExcel;

@RunWith(Suite.class)
@SuiteClasses({ 
	AllAreas.class, 
	AllTestsInmueblesExcel.class,//Carga masiva de inmuebles
									// Requerido cargar antes de empleados
	AllEmpleados.class,
	AllTestBienes.class,
	AllTestInmuebles.class
	
})
public class AllTestsGestion{

}
