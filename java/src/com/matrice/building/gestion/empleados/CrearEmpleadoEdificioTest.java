package com.matrice.building.gestion.empleados;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class CrearEmpleadoEdificioTest extends MatriceTest {
  String num_marca = System.getProperty("num_marca").toString();
  private String nombreEmpleado = "Roberto_"+ num_marca;
  private String edificioNombre = "Edificio_"+ num_marca;
  @Test
  public void testCrearEmpleadoEdificioA() throws Exception {
    driver.get(baseUrl + "/public/employee/employees");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys(nombreEmpleado);
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Casero");
    driver.findElement(By.id("idNumber")).clear();
    driver.findElement(By.id("idNumber")).sendKeys(num_marca);
    driver.findElement(By.id("email")).clear();
    driver.findElement(By.id("email")).sendKeys(nombreEmpleado+"@gmail.com");
    new Select(driver.findElement(By.id("building"))).selectByVisibleText(edificioNombre);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Empleado creado con exito[\\s\\S]*$"));
  }

}
