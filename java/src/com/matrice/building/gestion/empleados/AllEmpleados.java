package com.matrice.building.gestion.empleados;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	CargarEmpleadosExcel.class,
	ValidarABMEmpleado.class
  })

public class AllEmpleados {
	
}
