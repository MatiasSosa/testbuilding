package com.matrice.building.gestion.empleados;

import static org.junit.Assert.assertTrue;

import java.rmi.server.UID;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class ValidarABMEmpleado extends MatriceTest {
  
  private Long num = this.calendario.getTimeInMillis();
  private String mailEmpleado = "mail_"+num+"@mail.com";
  String user_id = "("+new UID().toString()+")";
  
  @Test
  public void testValidarABMEmpleado() throws Exception {
   
	    driver.get(baseUrl + "/public/employee/employees");
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
	    driver.findElement(By.id("link_create")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("firstName")));
	    driver.findElement(By.id("btn_accept")).click();
	    // validar mensajes de campos requeridos
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
	    		.matches("^[\\s\\S]*El campo Nombre es requerido[\\s\\S]*$"));
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
	    		.matches("^[\\s\\S]*El campo Apellido es requerido[\\s\\S]*$"));
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
	    		.matches("^[\\s\\S]*El campo Documento es requerido[\\s\\S]*$"));
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
	    		.matches("^[\\s\\S]*El campo Email es requerido[\\s\\S]*$"));
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
	    		.matches("^[\\s\\S]*El campo Edificio es requerido[\\s\\S]*$"));

	    //Carga de Datos
	    driver.findElement(By.id("firstName")).clear();
	    driver.findElement(By.id("firstName")).sendKeys("Roberto"+user_id);
	    driver.findElement(By.id("lastName")).clear();
	    driver.findElement(By.id("lastName")).sendKeys("Perez");
	    driver.findElement(By.id("idNumber")).clear();
	    driver.findElement(By.id("idNumber")).sendKeys(num.toString());
	    driver.findElement(By.id("email")).clear();
	    driver.findElement(By.id("email")).sendKeys(mailEmpleado);
	    new Select(driver.findElement(By.id("building"))).selectByVisibleText("Central");
	    driver.findElement(By.id("btn_accept")).click();
	    
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
	    
	    //actualizar registro
		wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	    driver.findElement(By.id("table_filter")).sendKeys(mailEmpleado);
	    
	    wait.until(ExpectedConditions.not(ExpectedConditions
	    		.visibilityOf(driver.findElement(By.id("employees_processing")))));
	    
	    Thread.sleep(2000);
	    driver.findElement(By.id("btn_edit")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("firstName")));
	    ((JavascriptExecutor) driver).executeScript ("document.getElementById ('birth__date').removeAttribute('readonly',0);");
	    driver.findElement(By.id("birth__date")).sendKeys("2014-05-01");
	    driver.findElement(By.id("btn_accept")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Empleado actualizado con exito[\\s\\S]*$"));
		
		// Eliminar Empleado
		wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	    driver.findElement(By.id("table_filter")).sendKeys("Roberto"+user_id);
	    
	    wait.until(ExpectedConditions.not(ExpectedConditions
	    		.visibilityOf(driver.findElement(By.id("employees_processing")))));
	    Thread.sleep(2000);
	    driver.findElement(By.id("btn_delete")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El empleado fue eliminado con exito.[\\s\\S]*$"));

		

	}
}
