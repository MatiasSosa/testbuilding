package com.matrice.building.gestion.empleados;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.matrice.general.MatriceTest;

public class CrearEmpleadoTestGral extends MatriceTest {
 private boolean noExiste;
 
  @Test
  public void testCrearEmpleadoTestGral() throws Exception {
	WebDriverWait wait = new WebDriverWait(driver, 60);
    driver.get(baseUrl + "/public/employee/employees");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys("javier@prueba.com");
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("employees_processing")))));
    Thread.sleep(2000);
	noExiste = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
	
	if (noExiste) {
	    driver.findElement(By.id("link_create")).click();
	    driver.findElement(By.id("firstName")).clear();
	    driver.findElement(By.id("firstName")).sendKeys("Javier");
	    driver.findElement(By.id("lastName")).clear();
	    driver.findElement(By.id("lastName")).sendKeys("Caniglia");
	    driver.findElement(By.id("idNumber")).clear();
	    driver.findElement(By.id("idNumber")).sendKeys("999999999");
	    driver.findElement(By.id("email")).clear();
	    driver.findElement(By.id("email")).sendKeys("javier@prueba.com");
	    new Select(driver.findElement(By.id("building"))).selectByVisibleText("Central");
	    driver.findElement(By.id("btn_accept")).click();
	    // Warning: assertTextPresent may require manual changes
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Empleado creado con exito\\.[\\s\\S]*$"));
		
	}else{
		System.out.println("El empleado ya existe");
	}
  }

}
