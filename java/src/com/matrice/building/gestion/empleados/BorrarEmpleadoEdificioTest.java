package com.matrice.building.gestion.empleados;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.MatriceTest;

public class BorrarEmpleadoEdificioTest extends MatriceTest {
  String num_marca = System.getProperty("num_marca").toString();
  private String nombreEmpleado = "Roberto_"+ num_marca;
  @Test
  public void testBorrarEmpleadoEdificioA() throws Exception {
    driver.get(baseUrl + "/public/employee/employees");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(nombreEmpleado);
    Thread.sleep(2000);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("employees_processing")))));
    Thread.sleep(2000);
    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*El empleado fue eliminado con exito[\\s\\S]*$"));
  }

}
