	package com.matrice.building.gestion;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.building.gestion.areas.ValidarAgregarAreasExcel;
import com.matrice.building.gestion.empleados.CargarEmpleadosExcel;

@RunWith(Suite.class)
@SuiteClasses({
	ValidarAgregarAreasExcel.class, 
	CargarEmpleadosExcel.class})
public class AllTestsExcel {

}
