package com.matrice.building.gestion.pedidos;

import static org.junit.Assert.assertFalse;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelPedidosStock;

import dto.PedidoStock;

public class BusquedaStockCompleta extends MatriceTest {


  @Test
  public void testBusquedaStockCompleta() throws Exception {
	  
	  String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_PEDIDOS;

		Collection<PedidoStock> pedidos = ReadExcelPedidosStock
				.leerArchivoExcel(archivoDestino, "NuevoPedidoStock");

		if (pedidos.size() != 0) {
			// agrego los tipos definidos en el archivo
			int cantidadCol = 9;
			for (PedidoStock pedido : pedidos) {
				System.out.println("pedido.getTitulo():"+pedido.getTitulo());
				int j;
				for (j=1;j!=cantidadCol;j++){
				System.out.println("Criterios aplicados:");
				driver.get(baseUrl + "/public/maintenance/orders");
				wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Busqueda Avanzada")));
				driver.findElement(By.linkText("Busqueda Avanzada")).click();

				System.out.println("cantidadCol-j= "+(cantidadCol-j));
				
				if ((cantidadCol-j) > 0) {
				System.out.println("1) Titulo: "+pedido.getTitulo());
				driver.findElement(By.id("titleCheck")).click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("title"))));
				driver.findElement(By.id("title")).clear();
				driver.findElement(By.id("title")).sendKeys(pedido.getTitulo());
				}
				
				if ((cantidadCol-j) > 1) {	
				System.out.println("2) Solicitante: "+pedido.getSolicitante());
				driver.findElement(By.id("applicant_idCheck")).click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("applicant_id"))));
				new Select(driver.findElement(By.id("applicant_id"))).selectByVisibleText(pedido.getSolicitante());
				}
				if ((cantidadCol-j) > 2) {	
					System.out.println("3) Tipo: "+pedido.getTipo());
					driver.findElement(By.id("sector_idCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("sector_id"))));
					new Select(driver.findElement(By.id("sector_id"))).selectByVisibleText(pedido.getTipo());
					Thread.sleep(1000);
					System.out.println("Bien: "+pedido.getBien());
					driver.findElement(By.id("commodity_type_idCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("commodity_type_id"))));
					new Select(driver.findElement(By.id("commodity_type_id"))).selectByVisibleText(pedido.getBien());
				}
				
				if ((cantidadCol-j) > 3) {
				System.out.println("4) Cantidad: "+pedido.getCantidad());
				driver.findElement(By.id("quantityCheck")).click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("quantity"))));
				new Select(driver.findElement(By.xpath("/html/body/div[1]/form/div/div/div[1]/div/div[10]/div[3]/select")))
							.selectByVisibleText("IGUAL");
				driver.findElement(By.id("quantity")).clear();
				driver.findElement(By.id("quantity")).sendKeys(pedido.getCantidad());
				}
				if ((cantidadCol-j) > 4) {
				System.out.println("5) Criticidad: "+pedido.getSeveridad());
				driver.findElement(By.id("order_severity_idCheck")).click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("order_severity_id"))));
				new Select(driver.findElement(By.id("order_severity_id"))).selectByVisibleText(pedido.getSeveridad());
				}
				if ((cantidadCol-j) > 5) {
				System.out.println("6) Descripcion: "+pedido.getDescripcion());
				driver.findElement(By.id("descriptionCheck")).click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("quantity"))));
				driver.findElement(By.id("description")).clear();
				driver.findElement(By.id("description")).sendKeys(pedido.getDescripcion());
				}
				if ((cantidadCol-j) > 6) {
				System.out.println("7) Estado: Abierto");
				driver.findElement(By.id("order_status_idCheck")).click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("order_status_id"))));
				new Select(driver.findElement(By.id("order_status_id"))).selectByVisibleText("ABIERTO");
				
				}
				if ((cantidadCol-j) > 7) {
					if(!pedido.getComentario().equals("")){
					System.out.println("8) Comentario: "+pedido.getComentario());
					driver.findElement(By.id("commentsCheck")).click();
					wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("comments"))));
					driver.findElement(By.id("comments")).clear();
					driver.findElement(By.id("comments")).sendKeys(pedido.getComentario());
					}
				}
				Thread.sleep(3000);
				driver.findElement(By.id("btn_accept")).click();

				wait.until(ExpectedConditions.not(ExpectedConditions
						.visibilityOf(driver.findElement(By.id("orders_processing")))));
				Thread.sleep(1000);
				wait.until(ExpectedConditions.elementToBeClickable(By.id("orders_info")));
				System.out.println("orders_info:  "+driver.findElement(By.id("orders_info")).getText()+"\n");
				Thread.sleep(1000);
				assertFalse(driver.findElement(By.id("orders_info")).getText().matches("^*Mostrando registros del 0 al 0[\\s\\S]*$"));
				}
			}
		}
  }

  }
