package com.matrice.building.gestion.pedidos;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelPedidosMantenimiento;

import dto.PedidoMantenimiento;

public class NuevoPedidoMantenimientoExcel extends MatriceTest {
	@Test
	public void testNuevoPedidoMantenimiento() throws Exception {
	    String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_PEDIDOS;
		boolean noExiste;

		Collection<PedidoMantenimiento> pedidos = ReadExcelPedidosMantenimiento
				.leerArchivoExcel(archivoDestino, "NuevoPedidoMantenimiento");

		if (pedidos.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (PedidoMantenimiento pedido : pedidos) {
				driver.get(baseUrl + "/public/maintenance/orders");
				wait.until(ExpectedConditions.elementToBeClickable(By
						.id("table_filter")));
				driver.findElement(By.id("table_filter")).sendKeys(
						pedido.getTitulo());

				wait.until(ExpectedConditions.not(ExpectedConditions
						.visibilityOf(driver.findElement(By
								.id("orders_processing")))));

				noExiste = driver
						.findElement(By.cssSelector("BODY"))
						.getText()
						.matches(
								"^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");

				if (noExiste) {
					// si no existe la agrego
					wait.until(ExpectedConditions.elementToBeClickable(By
							.linkText("Nuevo Pedido Mantenimiento")));
					driver.findElement(
							By.linkText("Nuevo Pedido Mantenimiento")).click();
					wait.until(ExpectedConditions.elementToBeClickable(By
							.id("title")));

					driver.findElement(By.id("title")).clear();
					driver.findElement(By.id("title")).sendKeys(
							pedido.getTitulo());
					new Select(driver.findElement(By.id("applicant_id")))
							.selectByVisibleText(pedido.getSolicitante());

					new Select(driver.findElement(By.id("asset_type_id")))
							.selectByVisibleText(pedido.getTipo());

					new Select(driver.findElement(By.id("assetBuilding")))
							.selectByVisibleText(pedido.getBien());
					wait.until(ExpectedConditions.elementToBeClickable(By
							.id("maintenance_type_id")));

					new Select(driver.findElement(By.id("maintenance_type_id")))
							.selectByVisibleText(pedido.getTipoMantenimiento());
					new Select(driver.findElement(By.id("order_severity_id")))
							.selectByVisibleText(pedido.getSeveridad());
					driver.findElement(By.id("description")).clear();
					driver.findElement(By.id("description")).sendKeys(
							pedido.getDescripcion());
					driver.findElement(By.id("comments")).clear();
					driver.findElement(By.id("comments")).sendKeys(
							pedido.getComentario());

					driver.findElement(By.id("btn_accept")).click();
					// Warning: assertTextPresent may require manual changes
					wait.until(ExpectedConditions.elementToBeClickable(By
							.id("succes_notify_div")));
					assertTrue(driver.findElement(By.cssSelector("BODY"))
							.getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
				}
			}
		}

	}

}
