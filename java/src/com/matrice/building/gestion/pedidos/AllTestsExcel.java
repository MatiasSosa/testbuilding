package com.matrice.building.gestion.pedidos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NuevoPedidoMantenimientoExcel.class,
		NuevoPedidoStockExcel.class })
public class AllTestsExcel {

}
