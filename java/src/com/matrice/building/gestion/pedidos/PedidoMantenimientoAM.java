package com.matrice.building.gestion.pedidos;

import static org.junit.Assert.assertTrue;

import java.util.GregorianCalendar;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

import dto.PedidoMantenimiento;

public class PedidoMantenimientoAM extends MatriceTest {
	@Test
	public void testPedidoMantenimientoAM() throws Exception {
		String num_marca = new Long(new GregorianCalendar().getTimeInMillis()).toString();
		PedidoMantenimiento pedido = new PedidoMantenimiento("titulo"
				+ num_marca, "Perez, Mauro", "Inmuebles", "Central",
				"Cambio de Aceite", "MEDIA", "Descripcion Pedido" + num_marca,
				"");
		//Alta
		driver.get(baseUrl + "/public/maintenance/orders");

		wait.until(ExpectedConditions.elementToBeClickable(By
				.linkText("Nuevo Pedido Mantenimiento")));
		driver.findElement(By.linkText("Nuevo Pedido Mantenimiento")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("title")));

		driver.findElement(By.id("title")).clear();
		driver.findElement(By.id("title")).sendKeys(pedido.getTitulo());
		new Select(driver.findElement(By.id("applicant_id")))
				.selectByVisibleText(pedido.getSolicitante());

		new Select(driver.findElement(By.id("asset_type_id")))
				.selectByVisibleText(pedido.getTipo());

		new Select(driver.findElement(By.id("assetBuilding")))
				.selectByVisibleText(pedido.getBien());
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("maintenance_type_id")));

		new Select(driver.findElement(By.id("maintenance_type_id")))
				.selectByVisibleText(pedido.getTipoMantenimiento());
		new Select(driver.findElement(By.id("order_severity_id")))
				.selectByVisibleText(pedido.getSeveridad());
		driver.findElement(By.id("description")).clear();
		driver.findElement(By.id("description")).sendKeys(
				pedido.getDescripcion());
		driver.findElement(By.id("comments")).clear();
		driver.findElement(By.id("comments")).sendKeys(pedido.getComentario());

		driver.findElement(By.id("btn_accept")).click();
		// Warning: assertTextPresent may require manual changes
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("succes_notify_div")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		
		//Modificacion
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("table_filter")));
		driver.findElement(By.id("table_filter")).sendKeys(
				pedido.getTitulo());

		wait.until(ExpectedConditions.not(ExpectedConditions
				.visibilityOf(driver.findElement(By
						.id("orders_processing")))));
		driver.findElement(By.id("btn_edit")).click();
		pedido.setTitulo(pedido.getTitulo()+"Modificado");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("title")));

		driver.findElement(By.id("title")).clear();
		driver.findElement(By.id("title")).sendKeys(pedido.getTitulo());
		new Select(driver.findElement(By.id("applicant_id")))
				.selectByVisibleText(pedido.getSolicitante());

		new Select(driver.findElement(By.id("asset_type_id")))
				.selectByVisibleText(pedido.getTipo());
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("assetBuilding")));

		new Select(driver.findElement(By.id("assetBuilding")))
				.selectByVisibleText(pedido.getBien());
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("maintenance_type_id")));

		new Select(driver.findElement(By.id("maintenance_type_id")))
				.selectByVisibleText(pedido.getTipoMantenimiento());
		new Select(driver.findElement(By.id("order_severity_id")))
				.selectByVisibleText(pedido.getSeveridad());
		driver.findElement(By.id("description")).clear();
		driver.findElement(By.id("description")).sendKeys(
				pedido.getDescripcion());
		driver.findElement(By.id("comments")).clear();
		driver.findElement(By.id("comments")).sendKeys(pedido.getComentario());

		driver.findElement(By.id("btn_accept")).click();
		// Warning: assertTextPresent may require manual changes
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("succes_notify_div")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("table_filter")));
		driver.findElement(By.id("table_filter")).sendKeys(
				pedido.getTitulo());

		wait.until(ExpectedConditions.not(ExpectedConditions
				.visibilityOf(driver.findElement(By
						.id("orders_processing")))));
		
		 assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
					.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));


	}

}
