package com.matrice.building.gastos.periodicos;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelGastos;

import dto.Gasto;

public class CargarGastosUnicosExcel extends MatriceTest {

	

	@Test
	public void testCargarGastosUnicosExcel() throws Exception {
		
		String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_GASTOS;
		boolean noExiste;
		int cont = 0;
		Collection<Gasto> gastos = ReadExcelGastos
				.leerArchivoExcelGastosUnicos(archivoDestino, "gastosUnicos");

		if (gastos.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (Gasto gasto : gastos) {
				driver.get(baseUrl + "/public/expense/expenses/0");
				wait.until(ExpectedConditions.elementToBeClickable(By
						.id("table_filter")));
				driver.findElement(By.id("table_filter")).sendKeys(
						gasto.getConcepto());

				wait.until(ExpectedConditions.not(ExpectedConditions
						.visibilityOf(driver.findElement(By
								.id("expenses_processing")))));

				noExiste = driver
						.findElement(By.cssSelector("BODY"))
						.getText().matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");

				if (noExiste) {
					// si no existe la agrego
					wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
				    driver.findElement(By.id("link_create")).click();

					wait.until(ExpectedConditions.elementToBeClickable(By.id("concept")));
					
				    driver.findElement(By.id("concept")).clear();
				    driver.findElement(By.id("concept")).sendKeys(gasto.getConcepto());

				    //SERVICIO MANTENIMIENTO OTRO
				    new Select(driver.findElement(By.id("expenseTypeClassification")))
			    	.selectByVisibleText(gasto.getTipoGasto());
				    
				    if(gasto.getTipoGasto().equals("MANTENIMIENTO")){
					    if(!gasto.getMantenimiento().equals("")){
						    String[] mantenimientos = gasto.getMantenimiento().split(",");
						    for (String mantenimiento: mantenimientos){
						    	new Select(driver.findElement(By.id("maintenances")))
						    		.selectByVisibleText(mantenimiento);
						    }				    	
					    }
				    }
				    
				    // TIPOS DE GASTOS DADOS DE ALTA POR EL USUARIO
				    new Select(driver.findElement(By.id("expenseType")))
				    	.selectByVisibleText(gasto.getTipoGastoUsuario());
	   			    Thread.sleep(3000);
				    if(!gasto.getTipoBien().equals("INMUEBLE")){
				    	new Select(driver.findElement(By.id("sector")))
				    	.selectByVisibleText(gasto.getRubro());
				    	
				    	 switch (gasto.getTipoBien()) {
				     	    case "INSUMO":  
						    	new Select(driver.findElement(By.id("commodityTypes_cb")))
						    	.selectByVisibleText(gasto.getBienes());
				                   break;
				     	    case "BIEN_DE_USO":  
						    	new Select(driver.findElement(By.id("assetTypes_cb")))
						    	.selectByVisibleText(gasto.getTipoBienUsuario());
						    	String[] bienes = gasto.getBienes().split(",");
							    for (String bien: bienes){
							    	new Select(driver.findElement(By.id("assets_cb")))
							    		.selectByVisibleText(bien);
							    }
				     	    	   break;
			                default: 
				     	    	   break;
			     	    } 
				    }
				    // Ubicación 
				    String[] edificios = gasto.getEdificio().split(",");
				    for (String edificio: edificios){
						System.out.println("edificio:"+edificio);
				    	new Select(driver.findElement(By.id("buildings_cb")))
				    		.selectByVisibleText(edificio);
				    	cont++;
				    }				    	
				    if (cont==1){
				    	if(!gasto.getPisos().equals("")){
					    	cont=0;
						    String[] pisos = gasto.getPisos().split(",");
						    for (String piso: pisos){
								System.out.println("Piso:"+piso);
						    	new Select(driver.findElement(By.id("buildingFloors_cb")))
						    		.selectByVisibleText(piso);
						    	cont++;
						    }				    	
					    	if(!gasto.getEspacios().equals("")){
							    if (cont==1){
							        String[] espacios = gasto.getEspacios().split(",");
								    for (String espacio: espacios){
										System.out.println("Espacio:"+espacio);
								    	new Select(driver.findElement(By.id("floorSpaces_cb")))
								    		.selectByVisibleText(espacio);
								    }
							    }
					    	}
				    	}
				    }

		   			((JavascriptExecutor) driver).executeScript("document.getElementById ('payDate__date').removeAttribute('readonly',0);");
		   		    driver.findElement(By.id("payDate__date")).sendKeys(gasto.getFechaPago()); 
			    	driver.findElement(By.id("payAmount__double")).clear();
			        driver.findElement(By.id("payAmount__double")).sendKeys(gasto.getImportePagado());
			        if(!gasto.getComprobante().equals("")){
			   			driver.findElement(By.id("voucherFile"))
			   			.sendKeys(Constantes.PATH_IMAGES+gasto.getComprobante());
			        }
			        new Select(driver.findElement(By.id("status")))
		    		.selectByVisibleText(gasto.getEstado());
			        		   		 
		   		 
					driver.findElement(By.id("btn_accept")).click();
					
					wait.until(ExpectedConditions.elementToBeClickable(By
							.id("succes_notify_div")));
					assertTrue(driver.findElement(By.cssSelector("BODY"))
							.getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
				}
			}
		}
	}

}
