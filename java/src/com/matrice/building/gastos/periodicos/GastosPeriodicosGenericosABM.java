package com.matrice.building.gastos.periodicos;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class GastosPeriodicosGenericosABM extends MatriceTest {

	private Long num = calendario.getTimeInMillis();
	private String nombreGP = "GastoPeriodico_" + num;
	private String nombreGP_Editado = "GastoPeriodicoEditado_" + num;

	@Test
	public void testGastosPeriodicosGenericosABM() throws Exception {
		
		driver.get(baseUrl + "/public/expense/expenses/1");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		driver.findElement(By.id("link_create")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("concept")));

		// validar mensajes de datos requeridos
		driver.findElement(By.id("btn_accept")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*El campo Nombre/Concepto es requerido[\\s\\S]*$"));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*El campo Tipo de Gasto es requerido[\\s\\S]*$"));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*El campo Gasto es requerido[\\s\\S]*$"));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*El campo Inmuebles es requerido[\\s\\S]*$"));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*El campo Tipo de Bien es requerido[\\s\\S]*$"));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*El campo Proveedor es requerido[\\s\\S]*$"));

		driver.findElement(By.id("concept")).clear();
		driver.findElement(By.id("concept")).sendKeys(nombreGP);
		driver.findElement(By.id("holder")).clear();
		driver.findElement(By.id("holder")).sendKeys(nombreGP);

		new Select(driver.findElement(By.id("expenseTypeClassification"))).selectByVisibleText("Servicio");
		new Select(driver.findElement(By.id("expenseType"))).selectByVisibleText("Servicio de Electricidad");

		new Select(driver.findElement(By.id("buildings_cb"))).selectByVisibleText("Central");
		new Select(driver.findElement(By.id("buildingFloors_cb"))).selectByVisibleText("Primer Piso");
		new Select(driver.findElement(By.id("supplier"))).selectByVisibleText("Edelap S.A.");

		driver.findElement(By.id("period__int")).clear();
		driver.findElement(By.id("period__int")).sendKeys("2");
		new Select(driver.findElement(By.name("periodicity"))).selectByVisibleText("Meses");
		driver.findElement(By.id("btn_accept")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*Gasto creado con exito[\\s\\S]*$"));

		// busca y edita
		wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
		driver.findElement(By.id("table_filter")).clear();
		driver.findElement(By.id("table_filter")).sendKeys(nombreGP);

		wait.until(ExpectedConditions.not(ExpectedConditions
				.visibilityOf(driver.findElement(By.id("expenses_processing")))));

		Thread.sleep(2000);
		
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));

		driver.findElement(By.id("btn_edit")).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("concept")));
		driver.findElement(By.id("concept")).clear();
		driver.findElement(By.id("concept")).sendKeys(nombreGP_Editado);
		driver.findElement(By.id("btn_accept")).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*Gasto actualizado con exito[\\s\\S]*$"));

		driver.findElement(By.id("table_filter")).clear();
		driver.findElement(By.id("table_filter")).sendKeys(nombreGP_Editado);
		wait.until(ExpectedConditions.not(ExpectedConditions
				.visibilityOf(driver.findElement(By.id("expenses_processing")))));
		Thread.sleep(2000);
		
		try { 
			assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
					.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));
		} catch (Error e) { 
		verificationErrors.append(e.toString()); 
		} 
		
		
	    driver.findElement(By.linkText("Eliminar")).click();

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By
				.id("succes_notify_div")));
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*Gasto eliminado exitosamente[\\s\\S]*$"));

	}
}
