package com.matrice.building.gastos.periodicos;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelPagos;

import dto.Pagos;

public class CargarPagosExcel extends MatriceTest {

	

	@Test
	public void testCargarPagosExcel() throws Exception {
		
		String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_GASTOS;
		boolean existe,existePago;
		Collection<Pagos> pagos = ReadExcelPagos
				.leerArchivoExcel(archivoDestino, "pagos");

		if (pagos.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (Pagos pago : pagos) {
				driver.get(baseUrl + "/public/expense/expenses/1");
				wait.until(ExpectedConditions.elementToBeClickable(By
						.id("table_filter")));
				driver.findElement(By.id("table_filter")).sendKeys(
						pago.getConcepto());

				wait.until(ExpectedConditions.not(ExpectedConditions
						.visibilityOf(driver.findElement(By
								.id("expenses_processing")))));

				existe = driver.findElement(By.cssSelector("BODY"))
						.getText().matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$");

				if (existe) {
					wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_edit")));
				    driver.findElement(By.id("btn_edit")).click();

				    // busco si existe el pago
					wait.until(ExpectedConditions.elementToBeClickable(By.id("concept")));

				    driver.findElement(By.linkText("Pagos")).click();
					wait.until(ExpectedConditions.not(ExpectedConditions
							.visibilityOf(driver.findElement(By
									.id("payments_processing")))));
				    
				    //buscar fecha de pago si esta ingresada
				    //09-09-2014
				    String fecha = pago.getVto1Fecha().substring(0, 2)+"-"
				    				+ pago.getVto1Fecha().substring(2,4)+"-"
				    					+ pago.getVto1Fecha().substring(4,8);
				    
				    existePago = driver.findElement(By.id("payments")).getText()
				    		.matches("^[\\s\\S]*"+fecha+"[\\s\\S]*$");

				    if(!existePago){

				        driver.findElement(By.id("link_create_floor")).click();
						wait.until(ExpectedConditions.elementToBeClickable(By.id("expiration1__date")));

			   			((JavascriptExecutor) driver).executeScript("document.getElementById ('expiration1__date').removeAttribute('readonly',0);");
			   		    driver.findElement(By.id("expiration1__date")).sendKeys(pago.getVto1Fecha()); 
				    	driver.findElement(By.id("amount1__double")).clear();
				        driver.findElement(By.id("amount1__double")).sendKeys(pago.getVto1Importe());

				        if(!pago.getVto2Fecha().equals("")&&!pago.getVto2Importe().equals("")){
				   			((JavascriptExecutor) driver).executeScript 
			   				("document.getElementById ('expiration2__date').removeAttribute('readonly',0);");
					   		    driver.findElement(By.id("expiration2__date")).sendKeys(pago.getVto2Fecha()); //ddmmaaaa
						        driver.findElement(By.id("amount2__double")).clear();
						        driver.findElement(By.id("amount2__double")).sendKeys(pago.getVto2Importe());
				        }
				        if(!pago.getVto3Fecha().equals("")&&!pago.getVto3Importe().equals("")){
				   			((JavascriptExecutor) driver).executeScript 
			   				("document.getElementById ('expiration3__date').removeAttribute('readonly',0);");
					   		    driver.findElement(By.id("expiration3__date")).sendKeys(pago.getVto2Fecha()); //ddmmaaaa
						        driver.findElement(By.id("amount3__double")).clear();
						        driver.findElement(By.id("amount3__double")).sendKeys(pago.getVto2Importe());
				        }
				    	
						driver.findElement(By.id("btn_accept")).click();
						
						wait.until(ExpectedConditions.elementToBeClickable(By
								.id("succes_notify_div")));
						assertTrue(driver.findElement(By.cssSelector("BODY"))
								.getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
				    	
				    }
				}
			}
		}
	}

}
