package com.matrice.building.gastos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.building.gastos.periodicos.GastosPeriodicosGenericosABM;

@RunWith(Suite.class)
@SuiteClasses({
	GastosPeriodicosGenericosABM.class 
	})

public class AllTestsGastos {

}
