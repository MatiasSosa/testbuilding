package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarGravedadPedidos extends MatriceTest{
	  private boolean existe;
  
	@Test
  public void testBorrarGravedadPedidos() throws Exception {
  //Gravedad de Pedidos
    driver.get(baseUrl + "/public/param/orderSeverities");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("BajaTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar BajaTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Gravedad de Pedido fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe BajaTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("MediaTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar MediaTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Gravedad de Pedido fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe MediaTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("CriticoTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar CriticoTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Gravedad de Pedido fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe CriticoTest");
		driver.findElement(By.id("table_filter")).clear();
	}    

  }

}
