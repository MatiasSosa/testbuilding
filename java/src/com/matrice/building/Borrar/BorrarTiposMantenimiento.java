package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarTiposMantenimiento extends MatriceTest{
	  private boolean existe;
  
	@Test
  public void testBorrarTiposMantenimiento() throws Exception {
    //Tipos de Mantenimiento
    driver.get(baseUrl + "/public/param/maintenanceTypes");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("AscensoresTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar AscensoresTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipo de mantenimiento fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe AscensoresTest");
	}    
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Reparacion ComputadorasTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al [\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar Reparacion ComputadorasTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipo de mantenimiento fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe Reparacion ComputadorasTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
	driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Reparacion AlfombrasTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar Reparacion AlfombrasTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipo de mantenimiento fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe Reparacion AlfombrasTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
	driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Cambio de AceiteTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar Cambio de AceiteTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipo de mantenimiento fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe Cambio de AceiteTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
  }

}
