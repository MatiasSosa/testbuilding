package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarRubros extends MatriceTest{
	  private boolean existe;
	
	@Test
  public void testBorrarRubros() throws Exception {
   //Tipos de Espacio
    driver.get(baseUrl + "/public/param/sectors");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("TestCuadernos");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar TestCuadernos");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El rubro fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe TestCuadernos");
		driver.findElement(By.id("table_filter")).clear();
	}    
    
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("ElectrodomesticosTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar ElectrodomesticosTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El rubro fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe ElectrodomesticosTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
	Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("LibreriaTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar LibreriaTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El rubro fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe LibreriaTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
	Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Maquinaria PesadaTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar Maquinaria PesadaTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El rubro fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe Maquinaria PesadaTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
     }
     }

