package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class EliminarAreas extends MatriceTest {
  private boolean existe;

  @Test
  public void testEliminarAreas() throws Exception {
	  driver.get(baseUrl + "/public/employee/areas");
    driver.findElement(By.id("table_filter")).sendKeys("ContabilidadTest");
    Thread.sleep(10000);
	existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar ContabilidadTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(10000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El area fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe ContabilidadTest");
	}    
	driver.get(baseUrl + "/public/employee/areas");
    driver.findElement(By.id("table_filter")).sendKeys("Contabilidad y FinanzasTest");
    Thread.sleep(10000);
	existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar Contabilidad y FinanzasTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(10000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El area fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe Contabilidad y FinanzasTest");
	}    
	driver.get(baseUrl + "/public/employee/areas");
    driver.findElement(By.id("table_filter")).sendKeys("MarketingTest");
    Thread.sleep(10000);
	existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al [\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar MarketingTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(10000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El area fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe MarketingTest");
	}    
  }

}
