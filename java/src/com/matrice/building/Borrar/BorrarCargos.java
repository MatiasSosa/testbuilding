package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarCargos extends MatriceTest{
	  private boolean existe;
  
	@Test
  public void testBorrarCargos() throws Exception {
    //Cargos
    driver.get(baseUrl + "/public/param/positions");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("DirectorTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar DirectorTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El Cargo fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe DirectorTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("EmpleadoTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar EmpleadoTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El Cargo fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe EmpleadoTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("EncargadoTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar EncargadoTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El Cargo fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe EncargadoTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
   
  }

}
