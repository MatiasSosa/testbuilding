package com.matrice.building.Borrar;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	//LoginAdmin.class,
	BorrarCargos.class,
	BorrarTiposDeEspacio.class,
	BorrarGravedadPedidos.class,
	BorrarEstadosPedidos.class,
	BorrarTiposMantenimiento.class,
	BorrarResultadosMantenimiento.class,
	BorrarProveedores.class,
	BorrarTiposDeInsumos.class,
	BorrarTiposDeBienes.class,
	BorrarTiposDeGastos.class,
	BorrarRubros.class,
	BorrarZonas.class,
	BorrarCiudades.class,
	BorrarProvincias.class,
	BorrarPaises.class
	})

public class AllTestBorrarConfiguracion {

}
