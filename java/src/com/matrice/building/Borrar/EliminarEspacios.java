package com.matrice.building.Borrar;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class EliminarEspacios extends MatriceTest {

  @Test
  public void testEliminarEspacios() throws Exception {
	driver.get(baseUrl + "/publics");
	  Thread.sleep(6000);
	    driver.findElement(By.id("table_filter")).click();
	    driver.findElement(By.id("table_filter")).sendKeys("Edificio1Test");
	    Thread.sleep(10000);
	    boolean existe = driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*Mostrando registros del 1 al [\\s\\S]*$");

		if (existe) {
			System.out.println("Borrar Edificio1Test");
	    driver.findElement(By.id("btn_edit")).click();
	    driver.findElement(By.linkText("Gestionar espacios")).click();
    driver.findElement(By.linkText("Gestionar espacios")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'Eliminar')])[2]")).click();
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El espacio fue eliminado con exito[\\s\\S]*$"));
    assertFalse(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Espacio ComunTest[\\s\\S]*$"));
    driver.findElement(By.linkText("Gestionar espacios")).click();
    driver.findElement(By.id("btn_delete")).click();
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El piso fue eliminado con exito[\\s\\S]*$"));
    assertFalse(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Planta BajaTest[\\s\\S]*$"));
		} else {
			System.out.println("No existe Edificio1Test");
		}
  }
}
