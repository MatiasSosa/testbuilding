package com.matrice.building.Borrar;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarTests extends MatriceTest{
  
	@Test
  public void testBorrarTests() throws Exception {
    //Zonas
    driver.get(baseUrl + "/public/param/zones");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("CentroTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
  //Ciudades
	driver.get(baseUrl + "/public/param/cities");
	  Thread.sleep(6000);
	  driver.findElement(By.id("table_filter")).click();
	  driver.findElement(By.id("table_filter")).sendKeys("Fangshan");
	  Thread.sleep(10000);
	  driver.findElement(By.id("btn_delete")).click();
	  Thread.sleep(6000);
	  driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Provincias
	driver.get(baseUrl + "/public/param/states");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Beijing");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Pa�ses
    driver.get(baseUrl + "/public/param/countries");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("China");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Cargos
    driver.get(baseUrl + "/public/param/positions");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("DirectorTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("EmpleadoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("EncargadoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Tipos de Espacio
    driver.get(baseUrl + "/public/param/spaceTypes");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Espacio ComunTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("OficinaTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Gravedad de Pedidos
    driver.get(baseUrl + "/public/param/orderSeverities");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("BajaTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("MediaTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("CriticoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Estados de Pedidos
    driver.get(baseUrl + "/public/param/orderStatuses");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("En CursoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("No RealizadoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("RealizadoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Tipos de Mantenimiento
    driver.get(baseUrl + "/public/param/maintenanceTypes");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("AscensoresTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Reparacion ComputadorasTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    //Resultados de Mantenimiento
    driver.get(baseUrl + "/public/param/maintenanceResults");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("En CursoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("No RealizadoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("RealizadoTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Proveedores
    driver.get(baseUrl + "/public/param/suppliers");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("ArregloTodo SRLTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("EdelapTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Tipos de insumos
    driver.get(baseUrl + "/public/param/commodityTypes");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Cuadernos rayadosTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Lapiceras BIC NegrasTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("TonerTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Tipos Bienes de Uso
    driver.get(baseUrl + "/public/param/assetTypes");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("ComputadorasTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("PrensaTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("RodadosTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    //Tipos de Gastos
    
    //Rubros
    driver.get(baseUrl + "/public/param/sectors");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("TestCuadernos");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("ElectrodomesticosTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("LibreriaTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Maquinaria PesadaTest");
    Thread.sleep(10000);
    driver.findElement(By.id("btn_delete")).click();
    Thread.sleep(6000);
    driver.findElement(By.xpath("//button[@type='submit']")).click();
  
  }

}
