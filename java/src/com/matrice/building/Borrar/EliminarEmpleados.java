package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class EliminarEmpleados extends MatriceTest {
  private boolean existe;

  @Test
  public void testEliminarEmpleados() throws Exception {
    driver.get(baseUrl + "/public/employee/employees");
    driver.findElement(By.id("table_filter")).sendKeys("mail@mail.com");
    Thread.sleep(7000);
	existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar empleado con mail: mail@mail.com");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El empleado fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe un empleado con el mail mail@mail.com");
	}    
	driver.get(baseUrl + "/public/employee/employees");
    driver.findElement(By.id("table_filter")).sendKeys("mail2@mail.com");
    Thread.sleep(7000);
	existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar empleado con mail: mail2@mail.com");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El empleado fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe un empleado con el mail mail2@mail.com");
	}    
  }

}
