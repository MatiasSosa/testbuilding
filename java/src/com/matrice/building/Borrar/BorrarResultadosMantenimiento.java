package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarResultadosMantenimiento extends MatriceTest{
	  private boolean existe;
  
	@Test
  public void testBorrarResultadosMantenimiento() throws Exception {
    //Resultados de Mantenimiento
    driver.get(baseUrl + "/public/param/maintenanceResults");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("En CursoTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar En CursoTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El resultado de mantenimiento fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe En CursoTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("No RealizadoTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar No RealizadoTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El resultado de mantenimiento fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe No RealizadoTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("RealizadoTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar RealizadoTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El resultado de mantenimiento fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe RealizadoTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    
  }

}
