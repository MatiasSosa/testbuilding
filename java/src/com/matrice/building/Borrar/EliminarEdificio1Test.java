package com.matrice.building.Borrar;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class EliminarEdificio1Test extends MatriceTest {

  @Test
  public void testEliminarEdificio1Test() throws Exception {
    driver.get(baseUrl + "/publics");
    Thread.sleep(8000);
    driver.findElement(By.id("table_filter")).sendKeys("Edificio1Test");
    // Warning: assertTextPresent may require manual changes
    Thread.sleep(8000);
    boolean existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar Edificio1Test");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*La propiedad fue eliminada con exito[\\s\\S]*$"));
	    assertFalse(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Edificio1Test[\\s\\S]*$"));
	} else {
		System.out.println("No existe Edificio1Test");
	}    
  }

}
