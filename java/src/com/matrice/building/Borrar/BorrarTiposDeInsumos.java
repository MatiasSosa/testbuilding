package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarTiposDeInsumos extends MatriceTest{
	  private boolean existe;
	
	@Test
  public void testBorrarTiposDeInsumos() throws Exception {
   //Tipos de Espacio
    driver.get(baseUrl + "/public/param/commodityTypes");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Cuadernos rayadosTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar Cuadernos rayadosTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipo de bien de consumo fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe Cuadernos rayadosTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Lapiceras BIC NegrasTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar Lapiceras BIC NegrasTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipo de bien de consumo fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe Lapiceras BIC NegrasTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
	Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("TonerTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar TonerTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipo de bien de consumo fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe TonerTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
     }
     }

