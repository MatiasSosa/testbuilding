package com.matrice.building.Borrar;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	EliminarEmpleados.class,
	EliminarEspacios.class,
	EliminarEdificio1Test.class,
	EliminarAreas.class
	})

public class AllTestBorrarGestion {

}
