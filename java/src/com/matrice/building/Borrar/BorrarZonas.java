package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarZonas extends MatriceTest{
	  private boolean existe;
  
	@Test
  public void testBorrarZonas() throws Exception {
    //Zonas
    driver.get(baseUrl + "/public/param/zones");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("CentroTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar CentroTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*La zona fue eliminada con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe CentroTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
   
  }

}
