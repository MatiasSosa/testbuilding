package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarProveedores extends MatriceTest{
	  private boolean existe;
  
	@Test
  public void testBorrarProveedores() throws Exception {
    //Provincias
	driver.get(baseUrl + "/public/param/suppliers");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("ArregloTodo SRLTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar ArregloTodo SRLTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El proveedor fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe ArregloTodo SRLTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
	  Thread.sleep(6000);
	    driver.findElement(By.id("table_filter")).click();
	    driver.findElement(By.id("table_filter")).sendKeys("EdelapTest");
	    Thread.sleep(10000);
	    existe = driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

		if (existe) {
			System.out.println("Borrar EdelapTest");
		    driver.findElement(By.id("btn_delete")).click();
		    driver.findElement(By.xpath("//button[@type='submit']")).click();
		    Thread.sleep(7000);
		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El proveedor fue eliminado con exito[\\s\\S]*$"));
		} else {
			System.out.println("No existe EdelapTest");
			driver.findElement(By.id("table_filter")).clear();
		}    
	}

}
