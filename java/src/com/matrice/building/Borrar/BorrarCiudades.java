package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarCiudades extends MatriceTest{
	  private boolean existe;
  
	@Test
  public void testBorrarCiudades() throws Exception {
   //Ciudades
	driver.get(baseUrl + "/public/param/cities");
	  Thread.sleep(6000);
	  driver.findElement(By.id("table_filter")).click();
	  driver.findElement(By.id("table_filter")).sendKeys("Fangshan");
	  Thread.sleep(10000);
	  existe = driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

		if (existe) {
			System.out.println("Borrar Fangshan");
		    driver.findElement(By.id("btn_delete")).click();
		    driver.findElement(By.xpath("//button[@type='submit']")).click();
		    Thread.sleep(7000);
		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*La ciudad fue eliminada con exito[\\s\\S]*$"));
		} else {
			System.out.println("No existe Fangshan");
			driver.findElement(By.id("table_filter")).clear();
		}    
  
  }

}
