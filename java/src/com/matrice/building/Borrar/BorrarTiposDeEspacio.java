package com.matrice.building.Borrar;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;

import com.matrice.general.MatriceTest;

public class BorrarTiposDeEspacio extends MatriceTest{
	  private boolean existe;
	
	@Test
  public void testBorrarTiposDeEspacio() throws Exception {
   //Tipos de Espacio
    driver.get(baseUrl + "/public/param/spaceTypes");
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("Espacio ComunTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al [\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar ComunTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipos de espacio fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe ComunTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
    
    Thread.sleep(6000);
    driver.findElement(By.id("table_filter")).click();
    driver.findElement(By.id("table_filter")).sendKeys("OficinaTest");
    Thread.sleep(10000);
    existe = driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al[\\s\\S]*$");

	if (existe) {
		System.out.println("Borrar OficinaTest");
	    driver.findElement(By.id("btn_delete")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    Thread.sleep(7000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*El tipos de espacio fue eliminado con exito[\\s\\S]*$"));
	} else {
		System.out.println("No existe OficinaTest");
		driver.findElement(By.id("table_filter")).clear();
	}    
     }
     }

