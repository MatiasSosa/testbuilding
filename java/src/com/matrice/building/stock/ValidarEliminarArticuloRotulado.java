package com.matrice.building.stock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class ValidarEliminarArticuloRotulado extends MatriceTest {

  private Long num = calendario.getTimeInMillis();
  private String nombreTipoInsumo = "insumo_rot_"+num;


  @Test
  public void testValidarEliminarArticuloRotulado() throws Exception {

	// Cargar Tipo de bien de consumo rotulado 
    driver.get(baseUrl + "/public/param/commodityTypes");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
	// cargar datos
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreTipoInsumo);
    new Select(driver.findElement(By.id("sector"))).selectByVisibleText("Escritura");
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Tipo de bien de consumo con rotulacion");
    driver.findElement(By.id("hasItemLabel")).click();
    
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("initialStock__int")))));
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("minimumStock__int")))));
    
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Tipo de Bien de Consumo creado con exito[\\s\\S]*$"));
	  
    // Control de stock	  
	driver.get(baseUrl + "/public/commodityStock/commodityStocks");
	wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	driver.findElement(By.id("table_filter")).clear();
	driver.findElement(By.id("table_filter")).sendKeys(nombreTipoInsumo);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("commodityTypes_processing")))));
    
    assertFalse(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Mostrando registros del 0[\\s\\S]*$"));
			    
    driver.findElement(By.id("btn_edit")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_floor")));
    Integer stockActual =  Integer.parseInt(driver.findElement(
 	By.xpath("/html/body/div[1]/div[2]/div[1]/div[6]/div/label")).getText().toString());
			    
    System.out.println("Stock Actual: "+stockActual);
			    
    driver.findElement(By.id("link_create_floor")).click();
    driver.findElement(By.id("tag")).clear();
    driver.findElement(By.id("tag")).sendKeys("etiqueta_"+num);
    driver.findElement(By.id("brand")).clear();
    driver.findElement(By.id("brand")).sendKeys("marca_"+num);
    driver.findElement(By.id("model")).clear();
    driver.findElement(By.id("model")).sendKeys("modelo_"+num);
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Prueba automatizada");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
			    
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Articulo registrado[\\s\\S]*$"));
			    
    Integer stockModificado =  Integer.parseInt(driver.findElement(
    		By.xpath("/html/body/div[1]/div[3]/div[1]/div[6]/div/label")).getText().toString());

    stockActual++;
			    
    assertTrue(stockActual.equals(stockModificado));
			    
	driver.findElement(By.linkText("Articulos registrados")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_assign")));
	driver.findElement(By.id("btn_assign")).click();
	
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
	new Select(driver.findElement(By.id("building"))).selectByVisibleText("Central");
	new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("Primer Piso");
	new Select(driver.findElement(By.id("floorSpace"))).selectByVisibleText("Sala de Capacitacion");
	new Select(driver.findElement(By.id("area"))).selectByVisibleText("Produccion");
	driver.findElement(By.xpath("//button[@type='submit']")).click();
			    
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Asignacion de articulo actualizada[\\s\\S]*$"));

    driver.findElement(By.linkText("Articulos registrados")).click();
    
    wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_unassign")));
    driver.findElement(By.id("btn_unassign")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Asignacion de articulo actualizada[\\s\\S]*$"));
    
    driver.findElement(By.linkText("Articulos registrados")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_delete")));
    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Articulo eliminado con exito[\\s\\S]*$"));
    
  }
}
