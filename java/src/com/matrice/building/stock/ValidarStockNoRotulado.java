package com.matrice.building.stock;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class ValidarStockNoRotulado extends MatriceTest {

  Long num_marca = calendario.getTimeInMillis();
  private String stockNoRot = "Stock_NoRot_"+ num_marca;

  @Test
  public void testValidarStockNoRotulado() throws Exception {
	// Crear Bien de consumo no rotulado
	driver.get(baseUrl + "/public/param/commodityTypes");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create"))); 
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name"))); 
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(stockNoRot);
    new Select(driver.findElement(By.id("sector"))).selectByVisibleText("Papel");
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Bien de consumo no rotulado "+num_marca);
    driver.findElement(By.id("initialStock__int")).clear();
    driver.findElement(By.id("initialStock__int")).sendKeys("50");
    driver.findElement(By.id("minimumStock__int")).clear();
    driver.findElement(By.id("minimumStock__int")).sendKeys("10");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*con exito[\\s\\S]*$"));

    driver.findElement(By.linkText("Gestion")).click();
    driver.findElement(By.linkText("Control de Stock")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(stockNoRot);
    
    Thread.sleep(1000);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("commodityTypes_processing")))));
    Thread.sleep(1000);
    
    driver.findElement(By.id("btn_edit")).click();
    
    assertTrue(driver.findElement(By.xpath("//div[@id='tab-general']/div[6]/div/label"))
    		.getText().matches("^[\\s\\S]*50[\\s\\S]*$"));
    
    driver.findElement(By.id("link_create_floor")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("number")));
    driver.findElement(By.id("number")).clear();
    driver.findElement(By.id("number")).sendKeys("80");
    driver.findElement(By.id("comments")).clear();
    driver.findElement(By.id("comments")).sendKeys("Se agregaron treinta unidades");
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));

    driver.findElement(By.id("link_create_space")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("number")));
    driver.findElement(By.id("number")).clear();
    driver.findElement(By.id("number")).sendKeys("20");
    new Select(driver.findElement(By.id("building"))).selectByVisibleText("Central");
    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("Primer Piso");
    new Select(driver.findElement(By.id("floorSpace"))).selectByVisibleText("Sala de Capacitacion");
    new Select(driver.findElement(By.id("area"))).selectByVisibleText("Investigacion de Mercado");
    driver.findElement(By.id("btn_accept")).click();
    
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
    		.matches("^[\\s\\S]*Stock asignado[\\s\\S]*$"));
    
    driver.findElement(By.linkText("Historial")).click();

    System.out.println("Esta el 20: "+driver.findElement(By.xpath("//table[@id='stockHistory']/tbody/tr[1]/td[3]"))
    		.getText().matches("^[\\s\\S]*20[\\s\\S]*$"));

    assertTrue(driver.findElement(By.xpath("//table[@id='stockHistory']/tbody/tr[1]/td[3]"))
    		.getText().matches("^[\\s\\S]*20[\\s\\S]*$"));

    System.out.println("Esta el 30: "+driver.findElement(By.xpath("//table[@id='stockHistory']/tbody/tr[2]/td[3]"))
    		.getText().matches("^[\\s\\S]*30[\\s\\S]*$"));

    assertTrue(driver.findElement(By.xpath("//table[@id='stockHistory']/tbody/tr[2]/td[3]"))
    		.getText().matches("^[\\s\\S]*30[\\s\\S]*$"));
    
  }


}
