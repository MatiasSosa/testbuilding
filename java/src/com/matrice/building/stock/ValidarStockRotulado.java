package com.matrice.building.stock;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelTipoBienConsumo;

import dto.TipoBienConsumo;

public class ValidarStockRotulado extends MatriceTest {

  Long num_marca = calendario.getTimeInMillis();

  @Test
  public void testValidarStockRotulado() throws Exception {
	  
	String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;
	List<TipoBienConsumo> tiposInsumos = ReadExcelTipoBienConsumo
			.leerArchivoExcel(archivoDestino, "TiposBienesConsumo");

	if (tiposInsumos.size() != 0) {
		for(TipoBienConsumo stock: tiposInsumos){
			// Buscar bien de consumo rotulado
			if(stock.getRotulacion().equals("S")){
				driver.get(baseUrl + "/public/commodityStock/commodityStocks");
				wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
				driver.findElement(By.id("table_filter")).clear();
				driver.findElement(By.id("table_filter")).sendKeys(stock.getNombre());
			    wait.until(ExpectedConditions.not(ExpectedConditions
			    		.visibilityOf(driver.findElement(By.id("commodityTypes_processing")))));
			    
			    assertFalse(driver.findElement(By.cssSelector("BODY")).getText()
			    		.matches("^[\\s\\S]*Mostrando registros del 0[\\s\\S]*$"));
			    
			    driver.findElement(By.id("btn_edit")).click();
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_floor")));
			    Integer stockActual =  Integer.parseInt(driver.findElement(
			    		By.xpath("/html/body/div[1]/div[2]/div[1]/div[6]/div/label")).getText().toString());
			    
			    System.out.println(stock.getNombre()+", Stock Actual: "+stockActual);
			    
			    driver.findElement(By.id("link_create_floor")).click();
			    driver.findElement(By.id("tag")).clear();
			    driver.findElement(By.id("tag")).sendKeys("etiqueta_"+num_marca);
			    driver.findElement(By.id("brand")).clear();
			    driver.findElement(By.id("brand")).sendKeys("marca_"+num_marca);
			    driver.findElement(By.id("model")).clear();
			    driver.findElement(By.id("model")).sendKeys("modelo_"+num_marca);
			    driver.findElement(By.id("description")).clear();
			    driver.findElement(By.id("description")).sendKeys("Prueba automatizada");
			    driver.findElement(By.id("btn_accept")).click();
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
			    
			    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			    		.matches("^[\\s\\S]*Articulo registrado[\\s\\S]*$"));
			    
			    Integer stockModificado =  Integer.parseInt(driver.findElement(
			    		By.xpath("/html/body/div[1]/div[3]/div[1]/div[6]/div/label")).getText().toString());
			    
			    System.out.println(stock.getNombre()+",Stock Modificado: " + stockModificado);
			    
			    stockActual++;
			    
			    assertTrue(stockActual.equals(stockModificado));
			    
			    driver.findElement(By.linkText("Articulos registrados")).click();
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_assign")));
			    driver.findElement(By.id("btn_assign")).click();
			    
			    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
			    new Select(driver.findElement(By.id("building"))).selectByVisibleText("Central");
			    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText("Primer Piso");
			    new Select(driver.findElement(By.id("floorSpace"))).selectByVisibleText("Sala de Capacitacion");
			    new Select(driver.findElement(By.id("area"))).selectByVisibleText("Produccion");
			    driver.findElement(By.xpath("//button[@type='submit']")).click();
			    
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
			    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			    		.matches("^[\\s\\S]*Asignacion de articulo actualizada[\\s\\S]*$"));

			    driver.findElement(By.linkText("Articulos registrados")).click();
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_delete")));
			    driver.findElement(By.id("btn_delete")).click();
			    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));
			    driver.findElement(By.xpath("//button[@type='submit']")).click();
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("session_error_notify_div")));
			    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			    		.matches("^[\\s\\S]*El articulo esta asignado, no puede ser eliminado[\\s\\S]*$"));
			    
			}
		}
	}
  }
}
