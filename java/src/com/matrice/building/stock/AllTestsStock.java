package com.matrice.building.stock;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
	ValidarStockNoRotulado.class,
	ValidarStockRotulado.class,
	ValidarEliminarArticuloRotulado.class
	})

public class AllTestsStock {

}
