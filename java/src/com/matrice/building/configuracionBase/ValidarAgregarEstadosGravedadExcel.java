package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelTupla;

import dto.Tupla;

public class ValidarAgregarEstadosGravedadExcel extends MatriceTest {
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarGravedadExcel() throws Exception {
   
    String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;
    List<Tupla> tuplas = ReadExcelTupla.leerArchivoExcel(archivoDestino,"Gravedades");
        
    if(tuplas.size()!=0){
    	// agrego los tipos definidos en el archivo
    	for(Tupla tupla: tuplas){
    	    driver.get(baseUrl + "/public/param/orderSeverities");
    	    
    	 // busco si existe un area con el mismo nombre
   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
   		    driver.findElement(By.id("table_filter")).sendKeys(tupla.getCampo1());
   		    
   		    wait.until(ExpectedConditions.not(ExpectedConditions
   		    	.visibilityOf(driver.findElement(By.id("orderSeverities_processing")))));
   		 
   			noExiste = !driver.findElement(By.id("orderSeverities")).getText()
   					.matches("^[\\s\\S]*"+tupla.getCampo1()+"[\\s\\S]*$");
   			
   			if (noExiste) {
     	    driver.findElement(By.id("link_create")).click();
     	    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
     	   driver.findElement(By.id("name")).clear();
     	    driver.findElement(By.id("name")).sendKeys(tupla.getCampo1());
     	    ((JavascriptExecutor) driver).executeScript ("document.getElementById ('color').removeAttribute('readonly',0);");
     	    driver.findElement(By.id("color")).sendKeys(tupla.getCampo2());
     	    driver.findElement(By.id("name")).click();
     	    driver.findElement(By.id("btn_accept")).click();
     	    
     	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
     	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
  		    	
   				System.out.println("Se agrego Gravedad"+tupla.getCampo1());
	   		    
   			}else{
   				System.out.println("La Gravedad "+tupla.getCampo1()+" ya existe");
   			}
    	}
    }
  }
}
