package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelTipoBienConsumo;

import dto.TipoBienConsumo;

public class ValidarAgregarTiposBienesConsumoExcel extends MatriceTest {
	private boolean noExiste;

	@Test
	public void testValidarAgregarTiposBienesConsumoExcel() throws Exception {

		String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;
		
		List<TipoBienConsumo> tiposInsumos = ReadExcelTipoBienConsumo
				.leerArchivoExcel(archivoDestino, "TiposBienesConsumo");

		if (tiposInsumos.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (TipoBienConsumo tipo : tiposInsumos) {
				driver.get(baseUrl + "/public/param/commodityTypes");

				// busco si existe un area con el mismo nombre
				wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
				driver.findElement(By.id("table_filter")).sendKeys(tipo.getNombre());

				wait.until(ExpectedConditions.not(ExpectedConditions
						.visibilityOf(driver.findElement(By.id("commodityTypes_processing")))));

				noExiste = driver.findElement(By.cssSelector("BODY")).getText()
						.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");

				if (noExiste) {
					driver.findElement(By.id("link_create")).click();
					wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
					driver.findElement(By.id("name")).clear();
					driver.findElement(By.id("name")).sendKeys(tipo.getNombre());
					new Select(driver.findElement(By.id("sector"))).selectByVisibleText(tipo.getRubro());
					driver.findElement(By.id("description")).clear();
					driver.findElement(By.id("description")).sendKeys(tipo.getDescripcion());

					if (!tipo.getFoto().equals("-1")) {
						driver.findElement(By.id("picture")).sendKeys(
								Constantes.PATH_IMAGES + tipo.getFoto());
					}

					if (tipo.getRotulacion().equals("S")) {
						driver.findElement(By.id("hasItemLabel")).click();
						wait.until(ExpectedConditions.not(ExpectedConditions
								.visibilityOf(driver.findElement(By.id("initialStock__int")))));
						wait.until(ExpectedConditions.not(ExpectedConditions
								.visibilityOf(driver.findElement(By.id("minimumStock__int")))));
					} else {
						if (!tipo.getStockInicial().equals("-1")) {
							driver.findElement(By.id("initialStock__int")).clear();
							driver.findElement(By.id("initialStock__int")).sendKeys(tipo.getStockInicial());
						}
						if (!tipo.getStockMinimo().equals("-1")) {
							driver.findElement(By.id("minimumStock__int")).clear();
							driver.findElement(By.id("minimumStock__int")).sendKeys(tipo.getStockMinimo());
						}
					}
					
					driver.findElement(By.id("btn_accept")).click();
					
					wait.until(ExpectedConditions.elementToBeClickable(By
							.id("succes_notify_div")));
					assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
							.matches("^[\\s\\S]*Tipo de Bien de Consumo creado con exito[\\s\\S]*$"));

					System.out.println("Se agrego tipo de bien de consumo: "+ tipo.getNombre());

				} else {
					System.out.println("El tipo de bien de consumo "
							+ tipo.getNombre() + " ya existe");
				}
			}
		}
	}
}
