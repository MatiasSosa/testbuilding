package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelZonas;

import dto.Zona;

public class ValidarAgregarZonasExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarZonasExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;
   Collection<Zona> zonas = ReadExcelZonas.leerArchivoExcel(archivoDestino, "Zonas");

   if(zonas.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Zona zona: zonas){
	   			driver.get(baseUrl + "/public/param/zones");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(zona.getNombreZona());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("zones_processing")))));
	   		 
	   			noExiste = !(driver.findElement(By.id("zones")).getText()
	   					.matches("^[\\s\\S]*"+zona.getNombreZona()+"[\\s\\S]*$"));
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    
		   		    
		   		    new Select(driver.findElement(By.id("country"))).selectByVisibleText(zona.getNombrePais());
		   		    new Select(driver.findElement(By.id("state"))).selectByVisibleText(zona.getNombreProvincia());
		   		    new Select(driver.findElement(By.id("city"))).selectByVisibleText(zona.getNombreCiudad());
		   		    
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(zona.getNombreZona());
		   		   
		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego zona "+zona.getNombreCiudad());
	   			}else{
	   				System.out.println("La zona "+zona.getNombreCiudad()+" ya existe");
	   			}
	   		}
   }   		
  }
}
