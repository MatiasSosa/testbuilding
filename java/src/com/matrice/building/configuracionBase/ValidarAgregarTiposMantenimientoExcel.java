package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelTriple;

import dto.Triple;

public class ValidarAgregarTiposMantenimientoExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarTiposBienesUsoExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;

   Collection<Triple> tipos = ReadExcelTriple.leerArchivoExcel(archivoDestino, "TiposMantenimiento");

   if(tipos.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Triple tipo: tipos){
	   			driver.get(baseUrl + "/public/param/maintenanceTypes");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(tipo.getCampo1());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("maintenanceTypes_processing")))));
	   		 
	   			noExiste = !(driver.findElement(By.id("maintenanceTypes")).getText()
	   					.matches("^[\\s\\S]*"+tipo.getCampo1() +"[\\s\\S]*$"));
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
			   		driver.findElement(By.id("name")).clear();
			   	    driver.findElement(By.id("name")).sendKeys(tipo.getCampo1());
			   	    if (tipo.getCampo2().equalsIgnoreCase("Si")){
			   	    driver.findElement(By.xpath("(//input[@id='isPreventive'])[2]")).click();}
			   	    if (tipo.getCampo3().equalsIgnoreCase("Si")){
			   	    driver.findElement(By.xpath("(//input[@id='isCorrective'])[2]")).click();}
			   	    driver.findElement(By.id("btn_accept")).click();
			   	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego tipo de mantenimiento "+tipo.getCampo2());
		   		    
	   			}else{
	   				System.out.println("El tipo de mantenimiento  "+tipo.getCampo2()+" ya existe");
	   			}
	   		}
   }   		
  }
}
