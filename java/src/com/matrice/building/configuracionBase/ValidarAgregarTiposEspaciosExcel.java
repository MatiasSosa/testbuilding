package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelListadoTextos;

public class ValidarAgregarTiposEspaciosExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarTiposEspaciosExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;

   Collection<String> tiposEspacioss = ReadExcelListadoTextos.leerArchivoExcel(archivoDestino, "Tipos Espacios");

   if(tiposEspacioss.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(String tiposEspacios: tiposEspacioss){
	   			driver.get(baseUrl + "/public/param/spaceTypes");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(tiposEspacios);
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("spaceTypes_processing")))));
	   		 
	   			noExiste = !(driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*"+tiposEspacios +"[\\s\\S]*$"));
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(tiposEspacios);
		   		   
		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego tipo de espacio "+tiposEspacios);
		   		    
	   			}else{
	   				System.out.println("El tipo de espacio "+tiposEspacios+" ya existe");
	   			}
	   		}
   }   		
  }
}
