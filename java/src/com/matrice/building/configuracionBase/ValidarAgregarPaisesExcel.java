package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelListadoTextos;

public class ValidarAgregarPaisesExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarPaisesExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;

   Collection<String> paises = ReadExcelListadoTextos.leerArchivoExcel(archivoDestino, "Paises");

   if(paises.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(String pais: paises){
	   			driver.get(baseUrl + "/public/param/countries");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(pais);
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("countries_processing")))));
	   		 
	   			noExiste = !(driver.findElement(By.id("countries")).getText()
	   					.matches("^[\\s\\S]*"+pais+"[\\s\\S]*$"));
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(pais);
		   		   
		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego pais "+pais);
		   		    
	   			}else{
	   				System.out.println("El pais "+pais+" ya existe");
	   			}
	   		}
   }   		
  }
}
