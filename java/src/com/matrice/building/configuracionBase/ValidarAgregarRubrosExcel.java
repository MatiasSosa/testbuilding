package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelListadoTextos;

public class ValidarAgregarRubrosExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarRubrosExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;
   Collection<String> rubros = ReadExcelListadoTextos.leerArchivoExcel(archivoDestino, "Rubros");

   if(rubros.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(String rubro: rubros){
	    			driver.get(baseUrl + "/public/param/sectors");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(rubro);
	   		     
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("sectors_processing")))));
	   		 
	   			noExiste = !(driver.findElement(By.id("sectors")).getText()
	   					.matches("^[\\s\\S]*"+rubro+"[\\s\\S]*$"));
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(rubro);
		   		    
		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego rubro "+rubro);
		   		     
	   			}else{
	   				System.out.println("El rubro "+rubro+" ya existe" );
	   			}
	   		}
   }   		
  }
}
