package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelTipoGastos;

import dto.TipoGastos;

public class ValidarAgregarTiposGastosExcel extends MatriceTest {
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarTiposGastosExcel() throws Exception {
   
    String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;
    List<TipoGastos> tiposGastos = ReadExcelTipoGastos.leerArchivoExcel(archivoDestino,"TipoGastos");
    
    if(tiposGastos.size()!=0){
    	// agrego los tipos definidos en el archivo
    	for(TipoGastos tipo: tiposGastos){
    	    driver.get(baseUrl + "/public/param/expenseTypes/index");
    	    
    	 // busco si existe un area con el mismo nombre
   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
   		    driver.findElement(By.id("table_filter")).sendKeys(tipo.getNombre());
   		    
   		    wait.until(ExpectedConditions.not(ExpectedConditions
   		    	.visibilityOf(driver.findElement(By.id("expenseTypes_processing")))));
   		 
   			noExiste = driver.findElement(By.cssSelector("BODY")).getText()
   					.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
   			
   			if (noExiste) {
     	    driver.findElement(By.id("link_create")).click();
     	    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
     	    driver.findElement(By.id("name")).clear();
     	    driver.findElement(By.id("name")).sendKeys(tipo.getNombre());
     	    new Select(driver.findElement(By.id("expenseTypeClassification"))).selectByVisibleText(tipo.getTipo());
     	   
     	    switch (tipo.getTipoBien()) {
     	    
	     	    case "INMUEBLE":  
	     	    	   new Select(driver.findElement(By.id("goodType"))).selectByValue("1building");
	                   break;
	     	    case "BIEN_DE_USO":  
	     	    	   new Select(driver.findElement(By.id("goodType"))).selectByValue("2asset");
	     	    	   break;
	     	    case "INSUMO":  
		  	    	   new Select(driver.findElement(By.id("goodType"))).selectByValue("3commodity");
		  	    	   break;
                default: 
	     	    	   new Select(driver.findElement(By.id("goodType"))).selectByValue("4other");
	     	    	   break;
     	    } 
     	   
     	    if(tipo.getIsAllPropertiesGeneral().equals("Si")){
         	    driver.findElement(By.xpath("(//input[@id='isAllPropertiesGeneral'])["+tipo.getIsAllPropertiesGeneral()+"]")).click();
     	    }
     	    driver.findElement(By.id("btn_accept")).click();
     	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
     	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
     				.matches("^[\\s\\S]*Tipo de Gasto creado con exito[\\s\\S]*$"));
   				
   				System.out.println("Se agrego tipo de gasto "+tipo.getNombre());
	   		    
   			}else{
   				System.out.println("El tipo de gasto "+tipo.getNombre()+" ya existe");
   			}
    	}
    }
  }
}
