package com.matrice.building.configuracionBase;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({  
		ValidarAgregarCargosExcel.class,
		ValidarAgregarPaisesExcel.class, 
		ValidarAgregarRubrosExcel.class,
		ValidarAgregarTiposEspaciosExcel.class,
		ValidarAgregarTiposMantenimientoExcel.class,
		ValidarAgregarTiposBienesUsoExcel.class,
		ValidarAgregarProvinciasExcel.class,
		ValidarAgregarCiudadesExcel.class,
		ValidarAgregarZonasExcel.class,
		ValidarAgregarTiposGastosExcel.class,
		ValidarAgregarEstadosGravedadExcel.class,
		ValidarAgregarEstadosPedidosExcel.class,
		ValidarAgregarProveedoresExcel.class,
		ValidarAgregarResultadosMantenimientoExcel.class,
		ValidarAgregarTiposBienesConsumoExcel.class
		
		
		})
public class AllTestsConfiguracionExcel {

}
