package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelCiudades;

import dto.Ciudad;

public class ValidarAgregarCiudadesExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarCiudadesExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;

   Collection<Ciudad> ciudades = ReadExcelCiudades.leerArchivoExcel(archivoDestino, "Ciudades");

   if(ciudades.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Ciudad ciudad: ciudades){
	   			driver.get(baseUrl + "/public/param/cities");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(ciudad.getNombreCiudad());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("cities_processing")))));
	   		 
	   			noExiste = !(driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*"+ciudad.getNombreCiudad()+"[\\s\\S]*$"));
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    
		   		    
		   		    new Select(driver.findElement(By.id("country"))).selectByVisibleText(ciudad.getNombrePais());
		   		    new Select(driver.findElement(By.id("state"))).selectByVisibleText(ciudad.getNombreProvincia());
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(ciudad.getNombreCiudad());
		   		   
		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego Ciudad "+ciudad.getNombreCiudad());
		   		    
	   			}else{
	   				System.out.println("La Ciudad "+ciudad.getNombreCiudad()+" ya existe");
	   			}
	   		}
   }   		
  }
}
