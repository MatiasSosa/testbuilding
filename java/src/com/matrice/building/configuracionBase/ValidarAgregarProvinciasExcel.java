package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelProvincias;

import dto.Provincia;

public class ValidarAgregarProvinciasExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarProvinciasExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;
   Collection<Provincia> provincias = ReadExcelProvincias.leerArchivoExcel(archivoDestino, "Provincias");

   if(provincias.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Provincia provincia: provincias){
	   			driver.get(baseUrl + "/public/param/states");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(provincia.getNombreProvincia());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("states_processing")))));
	   		 
	   			noExiste = !(driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*"+provincia.getNombreProvincia()+"[\\s\\S]*$"));
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    
		   		    new Select(driver.findElement(By.id("country"))).selectByVisibleText(provincia.getNombrePais());
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(provincia.getNombreProvincia());
		   		   
		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego provincia "+provincia.getNombreProvincia());
		   		    
	   			}else{
	   				System.out.println("La provincia "+provincia.getNombreProvincia()+" ya existe");
	   			}
	   		}
   }   		
  }
}
