package com.matrice.building.configuracionBase;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelTupla;

import dto.Tupla;

public class ValidarAgregarTiposBienesUsoExcel extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testValidarAgregarTiposBienesUsoExcel() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_CONFIGURACION;

   Collection<Tupla> tipos = ReadExcelTupla.leerArchivoExcel(archivoDestino, "TiposBienesUso");

   if(tipos.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Tupla tipo: tipos){
	   			driver.get(baseUrl + "/public/param/assetTypes");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(tipo.getCampo2());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("assetTypes_processing")))));
	   		 
	   			noExiste = !(driver.findElement(By.id("assetTypes")).getText()
	   					.matches("^[\\s\\S]*"+tipo.getCampo2()+"[\\s\\S]*$"));
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
		   		    driver.findElement(By.id("name")).clear();
		   		    driver.findElement(By.id("name")).sendKeys(tipo.getCampo2());
		   		    new Select(driver.findElement(By.id("sector"))).selectByVisibleText(tipo.getCampo1());
		   		    driver.findElement(By.id("btn_accept")).click();
		   	   
		   		    
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    
		   		    System.out.println("Se agrego tipo de bien de uso "+tipo.getCampo2());
		   		    
	   			}else{
	   				System.out.println("El tipo de bien de us "+tipo.getCampo2()+" ya existe");
	   			}
	   		}
   }   		
  }
}
