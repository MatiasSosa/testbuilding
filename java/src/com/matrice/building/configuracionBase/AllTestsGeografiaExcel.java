package com.matrice.building.configuracionBase;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.general.LoginAdmin;

@RunWith(Suite.class)
@SuiteClasses({  
		LoginAdmin.class,
		ValidarAgregarPaisesExcel.class, 
		ValidarAgregarProvinciasExcel.class,
		ValidarAgregarCiudadesExcel.class,
		ValidarAgregarZonasExcel.class,
		
		})
public class AllTestsGeografiaExcel {

}
