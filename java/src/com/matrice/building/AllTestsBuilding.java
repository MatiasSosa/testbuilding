package com.matrice.building;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.building.administracion.AllTestsAdministracion;
import com.matrice.building.configuracion.AllTestsConfiguracion;
import com.matrice.building.configuracionBase.AllTestsConfiguracionExcel;
import com.matrice.building.configuracionGeo.AllTestsGeografia;
import com.matrice.building.gastos.AllTestsGastos;
import com.matrice.building.gestion.AllTestsGestion;
import com.matrice.building.gestion.empleados.CrearEmpleadoTestGral;
import com.matrice.building.gestion.mantenimientos.AllTestMantenimiento;
import com.matrice.building.gestion.pedidos.AllTestsPedidos;
import com.matrice.building.stock.AllTestsStock;
import com.matrice.general.LoginAdmin;
import com.matrice.general.TestCerrar;

@RunWith(Suite.class)
@SuiteClasses({
	
		// Test Iniciar sesion
		LoginAdmin.class,
		// Tests con datos para las pruebas
		CrearEmpleadoTestGral.class,
		AllTestsConfiguracionExcel.class,
		AllTestsGeografia.class,
		AllTestsConfiguracion.class,
		// Tests de funcionalidad
		AllTestsGestion.class,
		AllTestsStock.class,
		AllTestsGastos.class,
		AllTestsPedidos.class,
		AllTestMantenimiento.class,
		
		AllTestsAdministracion.class,
		// Cerrar
		TestCerrar.class })

public class AllTestsBuilding {

}
