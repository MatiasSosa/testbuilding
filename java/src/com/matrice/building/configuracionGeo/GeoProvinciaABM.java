package com.matrice.building.configuracionGeo;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.MatriceTest;

public class GeoProvinciaABM extends MatriceTest {

  private Long num = calendario.getTimeInMillis();
  private String nombrePais = "GeoPais_"+num;
  private String nombreProv = "GeoProvincia_"+num;
  private String nombreProvEditado = "GeoProvinciaEditado_"+num;
  @Test
  public void testGeoProvinciaABM() throws Exception {
   
	//crear pais 
    crearPais(nombrePais);
    //Provincia
    driver.get(baseUrl + "/public/param/states");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));

    // validar mensajes de datos requeridos
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Nombre es requerido[\\s\\S]*$"));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Pais es requerido[\\s\\S]*$"));

    //cargar datos
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreProv);
    new Select(driver.findElement(By.id("country"))).selectByVisibleText(nombrePais);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Provincia creada con exito[\\s\\S]*$"));

  //validar que el nombre este ocupado
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreProv);
    new Select(driver.findElement(By.id("country"))).selectByVisibleText(nombrePais);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("error_notify_div")));
	assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*El campo Nombre se encuentra ocupado[\\s\\S]*$"));

	//busca y edita
	driver.get(baseUrl + "/public/param/states");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).sendKeys(nombreProv);
        
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("states_processing")))));
    Thread.sleep(2000);
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));
    Thread.sleep(2000);    
    driver.findElement(By.id("btn_edit")).click();
    
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(nombreProvEditado);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*con exito[\\s\\S]*$"));
    
	//valido que el nombre modificado no aparece mas
	driver.findElement(By.id("table_filter")).clear();
	driver.findElement(By.id("table_filter")).sendKeys(nombreProv);
	wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("states_processing")))));

    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$"));   

	//busco y borro el editado
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(nombreProvEditado);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("states_processing")))));
    Thread.sleep(2000);
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));   
    Thread.sleep(2000);

    driver.findElement(By.id("btn_delete")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));        
    driver.findElement(By.xpath("//button[@type='submit']")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*La provincia fue eliminada con exito[\\s\\S]*$"));

    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
    driver.findElement(By.id("table_filter")).clear();
    driver.findElement(By.id("table_filter")).sendKeys(nombreProvEditado);
    Thread.sleep(2000);
    wait.until(ExpectedConditions.not(ExpectedConditions
    		.visibilityOf(driver.findElement(By.id("states_processing")))));
    Thread.sleep(2000);
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$"));
    
    borrarPais(nombrePais);
    }
  
  
  public void crearPais(String pais) throws Exception {
	//crear pais 
	driver.get(baseUrl + "/public/param/countries");
    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
    driver.findElement(By.id("link_create")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys(pais);
    driver.findElement(By.id("btn_accept")).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			.matches("^[\\s\\S]*creado con exito[\\s\\S]*$"));
  }
  public void borrarPais(String pais) throws Exception {
		driver.get(baseUrl + "/public/param/countries");
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	    driver.findElement(By.id("table_filter")).clear();
	    driver.findElement(By.id("table_filter")).sendKeys(pais);
	    wait.until(ExpectedConditions.not(ExpectedConditions
	    		.visibilityOf(driver.findElement(By.id("countries_processing")))));
	    Thread.sleep(2000);
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$"));   
	    Thread.sleep(2000);

	    driver.findElement(By.id("btn_delete")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']")));        
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
	    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
				.matches("^[\\s\\S]*El Pais fue eliminado con exito[\\s\\S]*$"));

  }
}
