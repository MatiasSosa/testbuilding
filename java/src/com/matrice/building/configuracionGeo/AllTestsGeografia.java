package com.matrice.building.configuracionGeo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	GeoPaisesABM.class,
	GeoProvinciaABM.class,
	GeoCiudadesABM.class,
	GeoZonasABM.class
  })

public class AllTestsGeografia {
	
}
