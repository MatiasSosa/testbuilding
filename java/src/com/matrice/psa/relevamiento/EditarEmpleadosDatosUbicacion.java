package com.matrice.psa.relevamiento;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelEmpleados;

import dto.Empleados;

public class EditarEmpleadosDatosUbicacion extends MatriceTest {
  
  private boolean existe;
  
  @Test
  public void testEditarEmpleadosDatosUbicacion() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;

   List<Empleados> empleados = ReadExcelEmpleados.leerArchivoExcel(archivoDestino,"empleados");

   if(empleados.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Empleados empl: empleados){
	   			driver.get(baseUrl + "/public/employee/employees");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(empl.getEmail());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("employees_processing")))));
	   		 
	   			existe = driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$");
	   			
	   			if (existe) {
	   				// edito los datos del empleado
	   			    Thread.sleep(2000);
	   			    driver.findElement(By.id("btn_edit")).click();
	   			    wait.until(ExpectedConditions.elementToBeClickable(By.id("firstName")));
		   		    if(!empl.getEdificio().equals("-1")){
		   		    	new Select(driver.findElement(By.id("building")))
		   		    		.selectByVisibleText(empl.getEdificio());
		   		    }
			   		if(!empl.getArea().equals("-1")){
		   		    	new Select(driver.findElement(By.id("area")))
	   		    		.selectByVisibleText(empl.getArea());
			   		}		   			
			   		if(!empl.getPiso().equals("-1")){
		   		    	new Select(driver.findElement(By.id("buildingFloor")))
	   		    		.selectByVisibleText(empl.getPiso());
			   			
			   		}
			   		if(!empl.getEspacio().equals("-1")){
			   			new Select(driver.findElement(By.id("floorSpace")))
	   		    		.selectByVisibleText(empl.getEspacio());
			   		}
		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    System.out.println("Se edito datos de ubicacion del Empleado: "+empl.getApellido()+", "+empl.getNombre());
	   			}else{
	   				System.out.println("El empleado "+empl.getApellido()+" "+empl.getNombre()+" no existe");
	   			}
	   		}
   }   		
  }
}
