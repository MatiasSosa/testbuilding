package com.matrice.psa.relevamiento;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelInmuebles;

import dto.Inmueble;

/* 
 *  EditarResponsablePorEdificio permite completar el responsable de un edificio, 
 *  previamente cargado en la secci�n de empleados   
 */
public class EditarResponsablePorEdificio extends MatriceTest {
  @Test
  public void testEditarResponsablePorEdificio() throws Exception {

		System.out.println(">> testEditarResponsablePorEdificio");
		String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;

		boolean existe;

		Collection<Inmueble> inmuebles = ReadExcelInmuebles
				.leerArchivoExcel(archivoDestino, "Inmueble");

		if (inmuebles.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (Inmueble inmueble : inmuebles) {
			    if(!inmueble.getResponsable().equals("")){
					driver.get(baseUrl + "/public/building/buildings");
					
					wait.until(ExpectedConditions.elementToBeClickable(By
							.id("table_filter")));
					
					driver.findElement(By.id("table_filter")).sendKeys(
							inmueble.getNombre());

					wait.until(ExpectedConditions.not(ExpectedConditions
							.visibilityOf(driver.findElement(By
									.id("buildings_processing")))));

					existe = driver.findElement(By.cssSelector("BODY")).getText()
							.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$");

					if (existe) {
					    wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_edit")));
						driver.findElement(By.id("btn_edit")).click();
						wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_tab-buildingFloors")));
				    	new Select(driver.findElement(By.id("employee"))).selectByVisibleText(inmueble.getResponsable());
					    driver.findElement(By.id("btn_accept")).click();
					    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
					    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
					    System.out.println("Se edito inmueble: "+inmueble.getNombre()+", responsable: "+inmueble.getResponsable());
					}else{
					    System.out.println("No existe el inmueble: "+inmueble.getNombre());
					}
			    }

			}
		}
  }
  }


