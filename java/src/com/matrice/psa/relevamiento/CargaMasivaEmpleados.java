package com.matrice.psa.relevamiento;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelEmpleados;

import dto.Empleados;

public class CargaMasivaEmpleados extends MatriceTest {
  
  private boolean noExiste;
  
  @Test
  public void testCargaMasivaEmpleados() throws Exception {
   
   String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;
   
   List<Empleados> empleados = ReadExcelEmpleados.leerArchivoExcel(archivoDestino,"empleados");

   if(empleados.size()!=0){
   		// agrego los tipos definidos en el archivo
	   		for(Empleados empl: empleados){
	   			driver.get(baseUrl + "/public/employee/employees");
	   			// busco si existe un area con el mismo nombre
	   			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
	   		    driver.findElement(By.id("table_filter")).sendKeys(empl.getEmail());
	   		    
	   		    wait.until(ExpectedConditions.not(ExpectedConditions
	   		    	.visibilityOf(driver.findElement(By.id("employees_processing")))));
	   		 
	   			noExiste = driver.findElement(By.cssSelector("BODY")).getText()
	   					.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");
	   			
	   			if (noExiste) {
	   				// si no existe la agrego
		   			wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
		   		    driver.findElement(By.id("link_create")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("firstName")));
		   		    //datos requeridos
		   		    driver.findElement(By.id("firstName")).clear();
		   		    driver.findElement(By.id("firstName")).sendKeys(empl.getNombre());
		   		    driver.findElement(By.id("lastName")).clear();
		   		    driver.findElement(By.id("lastName")).sendKeys(empl.getApellido());
		   		    driver.findElement(By.id("idNumber")).clear();
		   		    driver.findElement(By.id("idNumber")).sendKeys(empl.getDocumento());
		   		    driver.findElement(By.id("email")).clear();
		   		    driver.findElement(By.id("email")).sendKeys(empl.getEmail());
		   		    if(!empl.getEdificio().equals("-1")){
		   		    	new Select(driver.findElement(By.id("building")))
		   		    		.selectByVisibleText(empl.getEdificio());
		   		    }
			   		if(!empl.getLegajo().equals("-1")){
			   			driver.findElement(By.id("fileNbr")).clear();
			   		    driver.findElement(By.id("fileNbr")).sendKeys(empl.getLegajo());	 
			   		}
			   		if(!empl.getFechaNacimiento().equals("-1")){
			   			((JavascriptExecutor) driver).executeScript ("document.getElementById ('birth__date').removeAttribute('readonly',0);");
			   		    driver.findElement(By.id("birth__date")).sendKeys(empl.getFechaNacimiento()); //2014-05-01
			   		}
			   		if(!empl.getTelefono().equals("-1")){
			   		    driver.findElement(By.id("phonePrivate")).clear();
			   		    driver.findElement(By.id("phonePrivate")).sendKeys(empl.getTelefono());
			   		}
			   		if(!empl.getInterno().equals("-1")){
			   		    driver.findElement(By.id("phoneWork")).clear();
			   		    driver.findElement(By.id("phoneWork")).sendKeys(empl.getInterno());
			   		}
			   		if(!empl.getFoto().equals("-1")){
			   			driver.findElement(By.id("picture"))
			   			.sendKeys(Constantes.PATH_IMAGES+empl.getFoto());
			   		}		   			
			   		if(!empl.getCargo().equals("-1")){
		   		    	new Select(driver.findElement(By.id("position")))
	   		    		.selectByVisibleText(empl.getCargo());
			   			
			   		}
			   		if(!empl.getFechaIngreso().equals("-1")){
			   			((JavascriptExecutor) driver).executeScript ("document.getElementById ('admission__date').removeAttribute('readonly',0);");
			   		    driver.findElement(By.id("birth__date")).sendKeys(empl.getFechaIngreso()); 
			   		}

		   		    driver.findElement(By.id("btn_accept")).click();
		   		    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
		   		    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
		   		    System.out.println("Se agrego Empleado: "+empl.getApellido()+", "+empl.getNombre());
		   		    
	   			}else{
	   				System.out.println("El empleado "+empl.getApellido()+" "+empl.getNombre()+" ya existe");
	   			}
	   		}
   }   		
  }
}
