package com.matrice.psa.relevamiento;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelPisos;

import dto.Piso;

public class CargaMasivaPisosPorEdificio extends MatriceTest {
  @Test
  public void testCargaMasivaPisosPorEdificio() throws Exception {
		System.out.println(">> testCargaMasivaPisosPorEdificio");
		String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;
		boolean existe;
		Collection<Piso> pisos = ReadExcelPisos
				.leerArchivoExcel(archivoDestino, "pisos");

		if (pisos.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (Piso piso : pisos) {
				driver.get(baseUrl + "/public/building/buildings");
				wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
				driver.findElement(By.id("table_filter")).clear();
				driver.findElement(By.id("table_filter")).sendKeys(piso.getEdificio());

				wait.until(ExpectedConditions.not(ExpectedConditions
			    		.visibilityOf(driver.findElement(By.id("buildings_processing")))));
			    
			    existe = driver.findElement(By.cssSelector("BODY")).getText()
						.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$");
			    Thread.sleep(3000);

				if (existe) {
				    wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_edit")));
					driver.findElement(By.id("btn_edit")).click();
					wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_tab-buildingFloors")));
					driver.findElement(By.id("btn_tab-buildingFloors")).click();
					wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_floor")));
				    wait.until(ExpectedConditions.not(ExpectedConditions
				    		.visibilityOf(driver.findElement(By.id("floorSpaces_processing")))));
					if(!driver.findElement(By.id("floorSpaces")).getText().contains(piso.getNombre())){
						
					    driver.findElement(By.id("link_create_floor")).click();
					    wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
					    driver.findElement(By.id("name")).clear();
					    driver.findElement(By.id("name")).sendKeys(piso.getNombre());

					    if(!piso.getEmployee().equals("")){
					    	new Select(driver.findElement(By.id("employee")))
					    		.selectByVisibleText(piso.getEmployee());	
					    }
					    
				   		if(!piso.getPlano().equals("")){
				   			driver.findElement(By.id("mapPicture"))
				   			.sendKeys(Constantes.PATH_IMAGES+piso.getPlano());
				   		}		   			
					    
					    driver.findElement(By.id("btn_accept")).click();
					    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
					    assertTrue(driver.findElement(By.cssSelector("BODY"))
					    		.getText().matches("^[\\s\\S]*Piso actualizado con exito[\\s\\S]*$"));
					    
						System.out.println("Edificio: "+piso.getEdificio()+" >> Piso: "+piso.getNombre()+" se agrego");

					}else{
						System.out.println("Edificio: "+piso.getEdificio()+" >> Piso: "+piso.getNombre()+" ya existe");
					}
				}else{
					System.out.println("El edificio: "+piso.getEdificio()+" no existe");
				}
			}
		}
  }

}
