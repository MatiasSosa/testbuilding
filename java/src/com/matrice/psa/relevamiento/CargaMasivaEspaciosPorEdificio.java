package com.matrice.psa.relevamiento;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelEspacios;

import dto.Espacio;

public class CargaMasivaEspaciosPorEdificio extends MatriceTest {

  @Test
  public void testCargaMasivaEspaciosPorEdificio() throws Exception {
	  System.out.println(">> testCargaMasivaEspaciosPorEdificio");
		String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;
		boolean existe;

		Collection<Espacio> espacios = ReadExcelEspacios
				.leerArchivoExcel(archivoDestino, "espacio");

		if (espacios.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (Espacio espacio: espacios) {
				
				driver.get(baseUrl + "/public/building/buildings");
				wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
				driver.findElement(By.id("table_filter")).sendKeys(espacio.getEdificio());
				
			    wait.until(ExpectedConditions.not(ExpectedConditions
			    		.visibilityOf(driver.findElement(By.id("buildings_processing")))));
			    
			    existe = driver.findElement(By.cssSelector("BODY")).getText()
						.matches("^[\\s\\S]*Mostrando registros del 1 al 1[\\s\\S]*$");
			    Thread.sleep(3000);

				if (existe) {
					wait.until(ExpectedConditions.elementToBeClickable(By.id("btn_edit")));
					driver.findElement(By.id("btn_edit")).click();
					wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Gestionar espacios")));
					driver.findElement(By.linkText("Gestionar espacios")).click();
				    wait.until(ExpectedConditions.not(ExpectedConditions
				    		.visibilityOf(driver.findElement(By.id("floorSpaces_processing")))));
					
					if(!driver.findElement(By.id("floorSpaces")).getText().contains(espacio.getNombre())){
					    driver.findElement(By.id("link_create_space")).click();
					    driver.findElement(By.id("name")).clear();
					    driver.findElement(By.id("name")).sendKeys(espacio.getNombre());
					    
					    new Select(driver.findElement(By.id("buildingFloor"))).selectByVisibleText(espacio.getPiso());
					    
					    if(!espacio.getTipo().equals("")){
					    	new Select(driver.findElement(By.id("spaceType")))
					    		.selectByVisibleText(espacio.getTipo());	
					    }
					    if(!espacio.getAreas().equals("")){
						    String[] areas = espacio.getAreas().split(",");
						    for (String area: areas){
						    	new Select(driver.findElement(By.id("areas"))).selectByVisibleText(area);
						    }				    	
					    }

					    if(!espacio.getResponsable().equals("")){
					    	new Select(driver.findElement(By.id("employee")))
					    	.selectByVisibleText(espacio.getResponsable());	
					    }

					    if(!espacio.getNombre().equals("")){
					    	driver.findElement(By.id("measurement__double")).clear();
						    driver.findElement(By.id("measurement__double")).sendKeys(espacio.getNombre());	
					    }
					    
					    if(espacio.getPropietario().equals("si") && espacio.getAlquilado().equals("si")){
					        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@id='isRented'])[2]")));
					        driver.findElement(By.xpath("(//input[@id='isRented'])[2]")).click();
					    }
					    
					    driver.findElement(By.id("btn_accept")).click();
					    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
					    
					    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
					    		.matches("^[\\s\\S]*Espacio creado con exito[\\s\\S]*$"));
					    
					    System.out.println("Edificio: "+espacio.getEdificio()
					    		+" >> Espacio: "+espacio.getNombre()+" se agregado");
					}else{
						System.out.println("Edificio: "+espacio.getEdificio()
								+" >> Espacio: "+espacio.getNombre()+" ya existe");
					}
				}else{
					System.out.println("El edificio: "+espacio.getEdificio()+" no existe");
				}
			}
		}
	}
}
