package com.matrice.psa.relevamiento;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelInmuebles;

import dto.Inmueble;

/* 
 *  CargaMasivaEdificios permite cargar la informacion de un edificio, 
 *  a excepci�n de su responsable  
 */
public class CargaMasivaEdificios extends MatriceTest {
  @Test
  public void testCargaMasivaEdificios() throws Exception {

		System.out.println(">> testCargaMasivaEdificios");
		String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;
		boolean noExiste;

		Collection<Inmueble> inmuebles = ReadExcelInmuebles
				.leerArchivoExcel(archivoDestino, "edificio");

		if (inmuebles.size() != 0) {
			// agrego los tipos definidos en el archivo
			for (Inmueble inmueble : inmuebles) {
				driver.get(baseUrl + "/public/building/buildings");
				
				wait.until(ExpectedConditions.elementToBeClickable(By
						.id("table_filter")));
				driver.findElement(By.id("table_filter")).sendKeys(
						inmueble.getNombre());

				wait.until(ExpectedConditions.not(ExpectedConditions
						.visibilityOf(driver.findElement(By
								.id("buildings_processing")))));

				noExiste = driver.findElement(By.cssSelector("BODY")).getText()
						.matches("^[\\s\\S]*Mostrando registros del 0 al 0[\\s\\S]*$");

				if (noExiste) {
					wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create")));
					driver.findElement(By.id("link_create")).click();
					wait.until(ExpectedConditions.elementToBeClickable(By.id("name")));
				    driver.findElement(By.id("name")).clear();
				    driver.findElement(By.id("name")).sendKeys(inmueble.getNombre());
				    
//				    if(!inmueble.getResponsable().equals("")){
//				    	new Select(driver.findElement(By.id("employee"))).selectByVisibleText(inmueble.getResponsable());
//				    }
				    
				    new Select(driver.findElement(By.id("country"))).selectByVisibleText(inmueble.getPais());
				    Thread.sleep(3000);
				    new Select(driver.findElement(By.id("state"))).selectByVisibleText(inmueble.getProvincia());
				    Thread.sleep(3000);
				    new Select(driver.findElement(By.id("city"))).selectByVisibleText(inmueble.getCiudad());
				    Thread.sleep(3000);
				    
				    new Select(driver.findElement(By.id("zone"))).selectByVisibleText(inmueble.getZona());
				    
				    
				    driver.findElement(By.id("address")).clear();
				    driver.findElement(By.id("address")).sendKeys(inmueble.getDireccion());
				    driver.findElement(By.id("geocode")).click();
				    
				    if(!inmueble.getDimension().equals("")){
				    	driver.findElement(By.id("measurement__double")).sendKeys(inmueble.getDimension());
				    }
				    
				    if(!inmueble.getPropietario().equals("No")){
				    	driver.findElement(By.id("isOwner")).click();
				    	if(!inmueble.getAlquilado().equals("No")){
				    	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@id='isRented'])[2]")));
				    	    driver.findElement(By.xpath("(//input[@id='isRented'])[2]")).click();
				    	}
				    }
				    
				    driver.findElement(By.id("btn_accept")).click();
				    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
				    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*con exito[\\s\\S]*$"));
				    System.out.println("Se agrego inmueble: "+inmueble.getNombre());
				    
				}else{
				    System.out.println("Ya existe el inmueble: "+inmueble.getNombre());
				}
			}
		}
  }
  }


