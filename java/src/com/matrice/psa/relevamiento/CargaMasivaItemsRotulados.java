package com.matrice.psa.relevamiento;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.matrice.general.Constantes;
import com.matrice.general.MatriceTest;
import com.matrice.general.readExcels.ReadExcelStockRotulado;

import dto.ItemRotulado;

public class CargaMasivaItemsRotulados extends MatriceTest {

  Long num_marca = calendario.getTimeInMillis();

  @Test
  public void testCargaMasivaItemsRotulados() throws Exception {
	  
	String archivoDestino = Constantes.PATH_DATA + Constantes.NAME_ARCH_MOD_GESTION;
	boolean existe;
	List<ItemRotulado> itemsRotulados = ReadExcelStockRotulado
			.leerArchivoExcel(archivoDestino, "InsumosRotulados");

	if (itemsRotulados.size() != 0) {
		for(ItemRotulado rotulado: itemsRotulados){
			driver.get(baseUrl + "/public/commodityStock/commodityStocks");
			wait.until(ExpectedConditions.elementToBeClickable(By.id("table_filter")));
			driver.findElement(By.id("table_filter")).clear();
			driver.findElement(By.id("table_filter")).sendKeys(rotulado.getTipoStock());
		    wait.until(ExpectedConditions.not(ExpectedConditions
		    		.visibilityOf(driver.findElement(By.id("commodityTypes_processing")))));
		    
		    assertFalse(driver.findElement(By.cssSelector("BODY")).getText()
		    		.matches("^[\\s\\S]*Mostrando registros del 0[\\s\\S]*$"));
		    
		    driver.findElement(By.id("btn_edit")).click();

		    driver.findElement(By.linkText("Articulos registrados")).click();

		    existe = driver.findElement(By.id("registeredItems")).getText()
		    		.matches("^[\\s\\S]*"+rotulado.getEtiqueta()+"[\\s\\S]*$");
		    
		    if(!existe){
		        driver.findElement(By.linkText("General")).click();
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("link_create_floor")));
			    
			    Integer stockActual =  Integer.parseInt(driver.findElement(
			    		By.xpath("/html/body/div[1]/div[2]/div[1]/div[6]/div/label")).getText().toString());
			    
			    System.out.println(rotulado.getTipoStock()+", Stock Actual: "+stockActual);
			    
			    driver.findElement(By.id("link_create_floor")).click();
			    driver.findElement(By.id("tag")).clear();
			    driver.findElement(By.id("tag")).sendKeys(rotulado.getEtiqueta());
			    driver.findElement(By.id("brand")).clear();
			    driver.findElement(By.id("brand")).sendKeys(rotulado.getMarca());
			    driver.findElement(By.id("model")).clear();
			    driver.findElement(By.id("model")).sendKeys(rotulado.getModelo());
			    
			    if(!rotulado.getDescripcion().equals("")){
				    driver.findElement(By.id("description")).clear();
				    driver.findElement(By.id("description")).sendKeys(rotulado.getDescripcion());
			    }

			    if(!rotulado.getEdificio().equals("")){
			    	driver.findElement(By.linkText("Ubicacion y asignacion")).click();
	   		    	new Select(driver.findElement(By.id("building")))
			    		.selectByVisibleText(rotulado.getEdificio());
				    if(!rotulado.getPiso().equals("")){
		   		    	new Select(driver.findElement(By.id("buildingFloor")))
				    		.selectByVisibleText(rotulado.getPiso());
					    if(!rotulado.getEspacio().equals("")){
			   		    	new Select(driver.findElement(By.id("floorSpace")))
					    		.selectByVisibleText(rotulado.getEspacio());
					    }
				    }
			    }

			    driver.findElement(By.id("btn_accept")).click();
			    
			    wait.until(ExpectedConditions.elementToBeClickable(By.id("succes_notify_div")));
			    
			    assertTrue(driver.findElement(By.cssSelector("BODY")).getText()
			    		.matches("^[\\s\\S]*Articulo registrado[\\s\\S]*$"));
			    
			    Integer stockModificado =  Integer.parseInt(driver.findElement(
			    		By.xpath("/html/body/div[1]/div[3]/div[1]/div[6]/div/label")).getText().toString());
			    
			    System.out.println(rotulado.getTipoStock()+",Stock Modificado: " + stockModificado);
			    stockActual++;
			    assertTrue(stockActual.equals(stockModificado));

		    	
				}

			}
		}
	}
}
