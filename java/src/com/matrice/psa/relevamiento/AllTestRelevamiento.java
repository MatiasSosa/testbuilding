package com.matrice.psa.relevamiento;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.matrice.building.configuracionBase.ValidarAgregarCargosExcel;
import com.matrice.building.configuracionBase.ValidarAgregarCiudadesExcel;
import com.matrice.building.configuracionBase.ValidarAgregarEstadosGravedadExcel;
import com.matrice.building.configuracionBase.ValidarAgregarEstadosPedidosExcel;
import com.matrice.building.configuracionBase.ValidarAgregarPaisesExcel;
import com.matrice.building.configuracionBase.ValidarAgregarProveedoresExcel;
import com.matrice.building.configuracionBase.ValidarAgregarProvinciasExcel;
import com.matrice.building.configuracionBase.ValidarAgregarResultadosMantenimientoExcel;
import com.matrice.building.configuracionBase.ValidarAgregarRubrosExcel;
import com.matrice.building.configuracionBase.ValidarAgregarTiposBienesConsumoExcel;
import com.matrice.building.configuracionBase.ValidarAgregarTiposBienesUsoExcel;
import com.matrice.building.configuracionBase.ValidarAgregarTiposEspaciosExcel;
import com.matrice.building.configuracionBase.ValidarAgregarTiposGastosExcel;
import com.matrice.building.configuracionBase.ValidarAgregarTiposMantenimientoExcel;
import com.matrice.building.configuracionBase.ValidarAgregarZonasExcel;
import com.matrice.general.LoginAdmin;


@RunWith(Suite.class)
@SuiteClasses({
	LoginAdmin.class,

	//Configuracion
	// Necesarios
	ValidarAgregarPaisesExcel.class,
	ValidarAgregarProvinciasExcel.class,
	ValidarAgregarCiudadesExcel.class,
	ValidarAgregarZonasExcel.class,
	ValidarAgregarTiposEspaciosExcel.class,
	ValidarAgregarCargosExcel.class,
	ValidarAgregarRubrosExcel.class,
	ValidarAgregarTiposBienesUsoExcel.class,
	ValidarAgregarTiposBienesConsumoExcel.class,
	// Adicionales
	ValidarAgregarProveedoresExcel.class,
	ValidarAgregarTiposGastosExcel.class,
	ValidarAgregarEstadosPedidosExcel.class,
	ValidarAgregarEstadosGravedadExcel.class,
	ValidarAgregarResultadosMantenimientoExcel.class,
	ValidarAgregarTiposMantenimientoExcel.class,
	
	//Carga de Inmuebles /empleados /areas
	CargaMasivaEdificios.class,
	CargaMasivaEmpleados.class,
	EditarResponsablePorEdificio.class,
	CargaMasivaAreas.class,
	CargaMasivaPisosPorEdificio.class,
	CargaMasivaEspaciosPorEdificio.class,
	EditarEmpleadosDatosUbicacion.class,
	
	//Bienes de uso y consumo
	CargarMasivaBienesDeUso.class,
	CargaMasivaItemsRotulados.class,
	
})


public class AllTestRelevamiento {
}
