package dto;

public class Gasto {
private String concepto;
private String titular;
private String tipoGasto; //servicio mantenimiento otro	
private String tipoGastoUsuario;	
private String tipoBien;  //INMUEBLE - BIEN DE USO - INSUMO - OTRO 
private String tipoBienUsuario;
private String bienes;
private String edificio;
private String pisos;	
private String espacios;	
private String proveedor ;	
private String periodicidad_num; 
private String periodicidad_unidad;
private String rubro;
private String mantenimiento;
// unicos
private String fechaPago;
private String importePagado;
private String comprobante;
private String estado;

public Gasto() {
	super();
}

public Gasto(String concepto, String titular, String tipoGasto,
		String tipoGastoUsuario, String tipoBien, String tipoBienUsuario,
		String bienes, String edificio,String pisos,
		String espacios, String proveedor, String periodicidad_num,
		String periodicidad_unidad, String rubro, String mantenimiento) {
	super();
	this.concepto = concepto;
	this.titular = titular;
	this.tipoGasto = tipoGasto;
	this.tipoGastoUsuario = tipoGastoUsuario;
	this.tipoBien = tipoBien;
	this.tipoBienUsuario = tipoBienUsuario;
	this.bienes = bienes;
	this.edificio = edificio;
	this.pisos = pisos;
	this.espacios = espacios;
	this.proveedor = proveedor;
	this.periodicidad_num = periodicidad_num;
	this.periodicidad_unidad = periodicidad_unidad;
	this.rubro = rubro;
	this.mantenimiento = mantenimiento;
}
public Gasto(String concepto, String titular, String tipoGasto,
		String tipoGastoUsuario, String tipoBien, String tipoBienUsuario,
		String bienes, String edificio,String pisos,
		String espacios, String proveedor, String periodicidad_num,
		String periodicidad_unidad, String rubro, String mantenimiento,
		String fechaPago,String importePagado, String comprobante,String estado) {
	super();
	this.concepto = concepto;
	this.titular = titular;
	this.tipoGasto = tipoGasto;
	this.tipoGastoUsuario = tipoGastoUsuario;
	this.tipoBien = tipoBien;
	this.tipoBienUsuario = tipoBienUsuario;
	this.bienes = bienes;
	this.edificio = edificio;
	this.pisos = pisos;
	this.espacios = espacios;
	this.proveedor = proveedor;
	this.periodicidad_num = periodicidad_num;
	this.periodicidad_unidad = periodicidad_unidad;
	this.rubro = rubro;
	this.mantenimiento = mantenimiento;
	this.fechaPago = fechaPago;
	this.importePagado = importePagado;
	this.comprobante = comprobante;
	this.estado = estado;
	
}
public String getConcepto() {
	return concepto;
}
public void setConcepto(String concepto) {
	this.concepto = concepto;
}
public String getTitular() {
	return titular;
}
public void setTitular(String titular) {
	this.titular = titular;
}
public String getTipoGasto() {
	return tipoGasto;
}
public void setTipoGasto(String tipoGasto) {
	this.tipoGasto = tipoGasto;
}
public String getTipoGastoUsuario() {
	return tipoGastoUsuario;
}
public void setTipoGastoUsuario(String tipoGastoUsuario) {
	this.tipoGastoUsuario = tipoGastoUsuario;
}
public String getTipoBien() {
	return tipoBien;
}
public void setTipoBien(String tipoBien) {
	this.tipoBien = tipoBien;
}
public String getBienes() {
	return bienes;
}
public void setBienes(String bienes) {
	this.bienes = bienes;
}
public String getPisos() {
	return pisos;
}
public void setPisos(String pisos) {
	this.pisos = pisos;
}
public String getEspacios() {
	return espacios;
}
public void setEspacios(String espacios) {
	this.espacios = espacios;
}
public String getProveedor() {
	return proveedor;
}
public void setProveedor(String proveedor) {
	this.proveedor = proveedor;
}
public String getPeriodicidad_num() {
	return periodicidad_num;
}
public void setPeriodicidad_num(String periodicidad_num) {
	this.periodicidad_num = periodicidad_num;
}
public String getPeriodicidad_unidad() {
	return periodicidad_unidad;
}
public void setPeriodicidad_unidad(String periodicidad_unidad) {
	this.periodicidad_unidad = periodicidad_unidad;
}

public String getRubro() {
	return rubro;
}

public void setRubro(String rubro) {
	this.rubro = rubro;
}

public String getMantenimiento() {
	return mantenimiento;
}

public void setMantenimiento(String mantenimiento) {
	this.mantenimiento = mantenimiento;
}

public String getEdificio() {
	return edificio;
}

public void setEdificio(String edificio) {
	this.edificio = edificio;
}

public String getTipoBienUsuario() {
	return tipoBienUsuario;
}

public void setTipoBienUsuario(String tipoBienUsuario) {
	this.tipoBienUsuario = tipoBienUsuario;
}

public String getFechaPago() {
	return fechaPago;
}

public void setFechaPago(String fechaPago) {
	this.fechaPago = fechaPago;
}

public String getImportePagado() {
	return importePagado;
}

public void setImportePagado(String importePagado) {
	this.importePagado = importePagado;
}

public String getComprobante() {
	return comprobante;
}

public void setComprobante(String comprobante) {
	this.comprobante = comprobante;
}

public String getEstado() {
	return estado;
}

public void setEstado(String estado) {
	this.estado = estado;
}


}
