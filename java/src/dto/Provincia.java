package dto;

public class Provincia {

	String nombrePais;
	String nombreProvincia;
	public Provincia(String nombrePais, String nombreProvincia) {
		super();
		this.nombrePais = nombrePais;
		this.nombreProvincia = nombreProvincia;
	}
	
	public String getNombrePais() {
		return nombrePais;
	}
	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}
	public String getNombreProvincia() {
		return nombreProvincia;
	}
	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}
	
	
}
