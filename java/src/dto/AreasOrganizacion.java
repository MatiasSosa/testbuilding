package dto;

public class AreasOrganizacion {
	String nombre;
	String padre;
	String responsable;
	
	public AreasOrganizacion() {
		super();
	}
	public AreasOrganizacion(String nombre, String padre, String responsable) {
		super();
		this.nombre = nombre;
		this.padre = padre;
		this.responsable = responsable;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPadre() {
		return padre;
	}
	public void setPadre(String padre) {
		this.padre = padre;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	
}
