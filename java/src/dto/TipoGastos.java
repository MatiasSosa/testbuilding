package dto;

public class TipoGastos {
	String nombre;
	String tipo;
	String tipoBien;
	String isAllPropertiesGeneral;
	
	
	
	public TipoGastos(String nombre, String tipo, String tipoBien,
			String isAllPropertiesGeneral) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
		this.tipoBien = tipoBien;
		this.isAllPropertiesGeneral = isAllPropertiesGeneral;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}
	public String getIsAllPropertiesGeneral() {
		return isAllPropertiesGeneral;
	}
	public void setIsAllPropertiesGeneral(String isAllPropertiesGeneral) {
		this.isAllPropertiesGeneral = isAllPropertiesGeneral;
	}

	
	
}
