package dto;

public class Ciudad {
	
	String nombrePais;
	String nombreProvincia;
	String nombreCiudad;
	public Ciudad(String nombrePais, String nombreProvincia, String nombreCiudad) {
		super();
		this.nombrePais = nombrePais;
		this.nombreProvincia = nombreProvincia;
		this.nombreCiudad = nombreCiudad;
	}
	public String getNombrePais() {
		return nombrePais;
	}
	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}
	public String getNombreProvincia() {
		return nombreProvincia;
	}
	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}
	public String getNombreCiudad() {
		return nombreCiudad;
	}
	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}
	
	

}
