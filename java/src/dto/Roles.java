package dto;

import java.util.ArrayList;
import java.util.List;

public class Roles {
	private String rol;
	private List<String> permisos;
	
	public Roles() {
		super();
		this.permisos = new ArrayList<String>();
	}
	public Roles(String rol) {
		super();
		this.rol = rol;
		this.permisos = new ArrayList<String>();
	}
	public Roles(String rol, List<String> permisos) {
		super();
		this.rol = rol;
		this.permisos = permisos;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	public List<String> getPermisos() {
		return permisos;
	}
	public void setPermisos(List<String> permisos) {
		this.permisos = permisos;
	}
	public void setPermisos(String permiso) {
		this.permisos.add(permiso);
	}
	
	
	
}
