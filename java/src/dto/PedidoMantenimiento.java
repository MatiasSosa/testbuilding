package dto;

public class PedidoMantenimiento {

	private String titulo;
	private String solicitante;
	private String tipo;
	private String bien;
	private String tipoMantenimiento;
	private String severidad;
	private String descripcion;
	private String comentario;
	
	
	public PedidoMantenimiento(String titulo, String solicitante, String tipo,
			String bien, String tipoMantenimiento, String severidad,
			String descripcion, String comentario) {
		super();
		this.titulo = titulo;
		this.solicitante = solicitante;
		this.tipo = tipo;
		this.bien = bien;
		this.tipoMantenimiento = tipoMantenimiento;
		this.severidad = severidad;
		this.descripcion = descripcion;
		this.comentario = comentario;
		
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getSolicitante() {
		return solicitante;
	}
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getBien() {
		return bien;
	}
	public void setBien(String bien) {
		this.bien = bien;
	}
	public String getTipoMantenimiento() {
		return tipoMantenimiento;
	}
	public void setTipoMantenimiento(String tipoMantenimiento) {
		this.tipoMantenimiento = tipoMantenimiento;
	}
	public String getSeveridad() {
		return severidad;
	}
	public void setSeveridad(String severidad) {
		this.severidad = severidad;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	

}
