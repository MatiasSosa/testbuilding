package dto;

public class Pagos {
	private String concepto;	
	private String cuota;	
	private String vto1Fecha;
	private String vto1Importe;
	private String vto2Fecha;
	private String vto2Importe;
	private String vto3Fecha;
	private String vto3Importe;
	
	public Pagos() {
		super();
	}
	
	public Pagos(String concepto, String cuota, String vto1Fecha,
			String vto1Importe, String vto2Fecha, String vto2Importe,
			String vto3Fecha, String vto3Importe) {
		super();
		this.concepto = concepto;
		this.cuota = cuota;
		this.vto1Fecha = vto1Fecha;
		this.vto1Importe = vto1Importe;
		this.vto2Fecha = vto2Fecha;
		this.vto2Importe = vto2Importe;
		this.vto3Fecha = vto3Fecha;
		this.vto3Importe = vto3Importe;
	}
	
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getCuota() {
		return cuota;
	}
	public void setCuota(String cuota) {
		this.cuota = cuota;
	}
	public String getVto1Fecha() {
		return vto1Fecha;
	}
	public void setVto1Fecha(String vto1Fecha) {
		this.vto1Fecha = vto1Fecha;
	}
	public String getVto1Importe() {
		return vto1Importe;
	}
	public void setVto1Importe(String vto1Importe) {
		this.vto1Importe = vto1Importe;
	}
	public String getVto2Fecha() {
		return vto2Fecha;
	}
	public void setVto2Fecha(String vto2Fecha) {
		this.vto2Fecha = vto2Fecha;
	}
	public String getVto2Importe() {
		return vto2Importe;
	}
	public void setVto2Importe(String vto2Importe) {
		this.vto2Importe = vto2Importe;
	}
	public String getVto3Fecha() {
		return vto3Fecha;
	}
	public void setVto3Fecha(String vto3Fecha) {
		this.vto3Fecha = vto3Fecha;
	}
	public String getVto3Importe() {
		return vto3Importe;
	}
	public void setVto3Importe(String vto3Importe) {
		this.vto3Importe = vto3Importe;
	}
	
	

}
