package dto;

public class Espacio {
	
	String edificio;
	String piso;
	String nombre;
	String tipo;
	String areas;
	String responsable;
	String dimension;
	String propietario;
	String alquilado;
	
	
	public Espacio(String edificio, String piso, String nombre, String tipo,
			String areas, String responsable, String dimension,
			String propietario, String alquilado) {
		super();
		this.edificio = edificio;
		this.piso = piso;
		this.nombre = nombre;
		this.tipo = tipo;
		this.areas = areas;
		this.responsable = responsable;
		this.dimension = dimension;
		this.propietario = propietario;
		this.alquilado = alquilado;
	}
	public String getEdificio() {
		return edificio;
	}
	public void setEdificio(String edificio) {
		this.edificio = edificio;
	}
	public String getPiso() {
		return piso;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getAreas() {
		return areas;
	}
	public void setAreas(String areas) {
		this.areas = areas;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getDimension() {
		return dimension;
	}
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}
	public String getPropietario() {
		return propietario;
	}
	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}
	public String getAlquilado() {
		return alquilado;
	}
	public void setAlquilado(String alquilado) {
		this.alquilado = alquilado;
	}
	
}
