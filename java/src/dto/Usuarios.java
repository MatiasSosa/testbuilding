package dto;

public class Usuarios {
	//Datos obligatorios
	private String usuario;
	private String email;
	private String  contrasenia;
	//Datos no obligatorios
	private String confirmarContrasenia;
	private String activado;
	private String rol;
	
	public Usuarios() {
		super();
	}
	public Usuarios(String usuario, String email, String contrasenia,
			String confirmarContrasenia, String activado, String rol) {
		super();
		this.usuario = usuario;
		this.email = email;
		this.contrasenia = contrasenia;
		this.confirmarContrasenia = confirmarContrasenia;
		this.activado = activado;
		this.rol = rol;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	public String getConfirmarContrasenia() {
		return confirmarContrasenia;
	}
	public void setConfirmarContrasenia(String confirmarContrasenia) {
		this.confirmarContrasenia = confirmarContrasenia;
	}
	public String getActivado() {
		return activado;
	}
	public void setActivado(String activado) {
		this.activado = activado;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	
}
