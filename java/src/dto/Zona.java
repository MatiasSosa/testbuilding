package dto;

public class Zona {
	
	String nombrePais;
	String nombreProvincia;
	String nombreCiudad;
	String nombreZona;
	public String getNombreZona() {
		return nombreZona;
	}
	public void setNombreZona(String nombreZona) {
		this.nombreZona = nombreZona;
	}
	public Zona(String nombrePais, String nombreProvincia, String nombreCiudad,String nombreZona) {
		super();
		this.nombrePais = nombrePais;
		this.nombreProvincia = nombreProvincia;
		this.nombreCiudad = nombreCiudad;
		this.nombreZona = nombreZona;
	}
	public String getNombrePais() {
		return nombrePais;
	}
	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}
	public String getNombreProvincia() {
		return nombreProvincia;
	}
	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}
	public String getNombreCiudad() {
		return nombreCiudad;
	}
	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}
	
	

}
