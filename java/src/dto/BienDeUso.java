package dto;

public class BienDeUso {
	//Obligatorios
	private String tipo;
	private String etiqueta;
	private String marca;
	private String estado;
	//no obligatorios
	private String modelo;
	private String numSerie;
	private String descripcion;
	private String fechaFabricacion;
	private String fechaCompra;
	private String vidaUtil;
	private String foto;
	
	public BienDeUso() {
		super();
	}
	public BienDeUso(
			String tipo, String etiqueta, String marca, String estado,
			String modelo, String numSerie, String descripcion,
			String fechaFabricacion, String fechaCompra, String vidaUtil,
			String foto) {
		super();
		this.tipo = tipo;
		this.etiqueta = etiqueta;
		this.marca = marca;
		this.estado = estado;
		this.modelo = modelo;
		this.numSerie = numSerie;
		this.descripcion = descripcion;
		this.fechaFabricacion = fechaFabricacion;
		this.fechaCompra = fechaCompra;
		this.vidaUtil = vidaUtil;
		this.foto = foto;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNumSerie() {
		return numSerie;
	}
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFechaFabricacion() {
		return fechaFabricacion;
	}
	public void setFechaFabricacion(String fechaFabricacion) {
		this.fechaFabricacion = fechaFabricacion;
	}
	public String getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(String fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public String getVidaUtil() {
		return vidaUtil;
	}
	public void setVidaUtil(String vidaUtil) {
		this.vidaUtil = vidaUtil;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	
}
