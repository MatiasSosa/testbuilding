package dto;

public class Empleados {
	//Requeridos (5)
	String nombre;
	String apellido;	
	String edificio;	
	String email;
	String documento;

	//No Requeridos (10)
	String legajo;
	String fechaNacimiento;	
	String telefono;
	String interno;
	String foto;
	String cargo;
	String fechaIngreso;
	String area;
	String piso;
	String espacio;
	
	public Empleados() {
		super();
	}
	
	public Empleados(String nombre, String apellido, String edificio,
			String email, String documento, String legajo,
			String fechaNacimiento, String telefono, String interno,
			String foto, String cargo, String fechaIngreso, String area,
			String piso, String espacio) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.edificio = edificio;
		this.email = email;
		this.documento = documento;
		this.legajo = legajo;
		this.fechaNacimiento = fechaNacimiento;
		this.telefono = telefono;
		this.interno = interno;
		this.foto = foto;
		this.cargo = cargo;
		this.fechaIngreso = fechaIngreso;
		this.area = area;
		this.piso = piso;
		this.espacio = espacio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEdificio() {
		return edificio;
	}

	public void setEdificio(String edificio) {
		this.edificio = edificio;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getLegajo() {
		return legajo;
	}

	public void setLegajo(String legajo) {
		this.legajo = legajo;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getInterno() {
		return interno;
	}

	public void setInterno(String interno) {
		this.interno = interno;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getEspacio() {
		return espacio;
	}

	public void setEspacio(String espacio) {
		this.espacio = espacio;
	}

	
	
	
}
