package dto;

public class ItemRotulado {
	private String tipoStock;
	private String etiqueta;
	private String marca;
	private String modelo;
	private String descripcion;
	private String edificio;
	private String piso;
	private String espacio;
	private String area;

	public ItemRotulado() {
		super();
	}

	public ItemRotulado(String tipo,String etiqueta, String marca, String modelo,
			String descripcion, String edificio, String piso, String espacio,
			String area) {
		super();
		this.tipoStock = tipo;
		this.etiqueta = etiqueta;
		this.marca = marca;
		this.modelo = modelo;
		this.descripcion = descripcion;
		this.edificio = edificio;
		this.piso = piso;
		this.espacio = espacio;
		this.area = area;
	}

	public String getTipoStock() {
		return tipoStock;
	}

	public void setTipoStock(String tipo) {
		this.tipoStock = tipo;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEdificio() {
		return edificio;
	}

	public void setEdificio(String edificio) {
		this.edificio = edificio;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getEspacio() {
		return espacio;
	}

	public void setEspacio(String espacio) {
		this.espacio = espacio;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
}
