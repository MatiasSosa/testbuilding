package dto;

import java.util.ArrayList;
import java.util.List;

public class OpcionMenuValidar {
	
	String opcionMenu;
	String url;
	// listado de casos
	List<Casos> listado;
	
	public OpcionMenuValidar() {
		super();
		this.listado = new ArrayList<Casos>();
	}

	public OpcionMenuValidar(String opcionMenu, String url) {
		super();
		this.opcionMenu = opcionMenu;
		this.url = url;
		this.listado = new ArrayList<Casos>();
	}

	public String getOpcionMenu() {
		return opcionMenu;
	}

	public void setOpcionMenu(String opcionMenu) {
		this.opcionMenu = opcionMenu;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Casos> getListado() {
		return listado;
	}

	public void setListado(List<Casos> listado) {
		this.listado = listado;
	}

	public void setListado(Casos caso) {
		this.listado.add(caso);
	}

}
