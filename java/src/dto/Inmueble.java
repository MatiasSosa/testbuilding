package dto;

public class Inmueble {
	private String nombre;
	private String responsable;
	private String pais;
	private String provincia;
	private String ciudad;
	private String zona;
	private String direccion;
	private String dimension;
	private String propietario;
	private String alquilado;
	private String empleados;
	private String piso;
	private String espacio;
	private String area;

	public Inmueble(String nombre, String responsable, String pais, String provincia,
			String ciudad, String zona, String direccion, String dimension, String propietario, 
			String alquilado, String empleados,String piso, String espacio, String area) {
		super();
		this.nombre = nombre;
		this.pais = pais;
		this.provincia = provincia;
		this.ciudad = ciudad;
		this.zona = zona;
		this.direccion = direccion;
		this.responsable = responsable;
		this.propietario = propietario;
		this.dimension = dimension;
		this.alquilado = alquilado;
		this.empleados = empleados;
		this.piso = piso;
		this.espacio = espacio;
		this.area = area;
	}
	
	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getEspacio() {
		return espacio;
	}

	public void setEspacio(String espacio) {
		this.espacio = espacio;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAlquilado() {
		return alquilado;
	}
	public void setAlquilado(String alquilado) {
		this.alquilado = alquilado;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getDimension() {
		return dimension;
	}
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}
	public String getPropietario() {
		return propietario;
	}
	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getEmpleados() {
		return empleados;
	}
	public void setEmpleados(String empleados) {
		this.empleados = empleados;
	}
	
	
	
	}
