package dto;

public class TipoBienConsumo {

private String nombre;
private String rubro;
private String descripcion;
private String foto;
private String rotulacion;
private String stockInicial;
private String stockMinimo;

public TipoBienConsumo() {
	super();
}

public TipoBienConsumo(String nombre, String rubro, String descripcion,
		String foto, String rotulacion, String stockInicial, String stockMinimo) {
	super();
	this.nombre = nombre;
	this.rubro = rubro;
	this.descripcion = descripcion;
	this.foto = foto;
	this.rotulacion = rotulacion;
	this.stockInicial = stockInicial;
	this.stockMinimo = stockMinimo;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getRubro() {
	return rubro;
}
public void setRubro(String rubro) {
	this.rubro = rubro;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getFoto() {
	return foto;
}
public void setFoto(String foto) {
	this.foto = foto;
}
public String getRotulacion() {
	return rotulacion;
}
public void setRotulacion(String rotulacion) {
	this.rotulacion = rotulacion;
}
public String getStockInicial() {
	return stockInicial;
}
public void setStockInicial(String stockInicial) {
	this.stockInicial = stockInicial;
}
public String getStockMinimo() {
	return stockMinimo;
}
public void setStockMinimo(String stockMinimo) {
	this.stockMinimo = stockMinimo;
}




}
