package dto;

import java.util.ArrayList;
import java.util.List;

public class Casos {
	
	int caso_num;
	List<DatoRevisarCompletar> listadoCompletar;
	List<DatoRevisarCompletar> listadoRevisar;

	public Casos() {
		super();
		this.listadoCompletar = new ArrayList<DatoRevisarCompletar>();
		this.listadoRevisar = new ArrayList<DatoRevisarCompletar>();
	}

	public Casos(int caso_num) {
		super();
		this.caso_num = caso_num;
		this.listadoCompletar = new ArrayList<DatoRevisarCompletar>();
		this.listadoRevisar = new ArrayList<DatoRevisarCompletar>();
	}

	public int getCaso_num() {
		return caso_num;
	}

	public void setCaso_num(int caso_num) {
		this.caso_num = caso_num;
	}

	public List<DatoRevisarCompletar> getListadoCompletar() {
		return listadoCompletar;
	}

	public void setListadoCompletar(List<DatoRevisarCompletar> listadoCompletar) {
		this.listadoCompletar = listadoCompletar;
	}
	
	public void setListadoCompletar(DatoRevisarCompletar dato) {
		this.listadoCompletar.add(dato);
	}

	public List<DatoRevisarCompletar> getListadoRevisar() {
		return listadoRevisar;
	}

	public void setListadoRevisar(List<DatoRevisarCompletar> listadoRevisar) {
		this.listadoRevisar = listadoRevisar;
	}
	
	public void setListadoRevisar(DatoRevisarCompletar dato) {
		this.listadoRevisar.add(dato);
	}


	



}
