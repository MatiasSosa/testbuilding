package dto;

public class Piso {
	
	String edificio;
	String nombre;
	String employee;
	String plano;
	
	
	public Piso(String edificio, String nombre, String employee, String plano) {
		super();
		this.edificio = edificio;
		this.nombre = nombre;
		this.employee = employee;
		this.plano = plano;
	}
	public String getEdificio() {
		return edificio;
	}
	public void setEdificio(String edificio) {
		this.edificio = edificio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public String getPlano() {
		return plano;
	}
	public void setPlano(String plano) {
		this.plano = plano;
	}
	
	

}
