package dto;

public class DatoRevisarCompletar {
	
	String campo;
	String tipo;
	String valor;
	String mensaje;
	String contenedorMensaje;

	
	public DatoRevisarCompletar() {
		super();
	}


	public DatoRevisarCompletar(String mensaje) {
		super();
		this.mensaje = mensaje;
	}


	public DatoRevisarCompletar(String campo, String tipo, String valor,
			String mensaje, String contenedorMensaje) {
		super();
		this.campo = campo;
		this.tipo = tipo;
		this.valor = valor;
		this.mensaje = mensaje;
		this.contenedorMensaje = contenedorMensaje;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getContenedorMensaje() {
		return contenedorMensaje;
	}

	public void setContenedorMensaje(String contenedorMensaje) {
		this.contenedorMensaje = contenedorMensaje;
	}


}
